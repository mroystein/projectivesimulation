# README #



### Projective simulation ###
A framework for Projective Simulation coded in Java. The code is based on the work from H.J.Briegel [Article](http://www.nature.com/srep/2012/120515/srep00400/full/srep00400.html)

Games included: Invasion Game, Grid World, Mountain Car, Cart Pole.



![SuccessRate_gammas.png](https://bitbucket.org/repo/poMq7z/images/664349047-SuccessRate_gammas.png).
![chart_4actions_Mutliple_gammas.png](https://bitbucket.org/repo/poMq7z/images/3410532860-chart_4actions_Mutliple_gammas.png)

## Benefit of using reflection (Emotion tags) ##
![reflection-emotion.png](https://bitbucket.org/repo/poMq7z/images/1226766549-reflection-emotion.png)

## Change of percepts (Symbols) ##
![symbol_change_color_vs_inverted2.png](https://bitbucket.org/repo/poMq7z/images/4244751233-symbol_change_color_vs_inverted2.png)

## To do ##
* n-ship game (late rewards)
* New symbols after trained
* Association 
* Composition (Creative memory) i.e Learning new moves

## Done ##
* Add reflection (Emotion)
* Symbols change meaning (inverted)


## Sample code ##

```
#!java
ArrayList<PairBoolean> percepts = new ArrayList<>();
percepts.add(new PairBoolean(false, false));
percepts.add(new PairBoolean(false, true));
percepts.add(new PairBoolean(true, false));
percepts.add(new PairBoolean(true, true));

ArrayList<Boolean> actions = new ArrayList<>();
actions.add(true);
actions.add(false);

ProjectionSimulationGen<PairBoolean, Boolean> ps = new ProjectionSimulationGen<>(this, percepts, actions, 0.02, 1.0, 2);

ps.train(20);

for (PairBoolean p : percepts) {
    boolean selectedAction = ps.getAction(p);
    System.out.println(p + " = " + selectedAction);
}
```

### Output ###
```
#!java
false,false = false
false,true = true
true,false = true
true,true = false

```