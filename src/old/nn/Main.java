package old.nn;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Oystein on 23/03/15.
 */
public class Main {


    public static interface IGate {

        //        public Unit forward();
        public void backward();
    }

    public static class Unit {
        double value;
        double grad;

        public Unit(double value, double grad) {
            this.value = value;
            this.grad = grad;
        }

        @Override
        public String toString() {
            return String.format("U{v=%.4f , g=%.4f}",value,grad);
        }
    }

    public static class MultiplyGate implements IGate {
        Unit u0, u1, utop;

        public MultiplyGate() {
        }

        public MultiplyGate(Unit u0, Unit u1) {
            this.u0 = u0;
            this.u1 = u1;

        }

        public Unit forward(Unit u0, Unit u1) {
            this.u0 = u0;
            this.u1 = u1;
            this.utop = new Unit(u0.value * u1.value, 0.0);
            return utop;
        }

        public void backward() {
            this.u0.grad += this.u1.value * this.utop.grad;
            this.u1.grad += this.u0.value * this.utop.grad;
        }
    }

    public static class AddGate implements IGate {
        Unit u0, u1, utop;

        public AddGate() {
        }

        public AddGate(Unit u0, Unit u1) {
        }

        public Unit forward(Unit u0, Unit u1) {
            this.u0 = u0;
            this.u1 = u1;
            this.utop = new Unit(u0.value + u1.value, 0.0);
            return utop;
        }

        public void backward() {
            this.u0.grad += 1 * this.utop.grad;
            this.u1.grad += 1 * this.utop.grad;
        }
    }

    public class SigmoidGate implements IGate {
        Unit u0, utop;

        public SigmoidGate() {
        }

        public SigmoidGate(Unit u0) {
            this.u0 = u0;


        }

        public Unit forward(Unit u0) {
            this.u0 = u0;
            this.utop = new Unit(sig(u0.value), 0.0);
            return utop;
        }

        public void backward() {
            double s = sig(u0.value);
            this.u0.grad += (s * (1 - s)) * this.utop.grad;
        }

        public double sig(double x) {
            return 1.0 / (1 + Math.exp(-x));
        }
    }


    public static class Circuit {
        MultiplyGate mulg0 = new MultiplyGate();
        MultiplyGate mulg1 = new MultiplyGate();
        AddGate addg0 = new AddGate();
        AddGate addg1 = new AddGate();
        private Unit ax, by, axpby, axpbypc;

        public Unit forward(Unit x, Unit y, Unit a, Unit b, Unit c) {
            this.ax = this.mulg0.forward(a, x); // a*x
            this.by = this.mulg1.forward(b, y); // b*y
            this.axpby = this.addg0.forward(this.ax, this.by); // a*x + b*y
            this.axpbypc = this.addg1.forward(this.axpby, c); // a*x + b*y + c
            return this.axpbypc;
        }

        public void backward(double gradient_top) {
            this.axpbypc.grad = gradient_top;
            this.addg1.backward(); // sets gradient in axpby and c
            this.addg0.backward(); // sets gradient in ax and by
            this.mulg1.backward(); // sets gradient in b and y
            this.mulg0.backward(); // sets gradient in a and x
        }
    }

    public static class SVM {
        // random initial parameter values
        Unit a = new Unit(1.0, 0.0);
        Unit b = new Unit(-2.0, 0.0);
        Unit c = new Unit(-1.0, 0.0);
        Circuit circuit = new Circuit();
        private Unit unit_out;

        public Unit forward(Unit x, Unit y) {
            this.unit_out = this.circuit.forward(x, y, this.a, this.b, this.c);
            return this.unit_out;
        }

        public void backward(int label) {
            // reset pulls on a,b,c
            this.a.grad = 0.0;
            this.b.grad = 0.0;
            this.c.grad = 0.0;

            // compute the pull based on what the circuit output was
            double pull = 0.0;
            if (label == 1 && this.unit_out.value < 1) {
                pull = 1; // the score was too low: pull up
            }
            if (label == -1 && this.unit_out.value > -1) {
                pull = -1; // the score was too high for a positive example, pull down
            }
            this.circuit.backward(pull); // writes gradient into x,y,a,b,c

            // add regularization pull for parameters: towards zero and proportional to value
            this.a.grad += -this.a.value;
            this.b.grad += -this.b.value;
        }

        public void learnFrom(Unit x, Unit y, int label) {
            forward(x, y);
            backward(label);
            parameterUpdate();
        }

        private void parameterUpdate() {
            double step_size = 0.01;
            this.a.value += step_size * this.a.grad;
            this.b.value += step_size * this.b.grad;
            this.c.value += step_size * this.c.grad;
        }

    }
Random random = new Random(System.currentTimeMillis());

    public void trainSVM2() {
        double[][] data = {
                {1.2, 0.7},
                {-0.3, -0.5},
                {3.0, 0.1},
                {-0.1, -1.0},
                {-1.0, 1.1},
                {2.1, -3},
        };
        int[] labels={1,-1,1,-1,-1,1};

        double a = 1, b = -2, c = -1; // initial parameters
        for(int iter = 0; iter < 4000; iter++) {
            // pick a random data point
            int i = (int) Math.floor(Math.random() * data.length);
            double x = data[i][0];
            double y = data[i][1];
            int label = labels[i];

            // compute pull
            double score = a*x + b*y + c;
            double pull = 0.0;
            if(label == 1 && score < 1) pull = 1;
            if(label == -1 && score > -1) pull = -1;

            // compute gradient and update parameters
            double step_size = 0.01;
            a += step_size * (x * pull - a); // -a is from the regularization
            b += step_size * (y * pull - b); // -b is from the regularization
            c += step_size * (1 * pull);

            if(iter%25==0){

                double num_correct=0;
                for (int j = 0; j < data.length; j++) {
                    double xt = data[j][0];
                    double yt = data[j][1];
                    int labelt = labels[j];

                    double scoret = a*xt + b*yt + c;
                    int predicted_label=scoret >0? 1:-1;
                    boolean isCorrect = (labelt == predicted_label);
                    if(isCorrect)
                        num_correct++;
                    System.out.printf("\tPredict: %s\t: %.2f,%.2f=\t%d\tout=%d (%.4f) \n",isCorrect,data[j][0],data[j][1],labelt,predicted_label,scoret);

                }
                num_correct/=data.length;

                System.out.println(num_correct);

            }
        }

    }
    public void trainSVM() {

        double[][] testCases = {
                {1.2, 0.7},
                {-0.3, -0.5},
                {3.0, 0.1},
                {-0.1, -1.0},
                {-1.0, 1.1},
                {2.1, -3},
        };
        int[] labels={1,-1,1,-1,-1,1};

        SVM svm = new SVM();


        //learning loop
        int[] counter = new int[testCases.length];
        for (int iter = 0; iter < 2000; iter++) {
            int i = random.nextInt( testCases.length);
//            i=iter%testCases.length;

            counter[i]++;
//            if(i==0 && iter>600)
//                System.out.println("ja");
            Unit x = new Unit(testCases[i][0],0.0);
            Unit y = new Unit(testCases[i][1],0.0);
            int label = labels[i];
            svm.learnFrom(x,y,label);

            if(iter%25==0)
                System.out.printf("Training accuracy at iter %d : %.4f\n",iter,evalTrainingAccuracy(testCases,labels,svm));
        }

        evalTrainingAccuracy(testCases,labels,svm,true);

        System.out.println(Arrays.toString(counter));

        int t=0;
        for (int i = 0; i < 100; i++) {
            Unit x = new Unit(testCases[t][0],0.0);
            Unit y = new Unit(testCases[t][1],0.0);
            int true_label = labels[t];

            //learn
            svm.learnFrom(x,y,true_label);

            double predicted_double = svm.forward(x,y).value;
            int predicted_label =predicted_double >0? 1:-1;

            System.out.printf("Predict: %s\t: %.2f,%.2f=\t%d\tout=%d (%.4f) \n",(predicted_label==true_label),testCases[t][0],testCases[t][1],true_label,predicted_label,predicted_double);


        }

    }

    public double evalTrainingAccuracy(double[][] testCases, int[] labels,SVM svm){

        return evalTrainingAccuracy(testCases,labels,svm,false);
    }
    public double evalTrainingAccuracy(double[][] testCases, int[] labels,SVM svm,boolean print){
        double num_correct =0.0;
        for (int i = 0; i < testCases.length; i++) {
            Unit x = new Unit(testCases[i][0],0.0);
            Unit y = new Unit(testCases[i][1],0.0);
            int true_label = labels[i];

            double predicted_double = svm.forward(x,y).value;
            int predicted_label =predicted_double >0? 1:-1;
            if(predicted_label==true_label)
                num_correct++;
            if(print)
                System.out.printf("Predict: %s\t: %.2f,%.2f=\t%d\tout=%d (%.4f) \n",(predicted_label==true_label),testCases[i][0],testCases[i][1],true_label,predicted_label,predicted_double);
        }
        return num_correct/testCases.length;
    }

    public Main() {
//        SigmoidGate sigmoidGate = new SigmoidGate(null);
//        System.out.println(sigmoidGate.sig(-5));
//        System.out.println(sigmoidGate.sig(0));
//        System.out.println(sigmoidGate.sig(5));

//        test();
        trainSVM2();
    }

    public static String p(double x) {
        return String.format("%.4f", x);
    }

    public void test() {

        // create input units
        Unit a = new Unit(1.0, 0.0);
        Unit b = new Unit(2.0, 0.0);
        Unit c = new Unit(-3.0, 0.0);
        Unit x = new Unit(-1.0, 0.0);
        Unit y = new Unit(3.0, 0.0);

// create the gates
        MultiplyGate mulg0 = new MultiplyGate();
        MultiplyGate mulg1 = new MultiplyGate();
        AddGate addg0 = new AddGate();
        AddGate addg1 = new AddGate();
        SigmoidGate sg0 = new SigmoidGate();

        Unit s = forwardNeuron(a, b, c, x, y, mulg0, mulg1, addg0, addg1, sg0);

        System.out.println(p(s.value) + " circuit out: ");

        s.grad = 1.0;
        sg0.backward(); // writes gradient into axpbypc
        addg1.backward(); // writes gradients into axpby and c
        addg0.backward(); // writes gradients into ax and by
        mulg1.backward(); // writes gradients into b and y
        mulg0.backward(); // writes gradients into a and x

        double step_size = 0.01;
        a.value += step_size * a.grad; // a.grad is -0.105
        b.value += step_size * b.grad; // b.grad is 0.315
        c.value += step_size * c.grad; // c.grad is 0.105
        x.value += step_size * x.grad; // x.grad is 0.105
        y.value += step_size * y.grad; // y.grad is 0.210

        s = forwardNeuron(a, b, c, x, y, mulg0, mulg1, addg0, addg1, sg0);
        System.out.println(p(s.value) + " circuit out after one backprop : ");


        vertify();

    }

    public void vertify() {
        double a = 1, b = 2, c = -3, x = -1, y = 3;
        double h = 0.0001;
        double a_grad = (forwardCircuitFast(a + h, b, c, x, y) - forwardCircuitFast(a, b, c, x, y)) / h;
        double b_grad = (forwardCircuitFast(a, b + h, c, x, y) - forwardCircuitFast(a, b, c, x, y)) / h;
        double c_grad = (forwardCircuitFast(a, b, c + h, x, y) - forwardCircuitFast(a, b, c, x, y)) / h;
        double x_grad = (forwardCircuitFast(a, b, c, x + h, y) - forwardCircuitFast(a, b, c, x, y)) / h;
        double y_grad = (forwardCircuitFast(a, b, c, x, y + h) - forwardCircuitFast(a, b, c, x, y)) / h;
        System.out.println();

    }

    public double forwardCircuitFast(double a, double b, double c, double x, double y) {
        return 1 / (1 + Math.exp(-(a * x + b * y + c)));
    }

    private Unit forwardNeuron(Unit a, Unit b, Unit c, Unit x, Unit y, MultiplyGate mulg0, MultiplyGate mulg1, AddGate addg0, AddGate addg1, SigmoidGate sg0) {
        Unit ax = mulg0.forward(a, x);
        Unit by = mulg1.forward(b, y);
        Unit axpby = addg0.forward(ax, by);
        Unit axpbypc = addg1.forward(axpby, c);
        return sg0.forward(axpbypc);
    }

    public double forwardMultiplyGate(double x, double y) {
        return x * y;
    }

    public double forwardAddGate(double x, double y) {
        return x + y;
    }

    public double forwardCircuit(double x, double y, double z) {
        double q = forwardAddGate(x, y);
        double f = forwardMultiplyGate(q, z);
        return f;
    }

    public static void main(String[] args) {
        Main m = new Main();


//        double x = -2;
//        double y = 5;
//        double z = -4;
//
//        double q = m.forwardAddGate(x, y);
//        double f = m.forwardMultiplyGate(q, z);
//
//
//        double derivate_f_wrt_z = q;
//        double derivate_f_wrt_q = z;
//
//        double derivate_q_wrt_x = 1.0;
//        double derivate_q_wrt_y = 1.0;
//
//        //chain rule
//        double derivate_f_wrt_x = derivate_q_wrt_x * derivate_f_wrt_q;
//        double derivate_f_wrt_y = derivate_q_wrt_y * derivate_f_wrt_q;
//        System.out.println();
//
//        double[] gradient_f_wrt_xyz = new double[]{derivate_f_wrt_x, derivate_f_wrt_y, derivate_f_wrt_z};
//
//
//        double step_size = 0.01;
//        x = x + step_size * derivate_f_wrt_x; // -2.04
//        y = y + step_size * derivate_f_wrt_y; // 4.96
//        z = z + step_size * derivate_f_wrt_z; // -3.97
//
//        q = m.forwardAddGate(x, y);
//        f = m.forwardMultiplyGate(q, z);
//        System.out.println(f);

//        double x_gradient = y;
//        double y_gradient = x;
//
//        double step_size = 0.01;
//        x += step_size * x_gradient;
//        y += step_size * y_gradient;
//        double out_new = m.forwardMultiplyGate(x, y);
//
//        System.out.println(out_new);


    }
}
