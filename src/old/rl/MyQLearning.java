package old.rl;

import experiments.mountaincar.MountainCarWorld;
import ps.v1.InterfacePS;
import gui.SimpleSwingGraphGUI;
import utils.IMatrix;
import utils.MatrixArray;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 18/03/15.
 */
public class MyQLearning implements InterfacePS<Integer, Integer> {
    boolean DEBUG_PRINT=false;

    Random random = new Random(System.currentTimeMillis());
    IMatrix table;

    int numStates;
    int numActions;

    int[][] transitions;

    int prevState;
    int prevAction;

    int[] debugOutLearnedState;

    double explorationChance = 0.1d;//0.3d;
    /**
     * Learning rate determines how much new information will overrite old information.
     * 0 Will not learn anything, 1 only consider the old information.
     * Transtion from 1 -> 0
     */
    private double learningRate = 1.0;//0.8d;//0.8d;
    private double learningScale = 2.8;//3.123;
    double gamma = 0.9;//0.6d;

    public MyQLearning(int numStates, int numActions) {
        this.numStates = numStates;
        this.numActions = numActions;
        reset();
    }


    @Override
    public Integer getAction(Integer stateNumber) {
        prevState = stateNumber;
        if (random.nextDouble() < explorationChance) {
            explorationChance *= 0.00001;
            prevAction = random.nextInt(numActions);
        } else
            prevAction = getBestAction(stateNumber);

        return prevAction;
    }

    private int getBestAction(Integer stateNumber) {
        double maxReward = Double.MIN_VALUE;
        int maxAction = -1;
        double[] Qs = table.getActions(stateNumber);
        for (int i = 0; i < numActions; i++) {
            if (Qs[i] > maxReward) {
                maxReward = Qs[i];
                maxAction = i;
            }
        }
        if (maxReward <= 0.000001) {
            if (DEBUG_PRINT)
                System.out.println("No good actions (all 0)" + stateNumber);
            return random.nextInt(numActions);
        }
        if (maxAction == -1) {
            if (DEBUG_PRINT)
                System.out.println("No good actions " + stateNumber);
            return random.nextInt(numActions);
        }
//        if (DEBUG_PRINT)
//            System.out.printf("q(%d,%s)=%.4f \n", stateNumber, MarioAction.getName(maxAction), maxReward);
        return maxAction;
    }



    @Override
    public void giveReward(boolean success) {

    }

    private void updateSrc(double reward, int currentStateNumber) {
        transitions[prevState][prevAction]++;

        double[] prevQs = table.getActions(prevState);
        double prevQ = prevQs[prevAction];

        int bestAction = getBestAction(currentStateNumber);
        double maxQ = table.getActions(currentStateNumber)[bestAction];

        int nTransStoA = transitions[prevState][prevAction];
//        double alpha = calcAlpha(learningRate, transitions[prevState][prevAction]);///(transitions[prevState][prevAction]+1);
//        double alpha = learningRate/nTransStoA;


        double alpha = learningRate / ((nTransStoA + learningScale) / learningScale);
        if (alpha <= 0.0001 && debugOutLearnedState[currentStateNumber] == 0) {
            debugOutLearnedState[currentStateNumber] = 1;
            System.out.println("Out learned.");
        }

//        System.out.printf("alpha=%.4f \n",alpha);
        double newQ = (1 - alpha) * prevQ + alpha * (reward + gamma * maxQ);

        if (DEBUG_PRINT) {
            double learnedValue = (reward * gamma * maxQ);
            System.out.printf("oldQ=%.4f , newQ=%.4f , r=%.4f \n", prevQ, newQ, reward);
            if (reward > 1)
                System.out.printf("%.3f = %.3f + %.3f * (%.3f) \n", newQ, prevQ, alpha, learnedValue);
        }


        prevQs[prevAction] = newQ;

    }

    @Override
    public void useSoftMax(boolean useSoftMax) {

    }

    @Override
    public void reset() {
        this.table=new MatrixArray(numStates,numActions);
        this.transitions = new int[numStates][numActions];
        debugOutLearnedState = new int[numStates];

    }

    @Override
    public String toString() {
        return String.format("Q{damping=%.3f, learning=%.3f , scale=%.3f}", gamma, learningRate, learningScale);
    }


    public static void main(String[] args) {
        int nPos = 1000;
        int nVelo=5;
        MountainCarWorld mcw = new MountainCarWorld(nPos,nVelo);

        MyQLearning qLearning = new MyQLearning(nPos*nVelo,3);


        List<Integer> histo = new ArrayList<>();
        int numGames=2000;
        for (int i = 0; i < numGames; i++) {
            while (!mcw.isGameOver()) {
                int state = mcw.getUniqueState();
                int action = qLearning.getAction(state) - 1;
                mcw.update(action);

                double reward = mcw.isGameOver()?1000:0;
//                reward =  -1 + mcw.getHeightAtPosition(mcw.getPosition());
                qLearning.updateSrc(reward, mcw.getUniqueState());
//                qLearning.giveReward(mcw.isGameOver());

            }
            histo.add(mcw.getStepCounter());
            System.out.println(i + " | " + mcw.getStepCounter() + " steps");
            mcw.reset();

        }

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(histo,"MC world RL",qLearning.toString(),"epoch","steps",0,5000));
//        return histo;

    }
}
