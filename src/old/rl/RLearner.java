package old.rl;

import java.util.Vector;

public class RLearner {

    RLWorld thisWorld;
    RLPolicy policy;

    // Learning types
    public static final int Q_LEARNING = 1;
    public static final int SARSA = 2;
    public static final int Q_LAMBDA = 3; // Good parms were lambda=0.05, damping=0.1, alpha=0.01, epsilon=0.1

    // Action selection types
    public static final int E_GREEDY = 1;
    public static final int SOFTMAX = 2;

    int learningMethod;
    int actionSelection;

    /**
     * Chance of doing a random action, instead of the 'best'
     */
    double epsilon, epsilonCopy;
    double epsilonMulti;
    double temp;

    double alpha;
    double gamma;
    double lambda;

    int[] dimSize;
    int[] state;
    int[] newstate;
    int action;
    double reward;

    int epochs;
//    public int epochsdone;

    //    Thread thisThread;
    public boolean running;

    Vector trace = new Vector();
    int[] saPair;

    long timer;

    boolean random = false;
//    Runnable a;


    public void setParams(double alpha, double gamma, double epsilon, double epsilonMulti, int learningMethod, int actionSelection) {
        this.alpha = alpha;
        this.gamma = gamma;
        this.epsilon = epsilon;
        this.epsilonCopy = epsilon;
        this.epsilonMulti = epsilonMulti;
        this.learningMethod = learningMethod;
        this.actionSelection = actionSelection;
    }

    public void setParam(String param, double v){
        switch (param.toLowerCase()){
            case "alpha": alpha=v; break;
            case "damping": gamma=v; break;
            case "epsilon": epsilon=v; epsilonCopy=v; break;
            case "learningMethod": learningMethod=(int)v; break;
            case "actionSelection": actionSelection=(int)v; break;
            default:
                System.err.println("Unknown parameter "+param);
        }
    }


    public RLearner(RLWorld world, double alpha, double gamma, double epsilon, double epsilonMulti, int learningMethod, int actionSelection) {
        setParams(alpha, gamma, epsilon, epsilonMulti, learningMethod, actionSelection);
        // Getting the world from the invoking method.
        thisWorld = world;

        // Get dimensions of the world.
        dimSize = thisWorld.getDimension();

        // Creating new policy with dimensions to suit the world.
        policy = new RLPolicy(dimSize);

        // Initializing the policy with the initial values defined by the world.
        policy.initValues(thisWorld.getInitValues());


        temp = 1;
        lambda = 0.1;  // For CliffWorld damping = 0.1, l = 0.5 (l*g=0.05)is a good choice.
        running = true;

        System.out.println("RLearner initialised");
    }

    public RLearner(RLWorld world, double alpha, double gamma, double epsilon, int learningMethod, int actionSelection) {
        this(world, alpha, gamma, epsilon, 0.992, learningMethod, actionSelection);

    }

    public RLearner(RLWorld world, double alpha, double gamma, int learningMethod, int actionSelection) {
        this(world, alpha, gamma, 0.03, learningMethod, actionSelection);
    }

    public RLearner(RLWorld world) {
        this(world, 0.1, 0.9, 0.03, SARSA, E_GREEDY);

    }

    // execute one trial
    public void runTrial() {
        System.out.println("Learning! (" + epochs + " epochs)\n");
        for (int i = 0; i < epochs; i++) {
            if (!running) break;

            runEpoch();

            if (i % 1000 == 0) {
                // give text output
                timer = (System.currentTimeMillis() - timer);
                System.out.println("Epoch:" + i + " : " + timer);
                timer = System.currentTimeMillis();
            }
        }
    }




    public void epochSARSA(){
        int newaction;
        double this_Q;
        double next_Q;
        double new_Q;

        action = selectAction(state);
        while (!thisWorld.endState()) {

            if (!running) break;
            if (thisWorld.getStepCounter()>10_000_000)
                System.out.println("Ran out of time.. fuck it stepCounter" + thisWorld.getStepCounter()+ " A="+toString());

            newstate = thisWorld.getNextState(action);
            reward = thisWorld.getReward();

            newaction = selectAction(newstate);

            this_Q = policy.getQValue(state, action);
            next_Q = policy.getQValue(newstate, newaction);

            new_Q = this_Q + alpha * (reward + gamma * next_Q - this_Q);

            policy.setQValue(state, action, new_Q);

            // Set state to the new state and action to the new action.
            state = newstate;
            action = newaction;
        }

        //Good? decrease epsilon (exploration)
        epsilon *= epsilonMulti;
    }
    // execute one epoch
    public void runEpoch() {

        // Reset state to start position defined by the world.
        state = thisWorld.resetState();

        switch (learningMethod) {

            case Q_LEARNING: {
                epochQLearning();
                break;
            }
            case SARSA: {
                epochSARSA();
                break;
            }

            case Q_LAMBDA: {
                epochQLambda();
                break;

            } // case
        } // switch
    } // runEpoch

    private void epochQLambda() {
        double max_Q;
        double this_Q;
        double new_Q;
        double delta;

        // Remove all eligibility traces.
        trace.removeAllElements();

        while (!thisWorld.endState()) {

            if (!running) break;

            action = selectAction(state);

            // Store state-action pair in eligibility trace.
            saPair = new int[dimSize.length];
            System.arraycopy(state, 0, saPair, 0, state.length);
            saPair[state.length] = action;
            trace.add(saPair);

            // Store only 10 traced states.
            if (trace.size() == 11)
                trace.removeElementAt(0);

            newstate = thisWorld.getNextState(action);
            reward = thisWorld.getReward();

            max_Q = policy.getMaxQValue(newstate);
            this_Q = policy.getQValue(state, action);

            // Calculate new Value for Q
            delta = reward + gamma * max_Q - this_Q;
            new_Q = this_Q + alpha * delta;

            policy.setQValue(state, action, new_Q);

            // Update values for the trace.
            for (int e = trace.size() - 2; e >= 0; e--) {

                saPair = (int[]) trace.get(e);

                System.arraycopy(saPair, 0, state, 0, state.length);
                action = saPair[state.length];

                this_Q = policy.getQValue(state, action);
                new_Q = this_Q + alpha * delta * Math.pow(gamma * lambda, (trace.size() - 1 - e));

                policy.setQValue(state, action, new_Q);

                //System.out.println("Set Q:" + new_Q + "for " + state[0] + "," + state[1] + " action " + action );
            }

            if (random) trace.removeAllElements();

            // Set state to the new state.
            state = newstate;


        }
    }

    private void epochQLearning() {
        double this_Q;
        double max_Q;
        double new_Q;

        while (!thisWorld.endState()) {
            // 4 1 1 0 4 4
            if (!running) break;
            action = selectAction(state);
            newstate = thisWorld.getNextState(action);
            reward = thisWorld.getReward();

            this_Q = policy.getQValue(state, action);
            max_Q = policy.getMaxQValue(newstate);

            // Calculate new Value for Q
            new_Q = this_Q + alpha * (reward + gamma * max_Q - this_Q);
            policy.setQValue(state, action, new_Q);

            // Set state to the new state.
            state = newstate;
        }

        //System.out.println(thisWorld.);
    }

    private int selectAction(int[] state) {

        double[] qValues = policy.getQValuesAt(state);

        switch (actionSelection) {
            case E_GREEDY: {
               return actionSelectEGreedy(qValues);
            }
            case SOFTMAX: {
                return actionSelectSoftMax(qValues);
            }
        }
        return -1;
    }

    private int actionSelectSoftMax(double[] qValues) {
        int selectedAction=-1;
        int action;
        double prob[] = new double[qValues.length];
        double sumProb = 0;

        for (action = 0; action < qValues.length; action++) {
            prob[action] = Math.exp(qValues[action] / temp);
            sumProb += prob[action];
        }
        for (action = 0; action < qValues.length; action++)
            prob[action] = prob[action] / sumProb;

        boolean valid = false;
        double rndValue;
        double offset;

        while (!valid) {

            rndValue = Math.random();
            offset = 0;

            for (action = 0; action < qValues.length; action++) {
                if (rndValue > offset && rndValue < offset + prob[action])
                    selectedAction = action;
                offset += prob[action];
                // System.out.println( "Action " + action + " chosen with " + prob[action] );
            }

//                    if (thisWorld.validAction(selectedAction))
                valid = true;
        }
        return selectedAction;
    }

    private int actionSelectEGreedy(double[] qValues) {
        int selectedAction=-1;
        double maxQ = -Double.MAX_VALUE;
        int[] doubleValues = new int[qValues.length];
        int maxDV = 0;

        //Explore
        if (Math.random() < epsilon) {
            return (int) (Math.random() * qValues.length);
        } else {

            for (int action = 0; action < qValues.length; action++) {

                if (qValues[action] > maxQ) {
                    selectedAction = action;
                    maxQ = qValues[action];
                    maxDV = 0;
                    doubleValues[maxDV] = selectedAction;
                } else if (qValues[action] == maxQ) {
                    maxDV++;
                    doubleValues[maxDV] = action;
                }
            }

            if (maxDV > 0) {
                int randomIndex = (int) (Math.random() * (maxDV + 1));
                selectedAction = doubleValues[randomIndex];
            }
        }

        // Select random action if all qValues == 0 or exploring.
        if (selectedAction == -1) {
            selectedAction = (int) (Math.random() * qValues.length);
        }

        return selectedAction;
    }
    
    /* private double getMaxQValue( int[] state, int action ) {

	double maxQ = 0;
	
	double[] qValues = policy.getQValuesAt( state );
	
	for( action = 0 ; action < qValues.length ; action++ ) {
	    if( qValues[action] > maxQ ) {
		maxQ = qValues[action];
	    }
	}
	return maxQ;
    }
    */


    public RLPolicy getPolicy() {

        return policy;
    }

    public void setAlpha(double a) {

        if (a >= 0 && a < 1)
            alpha = a;
    }

    public double getAlpha() {

        return alpha;
    }

    public void setGamma(double g) {

        if (g > 0 && g < 1)
            gamma = g;
    }

    public double getGamma() {

        return gamma;
    }

    public void setEpsilon(double e) {

        if (e > 0 && e < 1)
            epsilon = e;
    }

    public double getEpsilon() {

        return epsilon;
    }

    public void setEpisodes(int e) {

        if (e > 0)
            epochs = e;
    }

    public int getEpisodes() {

        return epochs;
    }

    public void setActionSelection(int as) {

        switch (as) {

            case SOFTMAX: {
                actionSelection = SOFTMAX;
                break;
            }
            case E_GREEDY:
            default: {
                actionSelection = E_GREEDY;
            }

        }
    }

    public int getActionSelection() {

        return actionSelection;
    }

    public void setLearningMethod(int lm) {

        switch (lm) {

            case SARSA: {
                learningMethod = SARSA;
                break;
            }
            case Q_LAMBDA: {
                learningMethod = Q_LAMBDA;
                break;
            }
            case Q_LEARNING:
            default: {
                learningMethod = Q_LEARNING;
            }
        }
    }

    @Override
    public String toString() {
        String lm = "";
        switch (learningMethod) {
            case SARSA:
                lm = "SARSA";
                break;
            case Q_LAMBDA:
                lm = "Q_LAMDBDA";
                break;
            case Q_LEARNING:
                lm = "Q_LEARNING";
                break;
        }
        String as = "";
        switch (actionSelection) {

            case SOFTMAX: {
                as = "SOFTMAX";
                break;
            }
            case E_GREEDY:
                as = "GREEDY";
                break;

        }
        String s = ", " + lm + ", " + as;

        return String.format("RL {a=%.4f, γ=%.4f, ε=%.4f, %s, %s", alpha, gamma, epsilonCopy, lm, as);

//        return String.format("RLeaner {alpha=%.4f, damping=%.4f, lambda=%.2f, epsilon=%.4f, %s, %s", alpha, damping, lambda, epsilonCopy, lm, as);

//        return "RLearner {" +
//                "alpha=" + alpha +
//                ", damping=" + damping +
//                ", lambda=" + lambda +
//                s + '}';
    }

    public int getLearningMethod() {

        return learningMethod;
    }

    //AK: let us clear the policy
    public RLPolicy newPolicy() {
        policy = new RLPolicy(dimSize);
        // Initializing the policy with the initial values defined by the world.
        policy.initValues(thisWorld.getInitValues());
        return policy;
    }
}

