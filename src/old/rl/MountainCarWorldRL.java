package old.rl;

import experiments.mountaincar.MountainCarWorld;

/**
 * Created by Oystein on 17/11/14.
 */
public class MountainCarWorldRL extends MountainCarWorld implements RLWorld {

    public static int MAX_STEPS=100_000;
    double widthPos, widthVelo;
    int n;
    int[] dimSize;

    public MountainCarWorldRL(int n ) {
        super();//Start at bottom.
       this.n=n;
        dimSize = new int[]{n, n, 3};
        //20 20 3
        widthPos = (Math.abs(MountainCarWorld.MIN_POSITION) + MountainCarWorld.MAX_POSITION) / n;
        widthVelo = (Math.abs(MountainCarWorld.MIN_VELOCITY) + MountainCarWorld.MAX_VELOCITY) / n;
        System.out.println(widthPos);


//        loop();

    }


    public int getXAsArr(double pos) {
        int count = -1;
        for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION - 0.001; i += widthPos) {
            if (i > pos)
                return count;
            count++;
        }

        if (count < 0 || count >= n)
            System.out.println("WTF");
        return count;
    }

    public int getYAsArr(double velo) {
        int count = -1;
        for (double i = MountainCarWorld.MIN_VELOCITY; i < MountainCarWorld.MAX_VELOCITY - 0.001; i += widthVelo) {

            if (i > velo)
                return count;
            count++;
        }
        if (count < 0 || count >= n)
            System.out.println("WTF");
        return count;
    }

    @Override
    public String toString() {
        String s = String.format("[%d][%d]", getXAsArr(getPosition()), getYAsArr(getVelocity()));
        return super.toString() + " " + s;
    }

    @Override
    public int[] getDimension() {
        return dimSize;
    }

    @Override
    public int[] getNextState(int action) {

        update(action-1);
        return new int[]{getXAsArr(getPosition()), getYAsArr(getVelocity())};
    }

    @Override
    public double getReward() {
        return isGameOver() ? 100 : -1;
    }

//    @Override
//    public boolean validAction(int action) {
//        return true;
//    }

    @Override
    public boolean endState() {
        if(getStepCounter()>=MAX_STEPS) {
            System.out.println("Ran out of steps. IN MCWORLDRL.java endState()");
            return true;
        }

        return isGameOver();
    }

    @Override
    public int[] resetState() {
        reset();
        return getState();
    }

    @Override
    public double getInitValues() {
        return 0.0;
    }

    public int[] getState() {
        return new int[]{getXAsArr(getPosition()), getYAsArr(getVelocity())};
    }


//    public CoordinatePair calcNextState(int action) {
//        update(action);
////        System.out.println(toString());
//
//        return getState();
//
//    }

    public static void main(String[] args) {
        MountainCarWorldRL m = new MountainCarWorldRL(20);

    }
}
