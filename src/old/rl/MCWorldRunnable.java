package old.rl;

import gui.SimpleSwingGraphGUI;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import utils.MyUtils;
import utils.Result;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by Oystein on 17/11/14.
 */
public class MCWorldRunnable {
    public MCWorldRunnable(boolean simple) {

    }

    public MCWorldRunnable() {


//       train(learner,epochs);


        long start = System.currentTimeMillis();
//        bruteForceParameters();

        goodSolution();
//        goodSolution2();
        long elapsed = System.currentTimeMillis() - start;
        String s = String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes(elapsed), TimeUnit.MILLISECONDS.toSeconds(elapsed) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsed)));

        System.out.printf(s);



        System.out.println("Done");
    }

    private void goodSolution() {
        MountainCarWorldRL world = new MountainCarWorldRL(10);
        int epochs = 400;
        int numAgents = 20;
        //        RLearner learner = new RLearner(new MountainCarWorldRL(10));

        // 0Result{name='RLeaner {alpha=0.1200, damping=0.9700, lambda=0.30, SARSA, GREEDY', steps=146.33}

        //0Result{name='RLeaner {alpha=0.1100, damping=0.8740, lambda=0.30, epsilon=0.0010, SARSA, GREEDY', steps=178.05}
        RLearner learner = new RLearner(world, 0.12, 0.953, RLearner.Q_LAMBDA, RLearner.E_GREEDY);
//        RLearner learner=new RLearner(world,0.13,0.8860,RLearner.SARSA,RLearner.E_GREEDY);
        learner.lambda = .3;
        learner.epsilon = 0.001;
        System.out.println(learner);
        double[] histo = createLearnersWithHisto(numAgents, epochs, learner);

        for (int i = 0; i < histo.length; i++) {
            System.out.println(i+" "+histo[i]);
        }
//        double avg = createLearners(numAgents, epochs, learner, false);

//        train(learner,epochs,true);
//        System.out.println("Average : " + avg);
    }
    private void goodSolution2() {
        MountainCarWorldRL world = new MountainCarWorldRL(10);
        int epochs = 400;
        int numAgents = 100;
        //        RLearner learner = new RLearner(new MountainCarWorldRL(10));

        // 0Result{name='RLeaner {alpha=0.1200, damping=0.9700, lambda=0.30, SARSA, GREEDY', steps=146.33}

        //0Result{name='RLeaner {alpha=0.1100, damping=0.8740, lambda=0.30, epsilon=0.0010, SARSA, GREEDY', steps=178.05}
        RLearner learner = new RLearner(world, 0.11, 0.8740, RLearner.SARSA, RLearner.E_GREEDY);
//        RLearner learner=new RLearner(world,0.13,0.8860,RLearner.SARSA,RLearner.E_GREEDY);
        learner.lambda = .3;
        learner.epsilon = 0.001;
        System.out.println(learner);
        double avg = createLearners(numAgents, epochs, learner, false);
        System.out.println("Average : " + avg);
    }

    public int train(RLearner learner, int epochs, boolean print) {

        int lastStepCount = 0;
//        System.out.println("Ecocks : "+epochs);
        for (int i = 0; i < epochs; i++) {

//            learner.epsilon *= 0.992;
            learner.runEpoch();


            if (/*i % (epochs / 5) == 0 &&*/ print)
                System.out.println(i + " Steps " + learner.thisWorld.getStepCounter());
            else if (i > epochs - 2 && print) {
                System.out.println("\t" + i + " Steps " + learner.thisWorld.getStepCounter());

            }
            lastStepCount = learner.thisWorld.getStepCounter();

        }
        return lastStepCount;

    }

    public int[] trainWithHisto(RLearner learner, int epochs) {

        int[] arr = new int[epochs];
        learner.running = true;

        for (int i = 0; i < epochs; i++) {

            learner.epsilon *= 0.992;
            learner.runEpoch();

            arr[i] = learner.thisWorld.getStepCounter();


        }
        return arr;

    }


    public XYSeries createXYSeries(int numAgents, int epochs, RLearner learner) {
        double[] arr = createLearnersWithHisto(numAgents, epochs, learner);
        XYSeries series = new XYSeries(learner.toString());
        for (int i = 0; i < arr.length; i++) {
            series.add(i, arr[i]);
        }
        System.out.println(learner + " last epoch: " + arr[arr.length - 1] + " steps");
        return series;
    }

    public double[] createLearnersWithHisto(int amount, int epochs, RLearner learner) {
        double[] stepsAvg = new double[epochs];
        for (int i = 0; i < amount; i++) {

            learner.getPolicy().initValues(learner.thisWorld.getInitValues());
            int[] steps = trainWithHisto(learner, epochs);
            for (int j = 0; j < steps.length; j++) {
                stepsAvg[j] += steps[j];
            }
        }

        for (int i = 0; i < stepsAvg.length; i++) {
            stepsAvg[i] /= amount;
        }

        return stepsAvg;
    }

    public double createLearners(int amount, int epochs, RLearner learner, boolean print) {

        double avg = 0;
        for (int i = 0; i < amount; i++) {

            learner.getPolicy().initValues(learner.thisWorld.getInitValues());
            //learner.policy.
            if (print)
                System.out.println("\tAgent: " + i);


            avg += train(learner, epochs, print);
        }
        avg /= (double) amount;

        if (print) System.out.println("\tAverage of " + amount + " agents after " + epochs + " epochs : " + avg);
        return avg;
    }


    public void epsilonDebugFindPara() {
        for (int i = 0; i < 100; i++) {

            double epsilon = 0.002 * Math.pow(0.98, i);
//            System.out.println(String.format("%.6f", epsilon));
        }

        double epsilonStatic = 0.002;
        for (int i = 0; i < 100; i++) {
            epsilonStatic *= 0.992;
            System.out.println(String.format("%.6f", epsilonStatic));
        }


    }

    public XYSeries makeGraphFromParamters(String para, double[] values, int epochs, int numAgents) {
        System.out.println("Bruteforce epochs=" + epochs + ", avg of " + numAgents + " agents. On parameter " + para + " values=" + Arrays.toString(values));
        MountainCarWorldRL world = new MountainCarWorldRL(8);
        RLearner learner = new RLearner(world);
        //default paras

        learner.lambda = .3;

        double alpha = 0.11;
        double epsilon = 0.03;
        double gamma = 0.95;

        int lm = 2;
        int as = 1;
        learner.setParams(alpha, gamma, epsilon, 0.992, lm, as);

        XYSeriesCollection collection = new XYSeriesCollection();
        XYSeries series = new XYSeries(para);
        for (double v : values) {//0.015 ish

            learner.setParam(para, v);
            double steps = createLearners(numAgents, epochs, learner, false);

            Result result = new Result(learner.toString(), steps);
            System.out.println(result);
//            results.add(result);
            series.add(v, steps);

        }
        collection.addSeries(series);

        String header = String.format("Mountain Car. avg of %d agents. Epochs=%d , %s",numAgents,epochs,learner.toString());

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Epoch", "Steps", 0, 250));


        return series;
    }


    public void bruteForceParameters() {
        int epochs = 400;
        int numAgents = 100;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 100;




        //ALPHA - Learning rate. 0=not learn anything, 1=only consider new information.
        collection.addSeries(makeGraphFromParamters("alpha", MyUtils.generateRange(0.01, 0.50, numPoints),epochs,numAgents));
        //Gamma - 0- only consider current rewards, 1=strive for long term reward.
        collection.addSeries(makeGraphFromParamters("damping", MyUtils.generateRange(0.85, 0.99, numPoints),epochs,numAgents));
        collection.addSeries(makeGraphFromParamters("epsilon", MyUtils.generateRange(0.0, 0.1, numPoints),epochs,numAgents));


        String header = String.format("Mountain Car. avg of %d agents. Epochs=%d",numAgents,epochs);
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Epoch", "Steps", 0, 250));


    }


    public void bruteForceParametersOld() {


        int epochs = 400;
        int numAgents = 50;


//       train(learner,epochs);


        System.out.println("Bruteforce epochs=" + epochs + ", avg of " + numAgents + " agents.");

        ArrayList<Result> results = new ArrayList<>();
        int[] worldSizesArr = {6, 8, 10, 15, 20, 30, 50, 100};
        worldSizesArr = new int[]{8};

        XYSeriesCollection collection = new XYSeriesCollection();
//        for (int worldTabSize = 8; worldTabSize < 100; worldTabSize += 8) {
        for (Integer worldTabSize : worldSizesArr) {
            MountainCarWorldRL world = new MountainCarWorldRL(worldTabSize);
            RLearner learner = new RLearner(world);
            learner.lambda = .3;
            learner.epsilon = 0.01;
            System.out.println("\t Current world size: " + worldTabSize);

            for (double alpha = 0.11; alpha <= 0.15; alpha += 0.01) {//0.015 ish


                for (double gamma = 0.85; gamma <= 1.0; gamma += 0.05) {//0.099

                    for (double epsilonLoop = 0.00001; epsilonLoop < 0.09; epsilonLoop += 0.005) {//8 loops

//                        0Result{name='RLeaner {alpha=0.1100, damping=0.8740, lambda=0.30, epsilon=0.0010, SARSA, GREEDY', steps=178.05}
//                        * 1Result{name='RLeaner {alpha=0.1100, damping=0.9460, lambda=0.30, epsilon=0.0000, SARSA, GREEDY', steps=179.85}
//                        * 2Result{name='RLeaner {alpha=0.1050, damping=0.8620, lambda=0.30, epsilon=0.0030, SARSA, GREEDY', steps=180.60}
                        int lm = 2;
                        int as = 1;
//                for (int lm = 1; lm <= 3; lm++) {//learning methods ( 1=Q_LEARNING, 2=SARSA, 3=Q_LAMBDA)


//                    for (int as = 1; as <= 1; as++) {//action selection (1=Greedy,2=softmax (sucks))


                        learner.setParams(alpha, gamma, epsilonLoop, 0.992, lm, as);
                        double steps = createLearners(numAgents, epochs, learner, false);

//                        System.out.println(steps+" "+learner);
                        Result result = new Result(learner.toString() + " TabSize :" + worldTabSize, steps);
                        System.out.println(result);
                        results.add(result);

                    }
//                }
//                }

                }

            }
        }
        System.out.println("DOne.");


        Collections.sort(results);
        for (int i = 0; i < 10; i++) {
            System.out.println(i + "" + results.get(i));
        }


        //createLearners(numAgents,epochs, learner, false);

        //Best: RLearner {alpha=0.7, damping=1.0, lambda=0.1, SARSA, GREEDY} : 473.2 100x100
        //Best: RLearner {alpha=0.7999999999999999, damping=1.0, lambda=0.1, SARSA, GREEDY} : 721.6 200x200
    }

    /**
     * 0Result{name='RLeaner {alpha=0.1010, damping=0.9310, lambda=0.30, epsilon=0.0012, SARSA, GREEDY', steps=175.25} epsilon: .2*0.992^i?
     * <p>
     * 0Result{name='RLeaner {alpha=0.1100, damping=0.8740, lambda=0.30, epsilon=0.0010, SARSA, GREEDY', steps=178.05}
     * 1Result{name='RLeaner {alpha=0.1100, damping=0.9460, lambda=0.30, epsilon=0.0000, SARSA, GREEDY', steps=179.85}
     * 2Result{name='RLeaner {alpha=0.1050, damping=0.8620, lambda=0.30, epsilon=0.0030, SARSA, GREEDY', steps=180.60}
     * <p>
     * 100 Epochs
     * 0Result{name='RLeaner {alpha=0.1300, damping=0.8860, lambda=0.30, SARSA, GREEDY', steps=176.05}
     * 1Result{name='RLeaner {alpha=0.1350, damping=0.9820, lambda=0.30, SARSA, GREEDY', steps=186.40}
     * <p>
     * Bruteforce epochs=400, avg of 3 agents.
     * 0Result{name='RLeaner {alpha=0.1200, damping=0.9700, lambda=0.30, SARSA, GREEDY', steps=146.33}
     * 0Result{name='RLeaner {alpha=0.0800, damping=0.8300, lambda=0.30, SARSA, GREEDY', steps=147.66666666666666}
     * <p>
     * 0Result{name='RLearner {alpha=0.10010000000000001, damping=0.9000000000000001, lambda=0.3, SARSA, GREEDY}', steps=149.66666666666666}
     * 1Result{name='RLearner {alpha=0.050100000000000006, damping=0.8, lambda=0.3, Q_LEARNING, GREEDY}', steps=156.33333333333334}
     * 2Result{name='RLearner {alpha=0.10010000000000001, damping=0.75, lambda=0.3, Q_LEARNING, GREEDY}', steps=163.33333333333334}
     */

    public static void main(String[] args) {
        new MCWorldRunnable();
    }


}
