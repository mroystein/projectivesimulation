package old.tic;

import java.util.Scanner;

/**
 * Created by Oystein on 23/10/14.
 */
public class RunTicTac {


    public RunTicTac(){

        TicTacToeWorld game = new TicTacToeWorld();

        System.out.println(game.isBoardFull());
        System.out.println(game.checkForWin());

        while(!game.isBoardFull() || !game.checkForWin()){

            Scanner sc = new Scanner(System.in);
            int x = sc.nextInt();
            int y = sc.nextInt();

            System.out.println(x+","+y);

            boolean isOkMove=game.placeMark(x,y);
            while(!isOkMove){
                System.out.println("Invalid move, try again:");
                isOkMove=game.placeMark(sc.nextInt(),sc.nextInt());
            }

            game.printBoard();

            game.changePlayer();

        }

        System.out.println("Game is over");

    }


    public static void main(String[] args){
        new RunTicTac();

    }
}
