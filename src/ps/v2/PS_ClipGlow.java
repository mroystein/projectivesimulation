package ps.v2;

import experiments.ILearningAlgorithm;
import experiments.IWorld;
import ps.v1.InterfacePS;
import utils.MyUtils;
import utils.entities.Pair;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 *
 * With edge glow.
 * <p>
 * <p>
 * Created by Oystein on 02/10/2014.
 */
public class PS_ClipGlow extends PS_MatrixSimple {




    public PS_ClipGlow(int numPercepts, int numActions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;


        h = new double[numPercepts][numActions];
        g = new double[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];
//        emotions = new int[percepts.size()][actions.size()];

        reset();
    }

    public PS_ClipGlow(IWorld world, double gamma, double rewardSuccess, int reflectionTime, double damperGow, int actionSelection) {
        this.world = world;
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;
        this.actionSelection = actionSelection;


        h = new double[world.getNumberOfStates()][world.getNumberOfActions()];
        g = new double[world.getNumberOfStates()][world.getNumberOfActions()];
        lastTimeUsed = new int[world.getNumberOfStates()][world.getNumberOfActions()];
//        emotions = new int[percepts.size()][actions.size()];

        reset();
    }

    public PS_ClipGlow(IWorld world, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this(world, gamma, rewardSuccess, reflectionTime, damperGow, SOFTMAX);
    }

    public PS_ClipGlow() {
    }

    @Override
    public int getAction(int stateNumber) {
        return getAction(new Integer(stateNumber));
    }

    @Override
    public void update(double reward, int currentStateNumber) {
        giveRewardGeneric(reward);
    }

    public void reset() {
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                g[i][j] = 0.0;
                lastTimeUsed[i][j] = 0;
            }
        }

    }

    @Override
    public void runOneEpoch() {
        world.reset();
        while (!world.hasGameEnded()) {
            int action = getAction(world.getState());
            world.doAction(action);
            giveRewardGeneric(world.getReward());
        }
        epislon*=0.99;

    }

    @Override
    public IWorld getWorld() {
        return world;
    }

    @Override
    public void setParameter(String parameter, double value) {
        switch (parameter.toLowerCase()) {

            case "damperglow":
                this.damperGow = value;
                break;
            case "damping":
                gamma = value;
                break;
            default:
                System.err.println("Unknown parameter " + parameter);
        }
    }

    @Override
    public Integer getAction(Integer percept) {
        //Runtime : O(|actions|^2)
        int index = percept;

        if(actionSelection==E_GREEDY){
            if (random.nextDouble() < epislon)
                return random.nextInt(h[0].length);

            int i = MyUtils.eGreedy(h[percept]);
            Pair pair = new Pair(index, i);

            if (!exitedEdges.contains(pair)) {
                exitedEdges.addFirst(pair);
            } else {
                Pair p2 = exitedEdges.get(exitedEdges.indexOf(pair));
                g[p2.a][p2.b] = 1.0;
            }
            //new
            lastSelectedPerc = index;
            lastSelectedAction = i;
            lastTimeUsed[index][i] = currentTimeStep;
            currentTimeStep++;

            return i;
        }



//        Collections.binarySearch(percepts,percept);

        //double p = ProjectionSimulationGen.random.nextDouble();
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[index].length; i++) {

            cumulativeProbability += getHoppingPropability(index, i);
            if (p <= cumulativeProbability) {

                g[index][i] = 1.0;

                Pair pair = new Pair(index, i);

                if (!exitedEdges.contains(pair)) {
                    exitedEdges.addFirst(pair);
                } else {
                    Pair p2 = exitedEdges.get(exitedEdges.indexOf(pair));
                    g[p2.a][p2.b] = 1.0;
                }


                //new
                lastSelectedPerc = index;
                lastSelectedAction = i;
                lastTimeUsed[index][i] = currentTimeStep;
                currentTimeStep++;

                return i;
            }
        }


        System.out.println("no actions. Should never happen.");
        return null;
    }


    public int biggestListSeen = 1;


    public void giveRewardGeneric(double reward) {
        if (gamma <= 0)
            giveRewardList(reward);
        else giveRewardLoop(reward);

    }

    //From PS
    @Override
    public void giveReward(boolean success) {
        giveRewardGeneric(success ? rewardSuccess : 0);
//        giveRewardOld(success);
    }

    public void giveRewardList(double reward) {
        if (exitedEdges.size() > 10000)
            System.err.println("Exited edges size : " + exitedEdges.size() + ". Not good!!");

        if (exitedEdges.size() > biggestListSeen) {
//            System.out.println("\t"+biggestListSeen+" exited edges");
            biggestListSeen = exitedEdges.size();
        }

        //g[lastSelectedPerc][lastSelectedAction] = 1.0;

        Iterator<Pair> it = exitedEdges.iterator();
        while (it.hasNext()) {
            Pair p = it.next();
            int i = p.a;
            int j = p.b;

            double oldH = h[i][j];
            h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];
            double newH = h[i][j];

            if (reward < 0 && newH > 0) {
//                System.out.println("debug");
            }
            g[i][j] = g[i][j] - damperGow * g[i][j];

            if (g[i][j] < DAMPER_GLOW_LOW) {
                g[i][j] = 0.0;
                it.remove();
            }
            if (h[i][j] < 1.0) h[i][j] = 1.0;
        }

    }

    public void giveRewardLoop(double reward) {
        if (reward < 0) reward = 0;
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {

                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];
                g[i][j] = g[i][j] - damperGow * g[i][j];

                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }

    double epislon = 0.01;

    /**
     * E_Greedy Action Selection
     *
     * @param percept
     * @return
     */
    private int E_Greedy(int percept) {
        if (random.nextDouble() < epislon)
            return random.nextInt(h[0].length);
        return MyUtils.eGreedy(h[percept]);

    }

    @Override
    public void useSoftMax(boolean useSoftMax) {
        actionSelection = useSoftMax ? SOFTMAX : PS_SIMPLE;
    }

    private double getHoppingPropability(int perceptInd, int actionInd) {
        switch (actionSelection) {
            case PS_SIMPLE:
                return getHoppingPropabilityFormulaOne(perceptInd, actionInd);

            case SOFTMAX:
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);

            case E_GREEDY:

                return E_Greedy(perceptInd)==actionInd?1.0:0.0;
            default:
                System.err.println("No Action policy selected. Doing SoftMax");
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
        }
    }

    private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }

    @Override
    public String toString() {
        String as = ILearningAlgorithm.toStringActionSelection(actionSelection);
        return "PS: γ=" + gamma + ", |S|=" + h.length + ",λ=" + rewardSuccess + ", R=" + reflectionTime + ", g=" + damperGow + " " + as;
    }

}
