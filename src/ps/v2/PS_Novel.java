package ps.v2;

import experiments.ILearningAlgorithm;
import experiments.IWorld;
import utils.MyUtils;
import utils.entities.Pair;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * A matrix implementation of PS ECM.
 * The clip network are represented as a matrix.
 * <p>
 * With edge glow.
 * Features:
 * - Damping
 * - Emotion (Reflection)
 * - Only Direct Walks
 * - Fast
 * <p>
 * <p>
 * <p>
 * I think it works..
 * Created by Oystein on 02/10/2014.
 */
public class PS_Novel implements ILearningAlgorithm {

    protected Random random = new Random(System.currentTimeMillis() + new Random().nextInt());
    protected IWorld world;
    protected double rewardSuccess;

    /**
     * Damping. How much to forget/remember from previous.
     */
    protected double damping;

    /**
     * How many times agent can reflect. 1=None. Must be >=1
     */
    protected int maxReflection;
    protected int currReflection;

    /**
     * Action selection policy. Default is SoftMax.
     */
    protected int actionSelection = SOFTMAX;//default

    double h[][];
    boolean[][] emotion;

    protected int lastSelectedPerc = 0, lastSelectedAction = 0;

    protected int[][] lastTimeUsed;
    protected int currentTimeStep;

    protected double epsilon = 0.01;

    public boolean EXPERIMENTAL=false;
    public double learningRate=0.25;
    public double gammaFuture =0.9;

    public PS_Novel(int numPercepts, int numActions, double damping, double rewardSuccess, int maxReflection) {
        this.rewardSuccess = rewardSuccess;
        this.damping = damping;
        this.maxReflection = maxReflection;

        h = new double[numPercepts][numActions];
        emotion = new boolean[numPercepts][numActions];
        lastTimeUsed = new int[numPercepts][numActions];
        reset();
        validateParamters();

    }

    public PS_Novel(IWorld world, double damping, double rewardSuccess, int maxReflection, int actionSelection) {
        this(world.getNumberOfStates(), world.getNumberOfActions(), damping, rewardSuccess, maxReflection);
        this.world = world;
        this.actionSelection = actionSelection;
    }

    public PS_Novel(IWorld world, double damping, double rewardSuccess) {
        this(world, damping, rewardSuccess, 1, SOFTMAX);
    }

    public PS_Novel(IWorld world) {
        this(world, 0.0, -1);
    }

    @Override
    public int getAction(int percept) {
        double p = random.nextDouble();
        currReflection++;
        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[percept].length; i++) {

            cumulativeProbability += getHoppingPropability(percept, i);
            if (p <= cumulativeProbability) {

                //If negative edge, reflection is on, and bigger than current reflection. Re-select.
                if (currReflection < maxReflection && !emotion[percept][i])
                    return getAction(percept);

                //Integer.MAX_VALUE
                //new
                lastSelectedPerc = percept;
                lastSelectedAction = i;
                lastTimeUsed[percept][i] = currentTimeStep;
                currentTimeStep++;

                return i;
            }
        }
        return -1;
    }

    @Override
    public void update(double reward, int currentStateNumber) {
        currReflection = 0;

      giveRewardLoop(reward);
    }

    @Override
    public void reset() {
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                lastTimeUsed[i][j] = 0;
                emotion[i][j] = true;
            }
        }

    }

    private void validateParamters() {
        if (damping < 0 && damping > 1)
            System.err.println("Invalid damping" + damping);

        if (maxReflection < 1)
            System.err.println("Reflection must >=1 , not " + maxReflection);

    }




    @Override
    public void runOneEpoch() {
        world.reset();
        while (!world.hasGameEnded()) {
            int action = getAction(world.getState());
            world.doAction(action);
            update(world.getReward(), 0);
        }
        epsilon *= 0.99;

    }

    @Override
    public IWorld getWorld() {
        return world;
    }

    @Override
    public void setParameter(String parameter, double value) {
        switch (parameter.toLowerCase()) {

            case "damping":
                damping = value;
                break;
            case "learningrate":
                learningRate=value;
                break;
            case "gammafuture":
                gammaFuture=value;
                break;
            default:
                System.err.println("Unknown parameter " + parameter);
        }
    }


    /**
     * Give reward by looping Matrix.
     * Takes longer time than the list method, but need this if damper is >0, since ALL cells get updated.
     *
     * @param reward
     */
    public void giveRewardLoop(double reward) {

        reward = setEmotionAndGetAdaptiveReward(reward);
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {

                if(i==lastSelectedPerc && j==lastSelectedAction){
                    h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward;
                }else{
                    h[i][j] = h[i][j] - damping * (h[i][j] - 1);
                }


                if (h[i][j] < 1.0) h[i][j] = 1.0;
            }
        }
    }


    /**
     * 1. Set the emotion tag.
     * 2. Decides if negative rewards are allowed.
     *
     * @param reward from environment.
     * @return new reward.
     */
    private double setEmotionAndGetAdaptiveReward(double reward) {
        boolean wasSuccess = reward > 0;
        emotion[lastSelectedPerc][lastSelectedAction] = wasSuccess;

        if (rewardSuccess < 0) {
            return reward;
        }
        if (reward < 0)
            return 0;
        return reward;
    }

    /**
     * If damping paramter is 0, there is no need to loop the whole matrix. Only update the recently used!
     *
     * @param reward
     */


    /**
     * E_Greedy Action Selection
     *
     * @param percept
     * @return
     */
    private int E_Greedy(int percept) {

        if (random.nextDouble() < epsilon)
            return random.nextInt(h[0].length);
        return MyUtils.eGreedy(h[percept]);

    }

    private double getHoppingPropability(int perceptInd, int actionInd) {
        switch (actionSelection) {
            case PS_SIMPLE:
                return getHoppingPropabilityFormulaOne(perceptInd, actionInd);

            case SOFTMAX:
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
            case E_GREEDY:

                return E_Greedy(perceptInd) == actionInd ? 1.0 : 0.0;
            default:
                System.err.println("No Action policy selected. Doing SoftMax");
                return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);
        }
    }

    private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }


    @Override
    public String toString() {
        return String.format("PS-S: γ=%.4f",damping);




//        return "PS: γ=" + damping + ", |S|=" + h.length + ",λ=" + rewardSuccess + ", R=" + maxReflection + ", g=" + glow + " " + as;
    }


    public static void main(String[] args) {
//        IWorld world1;
//        world1 = GridWorldGen.parseFile(GridWorldGen.BOARD_20x20);
//        PS ps = new PS(world1, 0.0, -1, 0.001);
//        System.out.println(ps);
//
//
//        PS ps2 = new PS(world1, 0.0, -1, 0.001);
//        System.out.println(ps2);


        double h, gamma, reward, g;
        for (int i = 0; i < 10; i++) {

            h = Math.random() * 50 + 100;
            gamma = Math.random();
            reward = Math.random() * 123 + 10;
            g = Math.random();

            double t1 = t1(h, gamma, reward, g);
            double t2 = t2(h, gamma, reward, g);
            System.out.println(t1);
            System.out.println(t2);
            System.out.println(t1==t2);
            System.out.println();
        }

        System.out.println(t1(2,0.1,1,1));
        System.out.println(t2(2, 0.1, 1, 1));
    }
    @Override
    public double getValue(int state, int action) {
        return h[state][action];
    }

    public static double t1(double h, double gamma, double reward, double g) {
        h = h - gamma * (h - 1) + reward * g;
        return h;
        // h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];
    }

    public static double t2(double h, double gamma, double reward, double g) {
        h += gamma * (h - 1) + reward * g;
        return h;
        // h[i][j] = h[i][j] - damping * (h[i][j] - 1) + reward * g[i][j];
    }
}
