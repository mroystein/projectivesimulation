package ps.v1;

import java.lang.reflect.Array;

/**
 * Created by Oystein on 11/11/14.
 */
public class MultiDimArr {

    private int[] dimSize;


    private Object arr;
    private int numStates;
    private final int numActions;

    public MultiDimArr(int[] dimentions){
        //8,8,4 : 8x8 board, 4 actions
        this.dimSize=dimentions;
        arr = Array.newInstance(double.class,dimSize);

        numStates = dimSize[0];
        for (int i = 1; i < dimSize.length-1; i++) {
            numStates *=dimSize[i];
        }
        numActions = dimSize[dimSize.length-1];

    }


    public double[] getHArrForState(int[] state){

        int i;
        Object curTable = arr;
        for (i = 0; i < dimSize.length-2; i++) {
            curTable = Array.get(curTable,state[i]);
        }

        return (double[]) Array.get(curTable,state[i]);
    }

    public void setHValue(int[] state, int action, double newHValue){
        getHArrForState(state)[action]=newHValue;
    }

    public void oldListMode(){
        int nStates = (int) Math.pow(5,6);
        System.out.println("nStaes :"+nStates);
        int[][] allStates = new int[nStates][5];




    }

    public static void main(String[] args) {
        MultiDimArr m = new MultiDimArr(new int[]{2,2});
        m.oldListMode();
    }
}
