package ps.v1;

/**
 * Created by Oystein on 22.08.14.
 */
public class EdgeGen<E, A> {
    public final static int EMOTION_POSITIVE = 1, EMOTION_NEGATIVE = -1;

    public int emotion = 0;
    public double h = 1.0;
    public double g = 1.0; //glow parameter
    //public ClipGen<E> start, end;
    public PerceptClip<E, A> start;
    public ActionClip<A> end;

    public EdgeGen(PerceptClip<E, A> start, ActionClip<A> end) {
        this.start = start;
        this.end = end;
    }


    /**
     * Update the edge that was traversed.
     *
     * @param gamma
     * @param reward
     */
    public void update(double gamma, double reward, double damperGlow) {
        // h = h - damping * (h - 1) + reward;
        g = 1.0;

        h = h - gamma * (h - 1) + reward * g;


//        System.out.println("\tUpdate: " + start + " -> " + end + " h: " +  String.format("%.2f",h) + "->" +  String.format("%.2f",newH));

    }

    public void damper(double gamma, double reward, double damperGow) {
        // h= h - damping * (h - 1);
        //System.out.println("\tDamper: " + start + " -> " + end + " h: " +  String.format("%.2f",h) + "->" +  String.format("%.2f",newH));

        //new.. ? works?
        h = h - gamma * (h - 1) + reward * g;

        //damper g
        g = g - damperGow * g;


    }

    public void wasChosen() {
        g = 1.0;
    }

    public void damp(double gamma, double reward, double damperGow) {
        h = h - gamma * (h - 1) + reward * g;
//        if (reward > 0)
//            if (h > 1.1)
//                System.out.println("h is updated : " + h);
        g = g - damperGow * g;
        if(g<0)
            g=0.0;


    }

    public void setEmotion(int e) {
        emotion = e;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EdgeGen edge = (EdgeGen) o;

        if (!end.equals(edge.end)) return false;
        if (!start.equals(edge.start)) return false;

        return true;
    }


    @Override
    public String toString() {

        return "(" + start + "->" + end + ")";
    }

    @Override
    public int hashCode() {
        int result = start.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }

    public int getEmotion() {
        return emotion;
    }

    public String getEmotionStr() {
        switch (emotion) {
            case EMOTION_NEGATIVE:
                return ":(";
            case EMOTION_POSITIVE:
                return ":)";
            default:
                return "?";
        }

    }
}
