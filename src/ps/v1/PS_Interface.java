package ps.v1;

/**
 * Interface a problem need to implement.
 * Used for callback from the PS experiments. Will ask if the last action was a success or not.
 *
 * Created by Oystein on 28.08.14.
 */
public interface PS_Interface<E,A> {

    public boolean isCorrectActionGen(E percept, A action);
}
