package ps.v1;

import java.util.Arrays;
import java.util.List;

/**
 * Extension to the regular PS.
 * Reflection based PS. Will find actions that have a positive emotion.
 * Created by Oystein on 26/10/14.
 */
public class PSR_Matrix<E, A> extends PS_Matrix<E, A> {

    private boolean[][] emotion;

    public PSR_Matrix(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        super(percepts, actions, gamma, rewardSuccess, reflectionTime, damperGow);

        emotion = new boolean[percepts.size()][actions.size()];
        for (boolean[] anEmotion : emotion) Arrays.fill(anEmotion, true);

    }


    /**
     * Calls the regular getAction(percept)
     * until it returns an (percept -> action) pair that have a positive emotion tag.
     * Or until it's tried more than max 'maxReflectionTime' parameter.
     */
    @Override
    public A getAction(E percept) {

        A action = super.getAction(percept);
        for (int i = 0; i < maxReflectionTime - 1; i++) {

            if (emotion[lastSelectedPerc][lastSelectedAction])
                return action;
            else action = super.getAction(percept);


        }

        return action;
    }

    @Override
    public void giveReward(boolean success) {
        emotion[lastSelectedPerc][lastSelectedAction] = success;
        super.giveReward(success);
    }

    public void giveReward(int reward){
        if(reward<0){
            super.giveReward(false);
            emotion[lastSelectedPerc][lastSelectedAction] =false;

        }else if(reward==0){
            super.giveReward(false);
            //do nothing with emotion

        }else{

            super.giveReward(true);
            emotion[lastSelectedPerc][lastSelectedAction] =true;
        }

    }
}
