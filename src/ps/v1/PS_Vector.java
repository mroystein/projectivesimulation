package ps.v1;

import utils.MyUtils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 11/11/14.
 */
public class PS_Vector<E, A> implements InterfacePS<E, A> {
//    public final static int EMOTION_POSITIVE = 1, EMOTION_NEGATIVE = -1;

    private Random random = new Random(System.currentTimeMillis() + new Random().nextInt());
    protected int reflectionTime;
    private double rewardSuccess;
    private double gamma;
    /**
     * Defines how far back recently used edges should get of the reward.
     */
    private double damperGow;

    double h[][], g[][];
//    int[][] emotions;

    private List<E> percepts;
    private List<A> actions;

    protected int lastSelectedPerc = 0, lastSelectedAction = 0;
    private boolean useSoftMax = true;

    int[] dimensions;
    Object hObjArr;
    public PS_Vector() {

         dimensions = new int[]{8,8,4};
//        double[] h= (double[]) Array.newInstance(double.class, dimensions);

         hObjArr =  Array.newInstance(double.class, dimensions);

        int[] state = {1,1};
        double[] hValuesForState = getHArrForState(state);
        System.out.println(Arrays.toString(hValuesForState));

        setHValue(state,1,3.0);
        System.out.println(Arrays.toString(hValuesForState));

        loop();



    }

    public PS_Vector(double[][][] s) {
        
    }
    

    public double[] getHArrForState(int[] state){

        int i;
        Object curTable = hObjArr;
        for (i = 0; i < dimensions.length-2; i++) {
            curTable = Array.get(curTable,state[i]);
        }

        return (double[]) Array.get(curTable,state[i]);
    }

    public void setHValue(int[] state, int action, double newHValue){
        getHArrForState(state)[action]=newHValue;
    }

    public void loop(){

        int dim=3;

        int[][] t = new int[dim][dim];
        for (int i = 0; i < t.length; i++) {
            for (int j = 0; j < t[i].length; j++) {


                int v = (i+1)*(j+1);
                t[i][j]=v;
            }
        }

        for (int i = 0; i < t.length; i++) {
            System.out.println(Arrays.toString(t[i]));
        }
        
    }






    public PS_Vector(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime, double damperGow) {
        this.reflectionTime = reflectionTime;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;
        this.damperGow = damperGow;
        this.percepts = percepts;
        this.actions = actions;


        h = new double[percepts.size()][actions.size()];
        g = new double[percepts.size()][actions.size()];
//        emotions = new int[percepts.size()][actions.size()];

        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {
                h[i][j] = 1.0;
                g[i][j] = 0.0;
            }
        }


    }

    @Override
    public A getAction(E percept) {
        //Runtime : O(|actions|^2)
        int index = percepts.indexOf(percept);
        if (index == -1) {
            System.out.println("Not found!");
            return null;
        }


        //double p = ProjectionSimulationGen.random.nextDouble();
        double p = random.nextDouble();

        double cumulativeProbability = 0.0;
        for (int i = 0; i < h[index].length; i++) {

            cumulativeProbability += getHoppingPropability(index, i);
            if (p <= cumulativeProbability) {
                lastSelectedPerc = index;
                lastSelectedAction = i;
                g[index][i] = 1.0;
                return actions.get(i);
            }
        }

        System.out.println("no actions. Should never happen.");
        return null;
    }

    @Override
    public void giveReward(boolean success) {

        //g[lastSelectedPerc][lastSelectedAction] = 1.0;

        double reward = 0.0;
        if (success)
            reward = rewardSuccess;

        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {


                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];

                g[i][j] = g[i][j] - damperGow * g[i][j];


                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }

    public void giveRewardAmount(double reward) {

        //g[lastSelectedPerc][lastSelectedAction] = 1.0;
        for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[i].length; j++) {


                h[i][j] = h[i][j] - gamma * (h[i][j] - 1) + reward * g[i][j];

                g[i][j] = g[i][j] - damperGow * g[i][j];


                if (g[i][j] < 0.0)
                    g[i][j] = 0.0;
            }
        }

    }

    @Override
    public void useSoftMax(boolean useSoftMax) {
        this.useSoftMax = useSoftMax;
    }

    @Override
    public void reset() {
        System.err.println("Not implemented");
    }

    private double getHoppingPropability(int perceptInd, int actionInd) {
        if (useSoftMax)
            return MyUtils.softMax(h[perceptInd], h[perceptInd][actionInd]);

        return getHoppingPropabilityFormulaOne(perceptInd, actionInd);
    }

    private double getHoppingPropabilityFormulaOne(int perceptInd, int actionInd) {
        double prop = h[perceptInd][actionInd];
        double total = 0.0;
        for (int i = 0; i < h[perceptInd].length; i++) {
            total += h[perceptInd][i];
        }
        return prop / total;
    }

    @Override
    public String toString() {
        return "G: γ=" + gamma + ", |S/A|=" + percepts.size() + ",λ=" + rewardSuccess + ", R=" + reflectionTime + ", g=" + damperGow;
    }


    public static void main(String[] args) {
        PS_Vector<Integer,Integer> ps = new PS_Vector<>();

    }
}