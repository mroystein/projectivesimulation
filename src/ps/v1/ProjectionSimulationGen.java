package ps.v1;


import org.jfree.data.xy.XYSeries;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ProjectionSimulationGen<E, A> implements InterfacePS<E,A>{
    public static Random random = new Random(System.currentTimeMillis());

    public static final double GAMMA_UPPER_TOP = 1.0 / 100.0, GAMMA_UPPER = 1.0 / 50.0, GAMMA_MIDDLE = 1.0 / 10.0, GAMMA_LOWER = 1.0 / 5.0;


    private PS_Interface<E, A> ps_interface;
    //private Random r;
    private XYSeries series;

    private PerceptClip<E, A> lastUsedClipPerc;
    private double gamma = 0.02, rewardSuccess = 1.0, rewardError = -0.04;
    public int timeSteps;
    private int reflectionTime;
    private List<PerceptClip<E, A>> clip_perc_list;
    private List<ActionClip<A>> clip_action_list;

    private double damperGlow = 1.0; //1 ==no glow


    public List<E> percepts;
    public List<A> actions;

    public ProjectionSimulationGen(PS_Interface<E, A> ps_interface, List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime) {

        this.reflectionTime = reflectionTime;
        this.ps_interface = ps_interface;
        this.rewardSuccess = rewardSuccess;
        this.gamma = gamma;

        this.percepts = percepts;
        this.actions=actions;

        reset();

    }
    @Override
    public void reset() {
        clip_perc_list = new ArrayList<>();
        clip_action_list = new ArrayList<>();

        //clip_perc_list.addAll(percetrons.stream().map(Clip::new).collect(Collectors.toList()));
        for (E percept : percepts) {
            clip_perc_list.add(new PerceptClip<>(percept, reflectionTime, damperGlow));
        }

        for (A action : actions) {
            clip_action_list.add(new ActionClip<>(action));
        }

        for (PerceptClip<E, A> perceptClip : clip_perc_list) {
            for (ActionClip<A> actionClip : clip_action_list) {
                perceptClip.addNeighbour(actionClip);
            }
        }

        series = new XYSeries(toString());
    }

    public ProjectionSimulationGen(PS_Interface<E, A> ps_interface, List<E> percepts, List<A> actions, double gamma, double rewardSuccess, int reflectionTime,double damperGlow) {
        this(ps_interface,percepts,actions,gamma,rewardSuccess,reflectionTime);
        this.damperGlow=damperGlow;
    }

    public double getSuccessRate() {


        //paper method : r = Sum( P(s) * p(a|s) They are equal..
        double sum = 0;
        for (PerceptClip<E, A> clip : clip_perc_list) {
            double propOfClip = 1.0 / clip_perc_list.size();

            for (EdgeGen<E, A> edge : clip.getEdges()) {

                if (ps_interface.isCorrectActionGen(edge.start.id, edge.end.getAction())) {
                    double propHop = edge.start.getHoppingProbability(edge.end);
                    sum += propOfClip * propHop;

                }
            }
        }

        //System.out.println("sum="+sum+" , old="+old);
        //return prop / numberOfCorrectActions;
        return sum;
    }

    public void addNewPercept(E symbol) {
        percepts.add(symbol);
    }

    public void removePercept(E symbol) {
        Iterator<E> ite = percepts.iterator();
        while (ite.hasNext()) {
            if (ite.next().equals(symbol))
                ite.remove();
        }
    }

    public void train(int time) {
        for (int i = 0; i < time; i++) {
            train();
        }
    }

    private PerceptClip<E, A> addNewPerceptClip(E symbol) {
        PerceptClip<E, A> c = new PerceptClip<>(symbol, reflectionTime, damperGlow);
        clip_perc_list.add(c);
        for (ActionClip<A> actionClip : clip_action_list) {
            c.addNeighbour(actionClip);
        }
        System.out.println("New percept clip :" +c);
        return c;
    }

    private PerceptClip<E, A> findPerceptClip(E symbol) {
        for(PerceptClip<E,A> clip : clip_perc_list){
            if(clip.id.equals(symbol))
                return clip;
        }

        System.out.println("Had to make a new perception!");
        return addNewPerceptClip(symbol);
    }

    public boolean train() {

        E randomPerception = percepts.get(random.nextInt(percepts.size()));
        A action = getActionSelfReward(randomPerception);
        return ps_interface.isCorrectActionGen(randomPerception,action);


    }


    public A getActionSelfReward(E symbol) {

        PerceptClip<E, A> perceptClip = findPerceptClip(symbol);
        if(perceptClip==null){
            System.out.println("Wtf. null");
            return null;
        }
        lastUsedClipPerc = perceptClip;

        series.add(timeSteps, getSuccessRate());
        timeSteps++;


        A bestAction = perceptClip.getBestAction();
        boolean correct = ps_interface.isCorrectActionGen(symbol, bestAction);

        if (correct) {
            giveRewardForLastAction(true);

        } else {
            giveRewardForLastAction(false);

        }

        return bestAction;
    }

    @Override
    public void giveReward(boolean success) {
        giveRewardForLastAction(success);
    }

    @Override
    public void useSoftMax(boolean useSoftMax) {
        System.out.println("Not implemented yet!! useSoftMAx");
    }




    @Override
    public A getAction(E symbol) {

        PerceptClip<E, A> perceptClip = findPerceptClip(symbol);
        if(perceptClip==null){
            System.out.println("Wtf. null");
        }
        lastUsedClipPerc = perceptClip;

        series.add(timeSteps, getSuccessRate());
        timeSteps++;


        A bestAction = perceptClip.getBestAction();

        return bestAction;
    }



    public void giveRewardForLastAction(boolean success) {
        if (lastUsedClipPerc == null) {
            System.out.println("last clip is null!");
            return;
        }
        if (lastUsedClipPerc.lastSelectedEdge == null) {
            System.out.println("last selected edge is null!");
            return;
        }

        updateRuleWithDamperNew(success);
        //oldUpdateRules(success);

    }

    private void updateRuleWithDamperNew(boolean success) {

        EdgeGen usedEdge = lastUsedClipPerc.lastSelectedEdge;
        //boolean wasActionSuccess = reward > 0.0;
        usedEdge.wasChosen();
        if (success) {

            usedEdge.setEmotion(EdgeGen.EMOTION_POSITIVE);
        } else {

            usedEdge.setEmotion(EdgeGen.EMOTION_NEGATIVE);
        }

        //damp other edges
        for (PerceptClip<E, A> clip : clip_perc_list) {
            for (EdgeGen<E, A> edge : clip.getEdges()) {
                if (success)
                    edge.damp(gamma, rewardSuccess, damperGlow);
                else
                    edge.damp(gamma, 0, damperGlow);

            }
        }

    }

    private void oldUpdateRules(boolean success) {
        EdgeGen usedEdge = lastUsedClipPerc.lastSelectedEdge;
        //boolean wasActionSuccess = reward > 0.0;

        if (success) {
            usedEdge.update(gamma, rewardSuccess, damperGlow);

            usedEdge.setEmotion(EdgeGen.EMOTION_POSITIVE);
        } else {
            usedEdge.setEmotion(EdgeGen.EMOTION_NEGATIVE);
        }

        //damp other edges
        for (PerceptClip<E, A> clip : clip_perc_list) {
            for (EdgeGen<E, A> edge : clip.getEdges()) {
                if (success && edge.equals(usedEdge))
                    continue;
                edge.damper(gamma, rewardSuccess, damperGlow);
            }
        }
    }


    @Override
    public String toString() {
        return "G: γ=" + gamma+ ", R=" + reflectionTime+"";
        //return "G: γ=" + damping + ", |S/A|=" + clip_action_list.size() + ",λ=" + rewardSuccess + ", R=" + maxReflectionTime+", g="+damperGlow;
    }

    public int getTimeSteps() {
        return timeSteps;
    }


    public void setDamperGlow(double damperGlow) {
        this.damperGlow = damperGlow;
    }

    public XYSeries getSeries() {
        return series;
    }

    public List<ActionClip<A>> getActionClips() {
        return clip_action_list;
    }

    public List<PerceptClip<E, A>> getPercClips() {
        return clip_perc_list;
    }

    public double getGamma() {
        return gamma;
    }

    public double getRewardSuccess() {
        return rewardSuccess;
    }

    public double getRewardError() {
        return rewardError;
    }

    public void setGamma(double gamma) {
        this.gamma = gamma;
    }

    public void setRewardSuccess(double rewardSuccess) {
        this.rewardSuccess = rewardSuccess;
    }

    public void setRewardError(double rewardError) {
        this.rewardError = rewardError;
    }
}
