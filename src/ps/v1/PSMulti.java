package ps.v1;


import org.jfree.data.xy.XYSeries;

import java.util.List;

/**
 * A class for creating n agents. And the XYSeries for plotting will be the average blocking rate.
 *
 * Created by Oystein on 01.09.14.
 */
public class PSMulti<E,A> {

    private XYSeries series;
    public ProjectionSimulationGen[] list;

    public PSMulti(PSBuilder<E,A> psBuilder){
        this(psBuilder.ps_interface,psBuilder.percepts,psBuilder.actions,psBuilder.gamma,psBuilder.reward,psBuilder.nAgents,psBuilder.reflectionTime);
    }

    private PSMulti(PS_Interface<E,A> ps_interface, List<E> percepts,List<A> actions, double gamma, double reward, int numberOfAgents, int reflectionTime){



        list = new ProjectionSimulationGen[numberOfAgents];
        for(int i=0;i<list.length;i++){

                list[i]=new ProjectionSimulationGen<>(ps_interface, percepts, actions, gamma, reward,reflectionTime);
           }
        series=new XYSeries(toString());


    }



    public void train(int n){


        for(int i=0;i<n;i++){

            double nSuccess = 0;

            for(ProjectionSimulationGen ps: list){

                boolean success = ps.train();
                if(success)
                    nSuccess++;

            }
            double rate = nSuccess/list.length;
            series.add(list[0].getTimeSteps(),rate);

        }

    }

    @Override
    public String toString() {
        return "|A|="+list.length+", "+list[0].toString();
    }

    public XYSeries getSeries(){
        return series;
    }


    public static class PSBuilder<E,A> {

        //  int nSymbols, int maxReflectionTime
        private int nAgents=1000;
        private PS_Interface<E,A> ps_interface;


        private int reflectionTime=1;
        private double gamma=0.02;
        private double reward=1.0;
        private List<E> percepts;
        private List<A> actions;


        public PSBuilder(PS_Interface<E,A> ps_interface, List<E> percepts,List<A> actions){
            this.percepts=percepts;
            this.actions=actions;
            this.ps_interface=ps_interface;
        }
        public PSBuilder<E,A> gamma(double gamma){
            this.gamma=gamma;
            return this;
        }
        public PSBuilder<E,A> reward(double reward){
            this.reward=reward;
            return this;
        }

        public PSBuilder<E,A> reflectionTime(int reflectionTime) {
            this.reflectionTime = reflectionTime;
            return this;
        }

        public PSBuilder<E,A> nAgents(int nAgents) {
            this.nAgents = nAgents;
            return this;
        }




        public PSMulti<E,A> build(){
            return new PSMulti<>(this);
        }


    }

}
