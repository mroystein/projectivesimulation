package ps.v1;

import experiments.InvasionGame.Const;
import utils.MyUtils;

import java.util.ArrayList;

/**
 * Created by Oystein on 04.09.14.
 */
public class PerceptClip<E, A> extends ClipGen {

    protected ArrayList<EdgeGen<E, A>> edges;
    public EdgeGen lastSelectedEdge;
    private int reflectionTime;
    private int currentReflecion = 0;
    public E id;
    private double damperGlow;
    boolean useSotfMax = true;


    public PerceptClip(E id, int reflectionTime, double damperGlow) {
        this.id = id;
        this.reflectionTime = reflectionTime;
        this.damperGlow = damperGlow;
        edges = new ArrayList<>();
    }


    public void addNeighbour(ActionClip<A> clip) {
        edges.add(new EdgeGen<>(this, clip));
    }

    public double getHoppingProbability(ActionClip cj) {

        if (useSotfMax){

            double soft1 = softMaxHoppingPropability(cj);
            double soft2 = softMax(cj);

            if(soft2>30){
                System.out.println("l");
            }
            return soft2;
        }


        double hWeight = 0, hWeightSumOthers = 0;
        for (EdgeGen edge : edges) {
            if (edge.end.equals(cj)) {
                hWeight = edge.h;
            }
            hWeightSumOthers += edge.h;
        }
        return hWeight / hWeightSumOthers;
    }

    /**
     * An alternative to getHoppingProbability().
     * Smaller differences in h leads to bigger change in the probability.
     *
     * @param cj
     * @return
     */
    private double softMaxHoppingPropability(ActionClip cj) {
        double hWeight = 0, hWeightSumOthers = 0;
        for (EdgeGen edge : edges) {
            if (edge.end.equals(cj)) {
                hWeight = Math.exp(edge.h);

            }
            hWeightSumOthers += Math.exp(edge.h);

        }
        return hWeight / hWeightSumOthers;

    }


    private double softMax(ActionClip cj){

        double a =0;
        double[] arr = new double[edges.size()];
        for (int i = 0; i < edges.size(); i++) {
            EdgeGen<E, A> e = edges.get(i);
            arr[i]= e.h;
            if(e.end.equals(cj))
                a= e.h;
        }


        return MyUtils.softMax(arr, a);
    }


    public A getBestAction() {
        EdgeGen<E, A> firstDebugEdge = null;
        //Edge edge =  getBestEdge();
        while (currentReflecion < (reflectionTime - 1)) {
            EdgeGen<E, A> edge = getBestEdge();

            if (firstDebugEdge == null)
                firstDebugEdge = edge;

            if (edge.getEmotion() == EdgeGen.EMOTION_POSITIVE) {

                //System.out.println("Reflected "+currentReflecion+" times to go to "+edge+" instead of "+firstDebugEdge);
                return edge.end.getAction();
            }
            currentReflecion++;

        }

        currentReflecion = 0;

        EdgeGen<E, A> bestEdge = getBestEdge();
        if (bestEdge == null) {
            System.out.println("Waa");
        }
        ActionClip<A> end = getBestEdge().end;
        if (end == null || end.getAction() == null) {
            System.out.println("WTF");
        }
        return end.getAction();
    }

    private EdgeGen<E, A> getBestEdge() {
        double p = ProjectionSimulationGen.random.nextDouble();

        double cumulativeProbability = 0.0;
        for (EdgeGen edge : edges) {

            cumulativeProbability += getHoppingProbability(edge.end);
            if (p <= cumulativeProbability) {
                lastSelectedEdge = edge;

                return edge;
            }
        }
        //deb
         p = ProjectionSimulationGen.random.nextDouble();

         cumulativeProbability = 0.0;
        for (EdgeGen edge : edges) {

            cumulativeProbability += getHoppingProbability(edge.end);
            if (p <= cumulativeProbability) {
                lastSelectedEdge = edge;

                return edge;
            }
        }




        lastSelectedEdge = edges.get(0);
        return edges.get(0);
    }

    //debug
    public double getHoppingProbability(A action) {
        for (EdgeGen<E, A> e : edges) {
            if (e.end.getAction().equals(action))
                return getHoppingProbability(e.end);
        }
        System.err.println("Error. AClip not found.");
        return 0;
    }

    //debug
    public double getHopping(A action) {
        for (EdgeGen<E, A> e : edges) {
            if (e.end.getAction().equals(action))
                return e.h;
        }
        System.err.println("Error. AClip not found.");
        return 0;
    }

    public ArrayList<EdgeGen<E, A>> getEdges() {
        return edges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (o instanceof PerceptClip) {
            PerceptClip<E, A> clip = (PerceptClip) o;
            return this.id.equals(clip.id);
        }


        return false;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        if (id instanceof Integer) {

            return Const.symbolToString((Integer) id);
        }
        return id.toString();

        //return Const.symbolToString(id);
    }

}
