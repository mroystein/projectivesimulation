package ps.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oystein on 28/11/14.
 */
public class PSGreedy<E, A> extends PS_Matrix<E, A> {



    private double epsilon;
    private final double epsilonCopy;
    private final double epsilonMulti;
    private  double errorMargin=0.01;

    public PSGreedy(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, double damperGow, double epsilonMulti, double epsilon,double errorMargin) {
        this(percepts, actions, gamma, rewardSuccess, damperGow,epsilon,epsilonMulti);
        this.errorMargin=errorMargin;
    }

    public PSGreedy(List<E> percepts, List<A> actions, double gamma, double rewardSuccess, double damperGow, double epsilon, double epsilonMulti) {
        super(percepts, actions, gamma, rewardSuccess, 1, damperGow);
        this.epsilon = epsilon;
        this.epsilonCopy = epsilon;
        this.epsilonMulti = epsilonMulti;
    }

    @Override
    public A getAction(E percept) {
        int index = percepts.indexOf(percept);


        if(random.nextDouble()<epsilon){
            int randAction = random.nextInt(actions.size());
            setSelectedPercActionPair(index,randAction);
            return actions.get(randAction);
        }



        int maxPos=-1;

        double maxValue= Double.MIN_VALUE;
        for (int i = 0; i < h[index].length; i++) {

            if(h[index][i]>maxValue){
                maxValue=h[index][i];
                maxPos=i;
            }
        }

        ArrayList<Integer> listEquals = new ArrayList<>();
        for (int i = 0; i <h[index].length ; i++) {
            if(h[index][i]==maxValue)
                listEquals.add(i);
            else if(h[index][i]>maxValue-errorMargin && h[index][i]<maxValue+errorMargin){
                listEquals.add(i);
            }
        }
        if(listEquals.size()>2)
            maxPos= listEquals.get(random.nextInt(listEquals.size()));


        setSelectedPercActionPair(index,maxPos);
        return actions.get(maxPos);
    }
    private void setSelectedPercActionPair(int perceptInd,int actionInd){
        lastSelectedPerc = perceptInd;
        lastSelectedAction = actionInd;
        g[perceptInd][actionInd] = 1.0;
        //Not needed
        lastTimeUsed[perceptInd][actionInd] = currentTimeStep;
        currentTimeStep++;
    }

    @Override
    public void giveReward(boolean success) {
        if(success) {
            epsilon *= epsilonMulti;
            errorMargin*=0.993;
        }
        super.giveReward(success);
    }

    public double getEpsilon() {
        return epsilon;
    }

    @Override
    public void reset() {
        epsilon=epsilonCopy;
        super.reset();
    }

    @Override
    public String toString() {

        return super.toString()+ String.format("Greedy, ε=%.4f (%.4f*%.4f), err=%.4f",epsilon,epsilonCopy,epsilonMulti,errorMargin);
    }
}