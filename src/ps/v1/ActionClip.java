package ps.v1;

import experiments.InvasionGame.Const;

/**
 * Created by Oystein on 04.09.14.
 */
public class ActionClip<A>  extends ClipGen{

    private final A action;

    public ActionClip(A action) {
        this.action=action;
    }

    public A getAction() {
        return action;
    }

    @Override
    public String toString() {
        if(action instanceof Integer){

            return Const.symbolToString((Integer) action);
        }
        return action.toString();

        //return Const.symbolToString(id);
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ActionClip))
            return false;

        ActionClip<A> a2 = (ActionClip<A>) obj;
        if(action.equals(a2.getAction()))
            return true;

        return false;
    }
}
