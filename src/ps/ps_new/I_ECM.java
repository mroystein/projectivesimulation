package ps.ps_new;

/**
 * Created by Oystein on 26/02/15.
 */
public interface I_ECM {
    public int getAction(int percept);
    public void giveReward(boolean success);
}
