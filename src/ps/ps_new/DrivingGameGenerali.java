package ps.ps_new;

import gui.SimpleSwingGraphGUI;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import ps.v2.PS;

import java.util.Random;

/**
 * Created by Oystein on 29/05/15.
 */
public class DrivingGameGenerali {
    public static int[] GREEN_LEFT =  {0, 0};
    public static int[] GREEN_RIGHT = {0, 1};
    public static int[] RED_LEFT =    {1, 0};
    public static int[] RED_RIGHT =   {1, 1};

    public static int[] ACTION_STOP = {0};
    public static int[] ACTION_GO =   {1};


    public static int[][] percepts = {GREEN_LEFT, GREEN_RIGHT, RED_LEFT, RED_RIGHT};
    public static int[][] actions = {ACTION_STOP, ACTION_GO};

    public DrivingGameGenerali() {

        int numAgents = 1000;
        double[] avg = new double[4000];
        double[] avgNormal = new double[4000];
        for (int i = 0; i < numAgents; i++) {

            double[] freq = experimentFromPaper();
            double[] freqNormal =experimentFromPaperRegular();
            for (int j = 0; j < freq.length; j++) {
                avg[j]+=freq[j];
                avgNormal[j]+=freqNormal[j];
            }
        }

        XYSeries series = new XYSeries("PS generalization");
        for (int i = 0; i < avg.length; i++) {
            avg[i]/=(double)numAgents;
            series.add(i,avg[i]);
        }

        XYSeries seriesNormal = new XYSeries("PS");
        for (int i = 0; i < avg.length; i++) {
            avgNormal[i]/=(double)numAgents;
            seriesNormal.add(i,avgNormal[i]);
        }

//        XYSeries[] serieses = {series,seriesNormal};
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(serieses));

        XYSeriesCollection ds = new XYSeriesCollection();
        ds.addSeries(series);
        ds.addSeries(seriesNormal);
        new SimpleSwingGraphGUI(ds,"Learning Curve","Time step","Accuracy",0.0,1.0);
    }

    public static Random r = new Random(System.currentTimeMillis());

    public static double[] experimentFromPaper() {
        PS_Generalization ps = new PS_Generalization(0.005, actions, percepts[0].length);
        int c=0;

        double correctCount = 0;
        int numTests = 4000;
        double[] freq = new double[numTests];
        XYSeries series = new XYSeries(ps.toString());
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[r.nextInt(percepts.length)];
            int[] action = ps.getAction(percept);
            boolean isCorrect = a(percept,action);
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[r.nextInt(percepts.length)];
            int[] action = ps.getAction(percept);
            boolean isCorrect = b(percept,action);
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[r.nextInt(percepts.length)];
            int[] action = ps.getAction(percept);
            boolean isCorrect = c(percept,action);
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[r.nextInt(percepts.length)];
            int[] action = ps.getAction(percept);
            boolean isCorrect =d(percept,action);
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }

//        System.out.println("Correct count : " + correctCount / numTests);
        return freq;
    }

    public static boolean a(int[] percept, int[] action){
        return percept[0] == action[0];
    }
    public static boolean b(int[] percept, int[] action){
        return percept[0] != action[0];
    }
    public static boolean c(int[] percept, int[] action){
        return percept[1] == action[0];
    }
    public static boolean d(int[] percept, int[] action){
        return action[0]==1;
    }

    public static double[] experimentFromPaperRegular() {
        int GREEN_LEFT=0; int GREEN_RIGHT=1;
        int RED_LEFT=2; int RED_RIGHT=3;

        int ACTION_STOP = 0;
        int ACTION_GO = 1;

        PS ps = new PS(4,2,0.005,1,1,1.0,PS.PS_SIMPLE);

        int c=0;


        int numTests = 4000;
        double[] freq = new double[numTests];


        for (int i = 0; i < 1000; i++) {
            int percept = r.nextInt(4);
            int action = ps.getAction(percept);

            //stop at red light, go else
            boolean isCorrect =  a(percepts[percept],actions[action]);
            ps.update(isCorrect ? 1 : 0,-1);
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }

        for (int i = 0; i < 1000; i++) {
            int percept = r.nextInt(4);
            int action = ps.getAction(percept);

            //stop at red light, go else
            boolean isCorrect =  b(percepts[percept],actions[action]);
            ps.update(isCorrect ? 1 : 0,-1);
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }
        for (int i = 0; i < 1000; i++) {
            int percept = r.nextInt(4);
            int action = ps.getAction(percept);

            //stop at red light, go else
            boolean isCorrect =  c(percepts[percept],actions[action]);
            ps.update(isCorrect ? 1 : 0,-1);
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }
        for (int i = 0; i < 1000; i++) {
            int percept = r.nextInt(4);
            int action = ps.getAction(percept);

            //stop at red light, go else
            boolean isCorrect =  d(percepts[percept],actions[action]);
            ps.update(isCorrect ? 1 : 0,-1);
            freq[c++]=isCorrect ? 1.0 : 0.0;
        }

//        System.out.println("Correct count : " + correctCount / numTests);
        return freq;
    }


    public static void main(String[] args) {
        new DrivingGameGenerali();
    }
}
