package ps.ps_new;

import java.util.List;
import java.util.Random;

/**
* Created by Oystein on 26/02/15.
*/
class Clip {
    public ECM ecm;
    public int id;
    Random random = new Random(System.currentTimeMillis());
    List<Edge> edges;

    public Clip(ECM ecm, int id) {
        this.ecm = ecm;
        this.id = id;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }

    public Edge getStrongestEdge() {

        double total = 0.0;
        for (Edge edge : edges)
            total += edge.weight;

        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < edges.size(); i++) {
            cumulativeProbability += edges.get(i).weight / total;
            if (p < cumulativeProbability) {
                if (!edges.get(i).emotion && ecm.currentReflection < ecm.reflectionTime - 1) {
                    ecm.currentReflection++;
                    getStrongestEdge();
                }
                return edges.get(i);
            }
        }

        return null;
    }
    public double getHoppingPropability(Clip toClip){

        double total = 0.0;
        for (Edge edge : edges)
            total += edge.weight;
        for (Edge edge : edges){
            if(edge.end.equals(toClip))
                return edge.weight/total;
        }
        System.out.println("Not connected getHopping()");
        return 0.0;
    }


    public Edge getEdgeToClip(Clip c){
        for(Edge e: edges)
            if(e.end.equals(c)) return e;
        return null;
    }

//        @Override
//        public boolean equals(Object obj) {
//            if(!(obj instanceof Clip))
//                return false;
//            Clip other = (Clip) obj;
//            return this.id==other.id;
//        }
}
