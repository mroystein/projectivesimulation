package ps.ps_new;

import ps.v1.InterfacePS;
import utils.MyUtils;

import java.util.*;

/**
 * Clip Glow with association. Clip to clip etc.
 *
 * Using the idea Clip glow.
 * Each clip can be exited
 * Created by Oystein on 10/04/15.
 */
public class PS_Association_ClipGlow implements I_ECM, InterfacePS<Integer, Integer> {
    Random random = new Random();
    double[][] hActions;
    double[][] gActions;
    double[] clipGlow;
    double[] clipGlowAction;
    boolean[][] emotionClipToAction;

    double[][] hClips;

    public double damperGlow = 1.0;
    public double gamma = 0.001;
    public double reward = 1;

    List<Edge> lastUsedEdges;
    public int reflectionTime;
    public int currentReflection = 0;
    public int deliberationLength;

    public int maksReflectionTime = 0;
    int numPercepts;
    int numActions;

    boolean debugPrint = false;
    int debugCounter=0;
    LinkedList<Integer> exitedPercepts;

    //(perceptList,actionList,damping,1.0,maxReflectionTime,damperGlow);

    public PS_Association_ClipGlow(int numPercepts, int numActions, double gamma, int maxReflectionTime, double damperGlow) {
        this(numPercepts, numActions);
        this.gamma = gamma;
        this.maksReflectionTime = maxReflectionTime;
        this.damperGlow = damperGlow;
    }

    public PS_Association_ClipGlow(int numPercepts, int numActions) {
        this.numPercepts = numPercepts;
        this.numActions = numActions;

        hActions = new double[numPercepts][numActions];
        gActions = new double[numPercepts][numActions];
        hClips = new double[numPercepts][numPercepts];
        clipGlow = new double[numPercepts];
        clipGlowAction = new double[numActions];

        emotionClipToAction = new boolean[numPercepts][numActions];
        reset();

        exitedPercepts = new LinkedList<>();
    }


    @Override
    public String toString() {
        return "PS-Asso: clipglow γ=" + gamma + ", |S/A|=" + numPercepts +"/"+numActions+ " R=" + maksReflectionTime + ", g=" + damperGlow;

    }

    public String toStringAll() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numPercepts; i++) {

            sb.append(String.format("Clip %d ", i));
            String sActions = "";
            for (int j = 0; j < numActions; j++) {

                sActions += String.format("{A%d h=%.1f %s }", j, hActions[i][j], (emotionClipToAction[i][j] ? ":)" : ":("));
            }
            sb.append(sActions);
            sb.append(" | To other Clips :");
            String sClips = "";
            for (int j = 0; j < numPercepts; j++) {

                sClips += String.format("{Clip%d h=%.1f} ", j, hClips[i][j]);
            }
            sb.append(sClips);
            sb.append("\n");
        }


        return sb.toString();
    }

    public void reset() {

        //Clip to Action
        for (int i = 0; i < numPercepts; i++) {
            Arrays.fill(hActions[i], 1.0);
            Arrays.fill(gActions[i], 0.0);
            Arrays.fill(emotionClipToAction[i], true);
        }
        //Clip to Clip
        for (int i = 0; i < numPercepts; i++) {
            Arrays.fill(hClips[i], 1.0);
        }
        //individual clip glow
        Arrays.fill(clipGlow, 0.0);
        Arrays.fill(clipGlowAction, 0.0);

        debugCounter=0;
    }

    private double getHoppingPropabilityToAction(int perceptInd, int actionInd) {
        return MyUtils.softMax(hActions[perceptInd], hActions[perceptInd][actionInd]);
    }

    private double getHoppingPropabilityToClip(int c1, int c2) {
        //original uses the simple formula

        return MyUtils.softMax(hClips[c1], hClips[c1][c2]);
    }

    private int getStrongestClip(int clip1) {
        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < hClips[clip1].length; i++) {
            cumulativeProbability += getHoppingPropabilityToClip(clip1, i);
            if (p <= cumulativeProbability) {
                return i;
            }
        }
        return -1;
    }

    int exitedAction = -1;
    int debugLastPercept = -1;
    StringBuilder currSequenceDebug = new StringBuilder();

    @Override
    public int getAction(int percept) {
        doRandomActionChance++;
        //debugLastPercept=
        clipGlow[percept] = 1.0;
        exitedPercepts.add(percept);
        currSequenceDebug.append("P "+percept+" ");
        int action = getDirectAction(percept);
        boolean forcedToExit = reflectionTime >= maksReflectionTime - 1;

        if (emotionClipToAction[percept][action] || forcedToExit) {
            clipGlowAction[action] = 1.0;
            exitedAction = action;

            if (debugPrint&& reflectionTime > maksReflectionTime)
                System.out.println("Had to return a negative action due to maxReflectionTime=" + reflectionTime + " and max is " + maksReflectionTime);
            currSequenceDebug.append("A "+action+" ");
            return action;
        }

        reflectionTime++;

        //Start a random sequence S = (s1,s2,..,sD,a)
        int clip2 = getStrongestClip(percept);
        return getAction(clip2);
    }

    private int getDirectAction(int percept) {
        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < hActions[percept].length; i++) {
            cumulativeProbability += getHoppingPropabilityToAction(percept, i);
            if (p <= cumulativeProbability) {
                return i;
            }
        }
        return -1;
    }
    private double doRandomActionChance=1.0;


    private int getDirectActionGreedy(int percept) {
        double  biggestValue=0;
        int biggestAction = 0;
        double p = random.nextDouble();
        if(p<doRandomActionChance) {
            doRandomActionChance*=0.01;
            System.out.println(debugCounter+" | Do random Action Chance "+doRandomActionChance);
            return random.nextInt(numActions);
        }


        for (int i = 0; i < hActions[percept].length; i++) {
            if(hActions[percept][i]>biggestValue){
                biggestValue=hActions[percept][i];
                biggestAction=i;
            }

        }
        return biggestAction;
    }
    @Override
    public Integer getAction(Integer percept) {
        int p = percept;
        return getAction(p);
    }

    @Override
    public void giveReward(boolean success) {
//        giveRewardNew(success);
        giveRewardArray(success);
    }

    public void giveRewardNew(boolean success) {
        reflectionTime = 0;//reset
        double reward = success ? 1.0 : 0.0;

        for (Integer clip:exitedPercepts){
            int i = clip;
            int j = exitedAction;
            double hOld =  hActions[i][j];
            hActions[i][j] = hActions[i][j] - gamma * (hActions[i][j] - 1) + reward * clipGlow[i] * clipGlowAction[j];
            double hNew =  hActions[i][j];
            double hDiff = hNew-hOld;
            String diff = String.format("h= %.4f -> %.4f diff=%.4f",hOld,hNew,hDiff);
            emotionClipToAction[i][j] = success;
        }

        if (debugPrint)
            System.out.println("Exited Clips: " + exitedPercepts.toString() + " , leading to action " + exitedAction);
        //direct sequences
        for (int i = 0; i < numPercepts; i++) {

            for (int j = 0; j < numActions; j++) {
                if(exitedPercepts.contains(i) && j==exitedAction)
                    continue;


                hActions[i][j] = hActions[i][j] - gamma * (hActions[i][j] - 1);
//

            }
        }

        //percept to percept
        for (int i = 0; i < numPercepts; i++) {
//            boolean wasExitedClip = clipGlow[i]==1.0;
            for (int j = 0; j < numPercepts; j++) {
                if (i == j) continue; // self reward?? not. a good idea..
                hClips[i][j] = hClips[i][j] - gamma * (hClips[i][j] - 1) + reward * clipGlow[i] * clipGlow[j];
            }
        }
        Arrays.fill(clipGlow, 0.0);
        Arrays.fill(clipGlowAction, 0.0);

        exitedPercepts.clear();
        exitedAction = -1;

    }

    public void giveRewardArray(boolean success) {
        reflectionTime = 0;//reset
        double reward = success ? 1.0 : 0.0;

        if (debugPrint)
            System.out.println("Exited Clips: " + exitedPercepts.toString() + " , leading to action " + exitedAction);
        //direct sequences
        for (int i = 0; i < numPercepts; i++) {
            for (int j = 0; j < numActions; j++) {

                if (clipGlow[i] >= 1) {
                    double reward2 = reward * clipGlow[i] * clipGlowAction[j];
//                    System.out.println("Clip " + i + "is glowing " + reward2);

                }

                hActions[i][j] = hActions[i][j] - gamma * (hActions[i][j] - 1) + reward * clipGlow[i] * clipGlowAction[j];
//                if(success){
                if (clipGlow[i] >= 1.0 && clipGlowAction[j] >= 1.0) {
//                    if(success){
//                        if(i%2!=j){
//                            System.out.println(currSequenceDebug);
//                            System.err.println("not good.");
//                        }
//
//                    }else{
//
//                    }


                    emotionClipToAction[i][j] = success;//risky.. fuck up other..
                }

            }
        }

        //percept to percept
        for (int i = 0; i < numPercepts; i++) {
//            boolean wasExitedClip = clipGlow[i]==1.0;
            for (int j = 0; j < numPercepts; j++) {
                if (i == j) continue; // self reward?? not. a good idea..

                if(debugPrint && clipGlow[i]>=1.0 && clipGlow[j]>=1.0){
                    double hOld= hClips[i][j];
                    double hNew = hClips[i][j] - gamma * (hClips[i][j] - 1) + reward * clipGlow[i] * clipGlow[j];
                    System.out.println(String.format("Clip %d -> Clip %d got rewarded success=%s : h: %.2f -> %.2f  ExitedClips:%s -> A%d", i, j,success, hOld, hNew,exitedPercepts,exitedAction));
                }
                hClips[i][j] = hClips[i][j] - gamma * (hClips[i][j] - 1) + reward * clipGlow[i] * clipGlow[j];

            }
        }
        Arrays.fill(clipGlow, 0.0);
        Arrays.fill(clipGlowAction, 0.0);

        exitedPercepts.clear();
        exitedAction = -1;

        currSequenceDebug.setLength(0);
    }

    @Override
    public void useSoftMax(boolean useSoftMax) {

    }


    public static void main(String[] args) {
        Random r = new Random();
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};
        PS_Association_ClipGlow ps = new PS_Association_ClipGlow(percepts.length,actions.length,0.01,2,1.0);

        for (int i = 0; i < percepts.length; i++) {
            System.out.println(String.format("Percept: %d   Action: 0=%s , 1=%s", percepts[i], isCorrectOdd(i, 0), isCorrectOdd(i, 1)));
        }

        System.out.println(ps.toStringAll());
//        System.out.println("------ Start training -----");
//        for (int i = 0; i < 500; i++) {
//            int percept = percepts[r.nextInt(percepts.length)];
//            int action = ps.getAction(percept);
//            boolean isCorrect = isCorrect(percept, action);
//            ps.giveReward(isCorrect);
//
//        }

        List<Integer> listCorrect = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            int percept = i % 4;//percepts[r.nextInt(percepts.length)];

            int action = ps.getAction(percept);
            boolean isCorrect = isCorrectOdd(percept, action);

            String riktig = isCorrect ? "JA" : "NEI";
            System.out.println(String.format("%d | Percept: %d   Action chosen: %d , riktig= %s", i, percept, action, riktig));

            ps.giveReward(isCorrect);

            listCorrect.add(isCorrect ? 1 : 0);
        }
        System.out.println(listCorrect);
        System.out.println(ps.toStringAll());

        System.out.println("Avg: "+listCorrect.stream().mapToInt(i->i).average().getAsDouble());
//        for (int i = 0; i < 50; i++) {
//            int percept = percepts[r.nextInt(percepts.length)];
//            int action = ps.getAction(percept);
//            boolean isCorrect = isCorrectOdd(percept, action);
//            ps.giveReward(isCorrect);
//            String riktig = isCorrect ? "JA" : "NEI";
//            System.out.println(String.format("%d | Percept: %d   Action chosen: %d , riktig= %s", i, percept, action, riktig));
//            listCorrect.add(isCorrect?1:0);
//        }
//        System.out.println(listCorrect);

//        System.out.println(ps.toStringAll());
    }

    public static boolean isCorrectOdd(int percept, int action) {
        return percept % 2 == action;
    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }
}
