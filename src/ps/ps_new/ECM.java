package ps.ps_new;

import ps.v1.InterfacePS;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * ECM with Associative memory. D>=1
 * Can create fictitious clips.
 *
 * Created by Oystein on 25/02/15.
 */
public class ECM implements I_ECM, InterfacePS<Integer,Integer> {

    public Clip[] perceptClips;
    public ActuatorClip[] actuatorClips;

    public List<CompositionClip> compClips;

    public double gamma = 0.00001;
    public double reward = 1;

    List<Edge> lastUsedEdges;
    public int reflectionTime;
    public int currentReflection = 0;
    //    private PerceptClip lastUsedPercept;
    public int deliberationLength;

    public ECM(int numPercept, int numActions, int deliberationLength, int reflectionTime) {
        if (reflectionTime < 1) System.err.println("ReflectionTIme");
        if (deliberationLength < 1) System.err.println("D");

        this.reflectionTime = reflectionTime;
        this.deliberationLength = deliberationLength;


        actuatorClips = new ActuatorClip[numActions];
        for (int i = 0; i < numActions; i++) {
            actuatorClips[i] = new ActuatorClip(this, i);
        }
        perceptClips = new PerceptClip[numPercept];
        compClips = new ArrayList<>();
        lastUsedEdges = new ArrayList<>();


    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("--------------- ECM -----------------\n");
        for (Clip perceptClip : perceptClips) {
            if (perceptClip == null)
                continue;
            sb.append(perceptClip).append("\n");
            for (Edge e : perceptClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }
        for (CompositionClip compClip : compClips) {
            if (compClip == null)
                continue;
            sb.append(compClip).append("\n");
            for (Edge e : compClip.edges) {
                sb.append("\t").append(e).append("\n");
            }
        }


        sb.append("-------------------------------------\n");
        return sb.toString();
    }

    public int getAction(int percept) {
        currentReflection = 0;
//        if(deliberationLength==2){
//            if(percept==0 || percept==1) return 0;
//            return 1;
//        }


        //if unseen percept, create new
        if (perceptClips[percept] == null) {
            PerceptClip perceptClip = new PerceptClip(this, percept);
            perceptClip.initDefaultActions();
            perceptClips[percept] = perceptClip;
        }

        lastUsedEdges.clear();

        Clip clip = perceptClips[percept];
        Edge edge;
        int debugLoopCount = 0;
        do {
            edge = clip.getStrongestEdge();
            debugLoopCount++;
            if (debugLoopCount > 5) {
                System.out.println("WTF");
            }
            lastUsedEdges.add(edge);
            clip = edge.end;

        } while (!(clip instanceof ActuatorClip));

        return edge.end.id;
    }


    @Override
    public Integer getAction(Integer percept) {
        return getAction(percept);
    }

    public void giveReward(boolean success) {
        if (lastUsedEdges.size() > 1) {
            System.out.println("Going to damp : " + lastUsedEdges + " : " + success);
        }
        if (success && lastUsedEdges.size() == 2) {
            // (s,s1,s2,..,a) -> (pc, cc) , (cc,a)
            Edge pcTOccEdge = lastUsedEdges.get(0);
            Edge ccTOaEdge = lastUsedEdges.get(1);

            pcTOccEdge.weight+=1;
            ccTOaEdge.weight+=pcTOccEdge.weight;

            //direct edge update with 1
            pcTOccEdge.start.getEdgeToClip(ccTOaEdge.end).weight+=1;

        }


        for (Clip c : perceptClips) {
            if (c == null) continue;
            for (Edge e : c.edges) {
                e.weight-= gamma*(e.weight-1);
//                if (lastUsedEdges.contains(e))
//                    e.giveReward(success);

//                else e.damp();
            }
        }
        for (Clip c : compClips) {
            for (Edge e : c.edges) {
                e.weight-= gamma*(e.weight-1);
//                if (lastUsedEdges.contains(e) && success)
//                    e.giveReward(success);
//                else e.damp();
            }
        }

//        for (Edge e : lastUsedEdges)
//            e.damp(success);


        if (deliberationLength < 2)
            return;

        if (lastUsedEdges.size() != 1)
            return;


        //if this is a really good edge. consider making it composition
        Edge edge = lastUsedEdges.get(0);

        if (edge.weight < 40)
            return;

        double hoppingProb = edge.start.getHoppingPropability(edge.end);
        if (hoppingProb < 0.99)
            return;
        if (edge.start instanceof CompositionClip)
            return; //We don't allow comp clips to form new comp clips

        if (!(edge.end instanceof ActuatorClip))
            return; //We don't allow percept -> comp to make new comp

        PerceptClip start = (PerceptClip) edge.start;
//        System.out.println(edge.end.c);
        ActuatorClip end = (ActuatorClip) edge.end;

        int newId = perceptClips.length + start.id;
        StringBuilder sb = new StringBuilder();
        sb.append("We allready had theses comp clips:\n");
        for (PerceptClip clip : compClips) {

            if (clip.id == newId)
                return;

            Edge edge1 = clip.getStrongestEdge();
            if (edge1.end.id == end.id)
                return;
            sb.append("\t Loop checking : " + edge1 + " is not the same as " + end.id + " \n");
        }

        CompositionClip newComp = new CompositionClip(this, newId, start);

        compClips.add(newComp);
        Edge newEdge = new Edge(this, start, newComp, edge.weight + 100);
        start.addEdge(newEdge);

//        System.out.printf("Making ficinous clip %s , since the %s is strong\n", newComp, edge);

        sb.append("But we decided to add clip : " + newComp).append("\n");
        sb.append("And the edge : " + newEdge);
        if (compClips.size() > 2) {
            System.out.println("----------------- Error ---------------------");
            System.out.println(sb.toString());
            for (CompositionClip clip : compClips)
                System.out.println(clip + " " + clip.getStrongestEdge());
            System.out.println("----------------- Error End ---------------------");
        }
        System.out.println("Making a ficitonus clip  id=" + newId + " : pointing at action " + edge.end);

//        System.out.println("\n");
    }

    @Override
    public void useSoftMax(boolean useSoftMax) {

    }

    @Override
    public void reset() {

    }


    public static void main(String[] args) {

//        printPerceptTraining(50);
        test();
    }

    public static void gui() {

    }

    public static void test() {
        int D = 2;
        int reflection = 2;
        ECM ecm = new ECM(4, 2, D, reflection);

        Random random = new Random(System.currentTimeMillis());
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};

//        System.out.println(ecm);


        //train only 2 actions
        System.out.println("\t Training with 2 percepts only");
        for (int i = 0; i < 200; i++) {
            int percept = percepts[random.nextBoolean() ? 1 : 2];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
//            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));
        }
        System.out.println(ecm);

        for (int i = 0; i < 5; i++) {
            int percept = percepts[0];
            int action = ecm.getAction(percept);
            boolean isCorrect2 = isCorrect(percept, action);
            ecm.giveReward(isCorrect2);
            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect2));
        }


        System.out.println("\t Training with all percepts");
        for (int i = 0; i < 200; i++) {
            int percept = percepts[random.nextInt(percepts.length)];
            int action = ecm.getAction(percept);
            ecm.giveReward(isCorrect(percept, action));
        }

        System.out.println(ecm);

        System.out.println("\t Results");
        for (int i = 0; i < percepts.length; i++) {

            int percept = percepts[i];
            int action = ecm.getAction(percept);
            boolean isCorrect = isCorrect(percept, action);
            ecm.giveReward(isCorrect);
            System.out.println(String.format("P:%d -> A:%d : %s", percept, action, isCorrect));

        }

        ecm.getAction(1);

    }


    public static void printPerceptTraining(int numEpochs) {
        ECM ecm = new ECM(4, 2, 1, 1);

        for (int i = 0; i < numEpochs; i++) {
            double avg = 0;
            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean correct = isCorrect(percept, action);
                ecm.giveReward(correct);
                avg += correct ? 1 : 0;

            }
            avg /= 4;
            System.out.println(i + " " + avg);
        }

    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }
}
