package ps.ps_new;

/**
* Created by Oystein on 26/02/15.
*/
class ActuatorClip extends Clip {
    public ActuatorClip(ECM ecm,int id) {
        super(ecm,id);
    }

    @Override
    public String toString() {
        return "ActionClip " + id;
    }
}
