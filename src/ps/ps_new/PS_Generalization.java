package ps.ps_new;

import ps.v1.InterfacePS;
import utils.MyUtils;

import java.util.*;

/**
 * Clip Glow with association. Clip to clip etc.
 * <p>
 * Using the idea Clip glow.
 * Each clip can be exited
 * Created by Oystein on 10/04/15.
 */
public class PS_Generalization {
    public int WILDCARD = -1;
    private double gamma;

    public static Random random = new Random(System.currentTimeMillis());

    public List<Edge> currentSequenceEdges = new LinkedList<>();

    public ArrayList<ArrayList<Clip>> clipLayers;

    int perceptLayer = 0;
    int actionLayer;
    int numCategories;

    public PS_Generalization(double gamma, int[][] actions, int numCategories) {
        this.numCategories = numCategories;
//        System.out.println("Number of categories: " + numCategories);
        int numLayers = numCategories + 2;
        actionLayer = numLayers - 1;
        clipLayers = new ArrayList<>();
        for (int i = 0; i < numLayers; i++) {
            clipLayers.add(new ArrayList<>());
        }
        this.gamma = gamma;

        for (int[] action : actions) {
            clipLayers.get(actionLayer).add(new Clip(action, true));
        }
    }

    public int[] getAction(int[] percept) {


        Clip perceptClip = new Clip(percept);

        int indexOf = clipLayers.get(perceptLayer).indexOf(perceptClip);
        if (indexOf == -1) {
            addClip(perceptClip);
        } else {
            perceptClip = clipLayers.get(perceptLayer).get(indexOf);
        }

        return perceptClip.hop();
    }

    private int[] createWildCard(int[] p1, int[] p2) {
        int[] newP = new int[p1.length];
        for (int i = 0; i < p1.length; i++) {
            newP[i] = p1[i] == p2[i] ? p1[i] : WILDCARD;
        }
        return newP;
    }

    private int countDiffElements(int[] p1, int[] p2) {
        int diff = 0;
        for (int i = 0; i < p1.length; i++) {
            diff += p1[i] == p2[i] ? 0 : 1;
        }
        return diff;
    }

    private void addClip(Clip perceptClip) {
        //look for match in clip layer
        for (Clip clip : clipLayers.get(perceptLayer)) {
            int diffElements = countDiffElements(perceptClip.percept, clip.percept);
            if (diffElements <= 0 || diffElements > numCategories) continue;

            int[] wildCardPercept = createWildCard(perceptClip.percept, clip.percept);
            Clip wildCardClip = new Clip(wildCardPercept);

            if (clipLayers.get(diffElements).contains(wildCardClip)) continue;

            clipLayers.get(diffElements).add(wildCardClip);
//            addWildCardClip(wildCardClip);
            addOrComfirmAllEdgesExist();
        }


        clipLayers.get(perceptLayer).add(perceptClip);

        addOrComfirmAllEdgesExist();
        // add direct edge to action clips
//        clipLayers.get(actionLayer).forEach(perceptClip::addEdge);


    }

    /**
     * Call this for all clips (except action clips) every time a clip is created
     * @param clip this
     * @param thisLayer current layer
     */
    private void addEdgesToEveryThingBelow(Clip clip, int thisLayer){
        for (int i = thisLayer+1; i < clipLayers.size(); i++) {
            clipLayers.get(i).forEach(clip::addEdge);
        }
    }
    private void addOrComfirmAllEdgesExist(){
        //skip action layer
        for (int i = 0; i < clipLayers.size() - 1; i++) {
            for (Clip c: clipLayers.get(i)){
                addEdgesToEveryThingBelow(c,i);
            }
        }
    }

//    private void addWildCardClip(Clip wildCardClip) {
////        System.out.println("Created new WildCard clip " + wildCardClip);
//        for (Clip actionClip : clipLayers.get(actionLayer)) {
//            wildCardClip.addEdge(actionClip);
//        }
//
//        for (Clip perceptClip : clipLayers.get(perceptLayer)) {
//            perceptClip.addEdge(wildCardClip);
//        }
//
//    }

    public void giveReward(double reward) {
//        System.out.println();
//        System.out.println("|S|=" + currentSequenceEdges.size() + " : " + currentSequenceEdges);
//        System.out.println();

        for (ArrayList<Clip> clipLayer : clipLayers) {
            for (Clip clip : clipLayer) {
                for (Edge edge : clip.edges) {
                    if (currentSequenceEdges.contains(edge))
                        edge.update(reward);
                    else edge.update(0);
                }
            }
        }


        currentSequenceEdges.clear();

    }


    public class Clip {
        public int[] percept;
        public List<Edge> edges = new ArrayList<>();
        public boolean isActionClip = false;

        public Clip(int[] percept) {
            this.percept = percept;
        }

        public Clip(int[] percept, boolean isActionClip) {
            this.percept = percept;
            this.isActionClip = isActionClip;
        }

        public void addEdge(Clip toClip) {
            if(edges.contains(new Edge(this, toClip))){
                return;
            }
            edges.add(new Edge(this, toClip));
        }

        @Override
        public String toString() {

            if (isActionClip) {
                return String.format("A%s", Arrays.toString(percept));
            }
            return String.format("P%s", Arrays.toString(percept));
//            return "C{" + Arrays.toString(percept) + '}';
        }

        public double getHoppingPropabilityToClip(Clip end) {
            double sum = 0.0;
            double endH = -1;
            for (Edge edge : edges) {
                sum += edge.h;
                if (edge.end.equals(end)) {
                    endH = edge.h;
                }
            }
            return endH / sum;


        }

        public int[] hop() {
//            currentSequence.add(this);
            if (isActionClip) return percept;

            double p = random.nextDouble();
            double cumulativeProbability = 0.0;
            for (Edge edge : edges) {
                cumulativeProbability += getHoppingPropabilityToClip(edge.end);
                if (p <= cumulativeProbability) {
                    currentSequenceEdges.add(edge);
                    return edge.end.hop();
                }

            }
            return null;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Clip clip = (Clip) o;
            return Arrays.equals(percept, clip.percept);
        }

        @Override
        public int hashCode() {
            return percept != null ? Arrays.hashCode(percept) : 0;
        }
    }

    public class Edge {
        public double h = 1;
        public Clip start, end;

        public Edge(Clip start, Clip end) {
            this.start = start;
            this.end = end;
        }

        public void update(double reward) {
            h = h - gamma * (h - 1) + reward;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Edge edge = (Edge) o;
            return !(end != null ? !end.equals(edge.end) : edge.end != null) && !(start != null ? !start.equals(edge.start) : edge.start != null);
        }

        @Override
        public int hashCode() {
            int result = start != null ? start.hashCode() : 0;
            result = 31 * result + (end != null ? end.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return String.format("E{%s -> %s h=%.3f}", start, end, h);
        }
    }

    public static void main(String[] args) {
        experimentFromPaper();
    }


    public static int[] GREEN_LEFT =  {0, 0};
    public static int[] GREEN_RIGHT = {0, 1};
    public static int[] RED_LEFT =    {1, 0};
    public static int[] RED_RIGHT =   {1, 1};

    public static int[] ACTION_STOP = {0};
    public static int[] ACTION_GO =   {1};

    public static void experimentFromPaper() {


        int[][] percepts = {GREEN_LEFT, GREEN_RIGHT, RED_LEFT, RED_RIGHT};
        int[][] actions = {ACTION_STOP, ACTION_GO};


        PS_Generalization ps = new PS_Generalization(0.0, actions, percepts[0].length);

        double correctCount = 0;
        double numTests = 4000;

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] == action[0];
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[0] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }

        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] == action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }
        for (int i = 0; i < 1000; i++) {
            int[] percept = percepts[i % percepts.length];
            int[] action = ps.getAction(percept);
            boolean isCorrect = percept[1] != action[0]; //reversed
            ps.giveReward(isCorrect ? 1 : 0);
//            System.out.printf("P: %s -> A %s ? %s \n", Arrays.toString(percept), Arrays.toString(action), isCorrect ? "Correct" : "Wrong");
            correctCount += isCorrect ? 1 : 0;
        }


        System.out.println("Correct count : " + correctCount / numTests);
    }


}
