package ps.ps_new;

import ps.v1.InterfacePS;
import ps.v1.PS_Matrix;
import gui.SimpleSwingGraphGUI;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 11/04/15.
 */
public class RunPS_ASS_vs_Regular {
    Random r = new Random();

    public RunPS_ASS_vs_Regular() {

//        showGraphByAddingMoreAndMorePerceptions();
            reverseAtSomeTimeGraph();
//        graphTrainingTimesDimenstions();
    }


    public void reverseAtSomeTimeGraph() {
        Integer[] perceptArr = new Integer[]{0, 1,2,3};
        Integer[] actionArr = new Integer[]{0, 1};
        List<Integer> perceptList = Arrays.asList(perceptArr);
        List<Integer> actionList = Arrays.asList(actionArr);

        double clipGlow = 1.0;
        double gamma = 0.05;
        int reflectionTime = 1;
        int deliberationLength=2;

//        InterfacePS<Integer,Integer>  ps
        int numPercepts = perceptArr.length;
        int numActions = actionArr.length;
        List<InterfacePS<Integer, Integer>> listPS = new ArrayList<>();
        listPS.add(new PS_Association_My_ClipGlow(numPercepts, numActions, gamma,4, reflectionTime,deliberationLength, clipGlow));
        listPS.add(new PS_Association_ClipGlow(numPercepts, numActions, gamma, reflectionTime, clipGlow));
        listPS.add(new PS_Association_ClipGlow(numPercepts, numActions, gamma, 4, clipGlow));
//        listPS.add(new PS_Matrix<>(perceptList, actionList, gamma, 1.0, reflectionTime, clipGlow));
//        listPS.add(new PS_Matrix<>(perceptList, actionList, gamma, 1.0, 4, clipGlow));
////        InterfacePS<Integer,Integer>  ps4 = new PS_Matrix<>(perceptList,actionList,gamma,1.0,10,clipGlow);
//        listPS.add(new PS_Association_EdgeGlow(numPercepts, numActions, gamma, 1, 2, clipGlow));
//        listPS.add(new PS_Association_EdgeGlow(numPercepts, numActions, gamma, 4, 2, clipGlow));
//        listPS.add(new PS_Association_EdgeGlow(numPercepts, numActions, gamma, 1, 0, clipGlow));
        XYSeriesCollection collection = new XYSeriesCollection();
        int numAgents = 1000;

        int numGames = 250;
        for (InterfacePS<Integer, Integer> lPS : listPS) {
            collection.addSeries(trainPS(lPS, numGames, numAgents, numPercepts));
        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, "Invasion Game avg of " + numAgents, "Epoch", "Steps", 0, 1.1));


    }

    public void showGraphByAddingMoreAndMorePerceptions() {

        int numActions = 2;
        int numPerceptIncreasExpo = 5;


        int[] perceptsPerLvl = new int[numPerceptIncreasExpo];
        perceptsPerLvl[0] = 2;
        for (int i = 1; i < perceptsPerLvl.length; i++) {
            perceptsPerLvl[i] = perceptsPerLvl[i - 1] * 2;
        }
        int numPercepts = perceptsPerLvl[numPerceptIncreasExpo - 1];

        System.out.println("Lvls: " + Arrays.toString(perceptsPerLvl));

        Integer[] perceptArr = new Integer[numPercepts]; // all percepts 1-max
        for (int i = 0; i < perceptArr.length; i++) {
            perceptArr[i] = i;
        }


        Integer[] actionArr = new Integer[]{0, 1};
        List<Integer> perceptList = Arrays.asList(perceptArr);
        List<Integer> actionList = Arrays.asList(actionArr);


        double damperGlow = 1.0;
        double gamma = 0.02;
        int reflectionTime = 1;

//        InterfacePS<Integer,Integer>  ps
        List<InterfacePS<Integer, Integer>> listPS = new ArrayList<>();
        listPS.add(new PS_Association_ClipGlow(numPercepts, numActions, gamma, reflectionTime, damperGlow));
        listPS.add( new PS_Association_ClipGlow(numPercepts, numActions, gamma, 10, damperGlow));
//        listPS.add( new PS_Matrix<>(perceptList, actionList, gamma, 1.0, reflectionTime, damperGlow));
//        listPS.add( new PS_Matrix<>(perceptList, actionList, gamma, 1.0, 5, damperGlow));
//        listPS.add(new PS_Association_EdgeGlow(numPercepts, numActions, gamma, 1, 5, damperGlow));


        XYSeriesCollection collection = new XYSeriesCollection();
        int numAgents = 1000;

        int numGames = 200;
        for (InterfacePS<Integer, Integer> ps : listPS) {
//            ps.useSoftMax(false);
            collection.addSeries(trainPSAddingPerceptions(ps, numGames, numAgents, perceptsPerLvl));
        }




        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, "S={left,right}*{c_1,c_2 ,...}. new c every 200step. avg of " + numAgents, "Epoch", "Steps", 0.5, 1.1));

//        PS_Association_Regular debugPs = (PS_Association_Regular) ps5;
//        System.out.println(debugPs.toStringAll());
    }


    private void graphTrainingTimesDimenstions() {
        int maxNumStates = 10;
        Integer[] perceptArr = new Integer[]{0, 1, 2, 3};
        Integer[] actionArr = new Integer[]{0, 1};
        List<Integer> perceptList = Arrays.asList(perceptArr);
        List<Integer> actionList = Arrays.asList(actionArr);

        double damperGlow = 1.0;
        double gamma = 0.005;
        int reflectionTime = 1;


        for (int numStates = 2; numStates < maxNumStates; numStates += 2) {
            int numAgents = 1000;
            List<InterfacePS<Integer, Integer>> listPs = new ArrayList<>();
            for (int i = 0; i < numAgents; i++) {
                listPs.add(new PS_Association_ClipGlow(maxNumStates, 2, gamma, reflectionTime, damperGlow));
            }
            int stepsNeeded = numStepsUntilSuccess(listPs, numStates);
            System.out.println("|S|=" + numStates + " . Steps needed:" + stepsNeeded);
        }
    }

    private int numStepsUntilSuccess(List<InterfacePS<Integer, Integer>> listPs, int numPercepts) {
        Random r = new Random();
        int maxSteps = 1000;
        double avg = 0;
        for (int i = 0; i < maxSteps; i++) {
            avg = 0;
            for (InterfacePS<Integer, Integer> ps : listPs) {
                int percept = r.nextInt(numPercepts);
                int action = ps.getAction(percept);
                boolean isCorrect = isCorrect(percept, action);
                avg += isCorrect ? 1 : 0;
            }
//            System.out.println(i+" "+avg);
            avg /= (double) listPs.size();
            if (avg > 0.9) {
                System.out.println("Stopped at step " + i + " avg=" + avg);
                return i;
            }
        }
        System.out.println("Failed, last avg=" + avg);
        return maxSteps;
    }

    private XYSeries trainPS(InterfacePS<Integer, Integer> ps, int numGames, int numAgents, int numPercept) {

        //reverse at 150
        double[] avg = new double[numGames * 2];
        for (int i = 0; i < numAgents; i++) {
            ps.reset();
            List<Integer> listCorrect = new ArrayList<>();
            for (int j = 0; j < numGames; j++) {
                int percept = r.nextInt(numPercept);
                int action = ps.getAction(percept);
                boolean isCorrect = isCorrect(percept, action);
                ps.giveReward(isCorrect);
                avg[j] += isCorrect ? 1 : 0;
                listCorrect.add(isCorrect ? 1 : 0);
            }
//            System.out.println(listCorrect);

            for (int j = 0; j < numGames; j++) {
                int percept = r.nextInt(numPercept);
                int action = ps.getAction(percept);
                boolean isCorrect = !isCorrect(percept, action); //reverse
                ps.giveReward(isCorrect);
                avg[j + numGames] += isCorrect ? 1 : 0;
            }


        }
        XYSeries series = new XYSeries(ps.toString());
//        System.out.print("\n Average: ");
        for (int i = 0; i < avg.length; i++) {
            avg[i] /= (double) numAgents;
//            System.out.printf("%.2f ",avg[i]);
            series.add(i, avg[i]);
        }


        return series;

    }

    private XYSeries trainPSAddingPerceptions(InterfacePS<Integer, Integer> ps, int numGames, int numAgents, int[] perceptsSpace) {
        System.out.println("Training " + ps.toString());
        //reverse at 150
        double[] avg = new double[numGames * perceptsSpace.length];
        for (int i = 0; i < numAgents; i++) {
            ps.reset();
            int avgCounter = 0;
//            List<Integer> listCorrect= new ArrayList<>();
            for (int k = 0; k < perceptsSpace.length; k++) {


                for (int j = 0; j < numGames; j++) {
                    int percept = r.nextInt(perceptsSpace[k]);
                    int action = ps.getAction(percept);
                    boolean isCorrect = isCorrect(percept, action);
                    ps.giveReward(isCorrect);
                    avg[avgCounter++] += isCorrect ? 1 : 0;
//                    listCorrect.add(isCorrect?1:0);
                }

            }

//


        }
        XYSeries series = new XYSeries(ps.toString());
//        System.out.print("\n Average: ");
        for (int i = 0; i < avg.length; i++) {
            avg[i] /= (double) numAgents;
//            System.out.printf("%.2f ",avg[i]);
            series.add(i, avg[i]);
        }


        return series;

    }

    public static boolean isCorrect(int percept, int action) {

        return percept % 2 == action;
    }

    public static void main(String[] args) {
        new RunPS_ASS_vs_Regular();
    }
}
