package ps.ps_new;

import org.jgraph.JGraph;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by Oystein on 26/02/15.
 */
public class GraphSwing extends JFrame{

    private ECM ecm;

    public GraphSwing(JGraph jGraph){
//        setLayout(new BorderLayout());
//        addImpl(jGraph.getGra);
        getContentPane().add(jGraph);

    }

    JGraph jGraph;
    GraphPanel2 graphPanel2;
    public GraphSwing(ECM ecm){
        this.ecm = ecm;
        setLayout(new BorderLayout());
         graphPanel2 = new GraphPanel2(ecm,950,600);

        add(topPanel(),BorderLayout.PAGE_START);
        jGraph=graphPanel2.getGraph();
       add(jGraph, BorderLayout.CENTER);
//        setLayout(new BorderLayout());
//        addImpl(jGraph.getGra);
//        getContentPane().add(jGraph);
        initSwing();
    }

    JTextField jTFTrainTime;
    public JPanel topPanel(){
        JPanel panel = new JPanel();

       jTFTrainTime = new JTextField("100",4);
        panel.add(jTFTrainTime);
        JButton button = new JButton("Train");
        button.addActionListener(a->{
            try {
                int trainTime = Integer.parseInt(jTFTrainTime.getText());
                train(trainTime);
            }catch (Exception e){

            }
        });
        panel.add(button);
        return panel;
    }

    Random random = new Random(System.currentTimeMillis());
    public void train(int trainTime){
        System.out.println("Training "+trainTime+" times");
        for (int i = 0; i < trainTime; i++) {

            int percept = random.nextInt(4);

            int action = ecm.getAction(percept);
            boolean isCorrect = percept%2==action;
            System.out.printf("P %d -> A %d : %s \n",percept,action,isCorrect);
            ecm.giveReward(isCorrect);
//            for (int percept = 0; percept < 4; percept++) {
//                int action = ecm.getAction(percept);
//                boolean isCorrect = percept%2==action;
//                ecm.giveReward(isCorrect);
//            }
        }


//        jGraph.refresh();
        graphPanel2.update();

    }

    private void initSwing(){
        setMinimumSize(new Dimension(1200, 700));
        setLocation(0,300);
        setVisible(true);
        setFocusable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    public static void main(String[] args) {
        ECM ecm = new ECM(4,2,2,2);

        for (int i = 0; i < 1; i++) {

            for (int percept = 0; percept < 4; percept++) {
                int action = ecm.getAction(percept);
                boolean isCorrect = percept%2==action;
                ecm.giveReward(isCorrect);
            }
        }

            new GraphSwing(ecm);

    }
}
