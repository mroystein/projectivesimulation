package ps.ps_new;

import ps.v1.InterfacePS;
import utils.MyUtils;

import java.util.*;

/**
 * Not clip glow, but regular assosication. from first paper
 * Allowing hopping from clip to clip
 * Created by Oystein on 10/04/15.
 */
public class PS_Association_EdgeGlow implements I_ECM, InterfacePS<Integer, Integer> {
    public boolean debugPrint = false;

    Random random = new Random();
    double[][] hActions;
    double[][] gActions;
    //    double[] clipGlow;
//    double[] clipGlowAction;
    boolean[][] emotionClipToAction;
    boolean[][] emotionClipToClip;

    double[][] hClips;
    double K = 2;

    public double damperGlow = 1.0;
    public double gamma = 0.001;
    public double reward = 1;

    List<Edge> lastUsedEdges;
    public int maxReflectionTime = 0;
    public int reflectionTime;

    public int maxReflectionTimeClip = 10;
    //public int reflectionTimeC;

    public int maxDeliberation;
    public int deliberationLength;


    int numPercepts;
    int numActions;


    int debugCounter = 0;
    LinkedList<Integer> exitedPercepts;

    public double IS_UNSEEN_CLIP=-1.0;

    //(perceptList,actionList,damping,1.0,maxReflectionTime,damperGlow);

    public PS_Association_EdgeGlow(int numPercepts, int numActions, double gamma, int maxReflectionTime, int maxDeliberation, double damperGlow) {
        this(numPercepts, numActions);
        this.gamma = gamma;
        this.maxReflectionTime = maxReflectionTime;
        this.damperGlow = damperGlow;
        this.maxDeliberation = maxDeliberation;
    }

    public PS_Association_EdgeGlow(int numPercepts, int numActions) {
        this.numPercepts = numPercepts;
        this.numActions = numActions;

        hActions = new double[numPercepts][numActions];
        gActions = new double[numPercepts][numActions];
        hClips = new double[numPercepts][numPercepts];
//        clipGlow = new double[numPercepts];
//        clipGlowAction = new double[numActions];

        emotionClipToAction = new boolean[numPercepts][numActions];
        emotionClipToClip = new boolean[numPercepts][numPercepts];
        reset();

        exitedPercepts = new LinkedList<>();
    }


    @Override
    public String toString() {
        return "PS-Asso edgeglow γ=" + gamma + ", |S/A|=" + numPercepts + "/" + numActions + " R=" + maxReflectionTime + ", g=" + damperGlow+", D="+maxDeliberation+  ", K="+K;

    }

    public String toStringAll() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numPercepts; i++) {

            sb.append(String.format("Clip %d ", i));
            String sActions = "";
            for (int j = 0; j < numActions; j++) {

                sActions += String.format("{A%d h=%.1f %s }", j, hActions[i][j], (emotionClipToAction[i][j] ? ":)" : ":("));
            }
            sb.append(sActions);
            sb.append(" | To other Clips :");
            String sClips = "";
            for (int j = 0; j < numPercepts; j++) {

                sClips += String.format("{Clip%d h=%.1f %s} ", j, hClips[i][j], (emotionClipToClip[i][j] ? ":)" : ":("));
            }
            sb.append(sClips);
            sb.append("\n");
        }


        return sb.toString();
    }

    public void reset() {

        //Clip to Action
        for (int i = 0; i < numPercepts; i++) {
            Arrays.fill(hActions[i], IS_UNSEEN_CLIP);
            Arrays.fill(gActions[i], 0.0);
            Arrays.fill(emotionClipToAction[i], true);
            Arrays.fill(emotionClipToClip[i], true);
        }
        //Clip to Clip
        for (int i = 0; i < numPercepts; i++) {
            Arrays.fill(hClips[i], K);
        }
        //individual clip glow
//        Arrays.fill(clipGlow, 0.0);
//        Arrays.fill(clipGlowAction, 0.0);

        debugCounter = 0;
    }

    private double getHoppingPropabilityToAction(int perceptInd, int actionInd) {
        return MyUtils.softMaxIgnoreNegative(hActions[perceptInd], hActions[perceptInd][actionInd]);
//        return MyUtils.softMax(hActions[perceptInd], hActions[perceptInd][actionInd]);
    }

    private double getHoppingPropabilityToClip(int c1, int c2) {
        //original uses the simple formula
        return MyUtils.softMaxIgnoreNegative(hClips[c1], hClips[c1][c2]);
//        return MyUtils.softMax(hClips[c1], hClips[c1][c2]);
    }

    private int getStrongestClip(int clip1) {
        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < hClips[clip1].length; i++) {
            cumulativeProbability += getHoppingPropabilityToClip(clip1, i);
            if (p <= cumulativeProbability) {
                return i;
            }
        }
        return -1;
    }

    int exitedAction = -1;
    int debugLastPercept = -1;


    //public List<Integer> sequenceClips;
    @Override
    public int getAction(int percept) {
        if(hActions[percept][0]==IS_UNSEEN_CLIP)
            Arrays.fill(hActions[percept],1.0);
//        doRandomActionChance++;
        //debugLastPercept=
//        clipGlow[percept] = 1.0;
        exitedPercepts.add(percept);
        int action;
        boolean forcedToExit = reflectionTime >= maxReflectionTime;
        //Reflect as long as have time.
        while (!forcedToExit) {
            action = getDirectAction(percept);
            if (emotionClipToAction[percept][action]) {
                exitedAction = action;
                //hActions[percept][action]=1.0;
                return action;
            }
            reflectionTime++;
            forcedToExit = reflectionTime >= maxReflectionTime - 1;
        }
        reflectionTime = 0; //reset here?

        //if no deliberation is on, or max, return random..
        if (deliberationLength >= maxDeliberation) {
            exitedAction = random.nextInt(numActions);
            //cant do anything. Return random
            return exitedAction;
        }

        deliberationLength++;

        int reflectionTimeClip=0;
        int clip2;
        do {
             clip2 = getStrongestClip(percept);
            if (emotionClipToClip[percept][clip2]) {
                action = getAction(clip2);

                return action;
            }

            reflectionTimeClip++;
        } while (reflectionTimeClip < maxReflectionTimeClip);


        //Start a random sequence S = (s1,s2,..,sD,a)

        return getAction(clip2);
    }

   
    public int getAction2(int percept) {


        int clip = percept;
        int action=-1;
        for (int i = 0; i < maxDeliberation+1; i++) {
            exitedPercepts.add(clip);
             action = getDirectAction(clip);
            if(emotionClipToAction[clip][action]){
                exitedAction = action;
                return action;
            }
            clip =getStrongestClip(clip);

        }
        exitedAction = action;
        return action;

//        int action;
//        boolean forcedToExit = reflectionTime >= maxReflectionTime;
//        //Reflect as long as have time.
//        while (!forcedToExit) {
//            action = getDirectAction(percept);
//            if (emotionClipToAction[percept][action]) {
//                exitedAction = action;
//                //hActions[percept][action]=1.0;
//                return action;
//            }
//            reflectionTime++;
//            forcedToExit = reflectionTime >= maxReflectionTime - 1;
//        }
//        reflectionTime = 0; //reset here?
//
//        //if no deliberation is on, or max, return random..
//        if (deliberationLength >= maxDeliberation) {
//            exitedAction = random.nextInt(numActions);
//            //cant do anything. Return random
//            return exitedAction;
//        }
//
//        deliberationLength++;
//
//        int reflectionTimeClip=0;
//        int clip2;
//        do {
//            clip2 = getStrongestClip(percept);
//            if (emotionClipToClip[percept][clip2]) {
//                action = getAction(clip2);
//
//                return action;
//            }
//
//            reflectionTimeClip++;
//        } while (reflectionTimeClip < maxReflectionTimeClip);
//
//
//        //Start a random sequence S = (s1,s2,..,sD,a)
//
//        return getAction(clip2);
    }

    private int getDirectAction(int percept) {
        if(hActions[percept][0]==IS_UNSEEN_CLIP)
            Arrays.fill(hActions[percept],1.0);

        double p = random.nextDouble();
        double cumulativeProbability = 0.0;
        for (int i = 0; i < hActions[percept].length; i++) {
            cumulativeProbability += getHoppingPropabilityToAction(percept, i);
            if (p <= cumulativeProbability) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public Integer getAction(Integer percept) {
        int p = percept;
        return getAction2(p);
    }

    @Override
    public void giveReward(boolean success) {
//        giveRewardNew(success);
        giveRewardArray(success);
    }

    public void giveRewardArray(boolean success) {
        deliberationLength = 0;//reset
        double reward = success ? 1.0 : 0.0;

        if (debugPrint)
            System.out.println("Exited Clips: " + exitedPercepts.toString() + " , leading to action " + exitedAction);

        int firstClip = exitedPercepts.getFirst();
        //give reward to the exited clips, if rewarded
        if (success) {
            //Direct
            if(firstClip==-1 || exitedAction==-1)
                System.out.println("wtf");
            hActions[firstClip][exitedAction] += 1;
            if (exitedPercepts.size() == 2) {
                int secondClip = exitedPercepts.get(1);
                hClips[firstClip][secondClip] += K;
            }
            if (exitedPercepts.size() > 2) {
//                System.out.println("Not done yet!! " + "Exited Clips: " + exitedPercepts.toString() + " , leading to action " + exitedAction);
                for (int i = 1; i < exitedPercepts.size()-1; i++) {

                    Integer first = exitedPercepts.get(i);
                    Integer second = exitedPercepts.get(i + 1);
                    double old =  hClips[first][second];
                    hClips[first][second]+=K;
//                    System.out.println(String.format("hClip[%d][%d] : %.2f -> %.2f",first,second,old,hClips[first][second]));
                }
            }
        }
        if (exitedPercepts.size() == 2) {
            emotionClipToClip[firstClip][exitedPercepts.get(1)] = success;
        }

        emotionClipToAction[firstClip][exitedAction] = success;

        //damp all !


        //direct sequences
        for (int i = 0; i < numPercepts; i++) {
            for (int j = 0; j < numActions; j++) {
                if(hActions[i][j]==IS_UNSEEN_CLIP) continue;
                hActions[i][j]=  hActions[i][j] -gamma * (hActions[i][j] - 1);
            }

        }

        //percept to percept
        for (int i = 0; i < numPercepts; i++) {
//            boolean wasExitedClip = clipGlow[i]==1.0;
            for (int j = 0; j < numPercepts; j++) {

                hClips[i][j] = hClips[i][j] -gamma * (hClips[i][j] - K);

            }
        }
//        Arrays.fill(clipGlow, 0.0);
//        Arrays.fill(clipGlowAction, 0.0);

        exitedPercepts.clear();
        exitedAction = -1;
    }

    @Override
    public void useSoftMax(boolean useSoftMax) {

    }


    public static void main(String[] args) {
        Random r = new Random();
        int[] percepts = {0, 1, 2, 3};
        int[] actions = {0, 1};
        int D = 4; // direct
        int maxReflection = 1;
        PS_Association_EdgeGlow ps = new PS_Association_EdgeGlow(percepts.length, actions.length, 0.05, maxReflection, D, 1.0);

        for (int i = 0; i < percepts.length; i++) {
            System.out.println(String.format("Percept: %d   Action: 0=%s , 1=%s", percepts[i], isCorrectOdd(i, 0), isCorrectOdd(i, 1)));
        }

        System.out.println(ps.toStringAll());
//        System.out.println("------ Start training -----");
//        for (int i = 0; i < 500; i++) {
//            int percept = percepts[r.nextInt(percepts.length)];
//            int action = ps.getAction(percept);
//            boolean isCorrect = isCorrect(percept, action);
//            ps.giveReward(isCorrect);
//
//        }

        List<Integer> listCorrect = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            int percept = i % 4;//percepts[r.nextInt(percepts.length)];
            if (i > 400) {
                System.out.println();
            }
            int action = ps.getAction(percept);
            boolean isCorrect = isCorrectOdd(percept, action);

            String riktig = isCorrect ? "JA" : "NEI";
            System.out.println(String.format("%d | Percept: %d   Action chosen: %d , riktig= %s", i, percept, action, riktig));

            ps.giveReward(isCorrect);

            listCorrect.add(isCorrect ? 1 : 0);
        }
        System.out.println(listCorrect);
        System.out.println(ps.toStringAll());

        System.out.println("Avg: " + listCorrect.stream().mapToInt(i -> i).average().getAsDouble());



         listCorrect = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            int percept = i % 4;//percepts[r.nextInt(percepts.length)];
            if (i > 400) {
                System.out.println();
            }
            int action = ps.getAction(percept);
            boolean isCorrect = !isCorrectOdd(percept, action);

            String riktig = isCorrect ? "JA" : "NEI";
            System.out.println(String.format("%d | Percept: %d   Action chosen: %d , riktig= %s", i, percept, action, riktig));

            ps.giveReward(isCorrect);

            listCorrect.add(isCorrect ? 1 : 0);
        }
        System.out.println(listCorrect);
        System.out.println(ps.toStringAll());

        System.out.println("Avg: " + listCorrect.stream().mapToInt(i -> i).average().getAsDouble());
    }

    public static boolean isCorrectOdd(int percept, int action) {
        return percept % 2 == action;
    }

    public static boolean isCorrect(int percept, int action) {
        if (percept == 0 || percept == 1)
            return action == 0;
        return action == 1;
    }
}
