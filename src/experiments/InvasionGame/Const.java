package experiments.InvasionGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Oystein on 28.08.14.
 */
public class Const {

    public static final int SYMBOL_LEFT = -1, SYMBOL_RIGHT = 1;
    public static final int ACTION_LEFT = -2, ACTION_RIGHT = 2;

    public static final int SYMBOL_2_LEFT = -3, SYMBOL_2_RIGHT = 3;
    public static final int ACTION_2_LEFT = -4, ACTION_2_RIGHT = 4;


    public static final int SYMBOL_BLUE_LEFT = -10, SYMBOL_BLUE_RIGHT = 10;
    public static final int SYMBOL_RED_LEFT = -11, SYMBOL_RED_RIGHT = 11;




    public static List<Integer> getSymbols(){
        Integer[] a = {SYMBOL_LEFT,SYMBOL_RIGHT};
        return new ArrayList<>(Arrays.asList(a));
    }
    public static List<Integer> getSymbolsFour(){
        Integer[] a = {SYMBOL_LEFT,SYMBOL_RIGHT,SYMBOL_2_LEFT,SYMBOL_2_RIGHT};
        return new ArrayList<>(Arrays.asList(a));
    }

    public static List<Integer> getActions(){
        Integer[] a = {ACTION_LEFT,ACTION_RIGHT};
        return new ArrayList<>(Arrays.asList(a));
    }
    public static List<Integer> getActionsFour(){
        Integer[] a = {ACTION_LEFT,ACTION_RIGHT,ACTION_2_LEFT,ACTION_2_RIGHT};
        return new ArrayList<>(Arrays.asList(a));
    }

    public static List<Integer> getSymbols2Colour(){
        Integer[] a = {SYMBOL_BLUE_LEFT,SYMBOL_RED_LEFT,SYMBOL_BLUE_RIGHT,SYMBOL_RIGHT};
        return new ArrayList<>(Arrays.asList(a));
    }


    public static String symbolToString(int symbol){
        switch (symbol) {
            case Const.ACTION_RIGHT:
                return "ActionRight";
            case Const.ACTION_LEFT:
                return "ActionLeft";
            case Const.SYMBOL_LEFT:
                return "SymbolLeft";
            case Const.SYMBOL_RIGHT:
                return "SymbolRight";

            case Const.SYMBOL_BLUE_LEFT:
                return "SymbolBlueLeft";
            case Const.SYMBOL_BLUE_RIGHT:
                return "SymbolBlueRight";


            case Const.ACTION_2_RIGHT:
                return "Action_2_Right";
            case Const.ACTION_2_LEFT:
                return "Action_2_Left";
            case Const.SYMBOL_2_LEFT:
                return "Symbol_2_Left";
            case Const.SYMBOL_2_RIGHT:
                return "Symbol_2_Right";



        }
        return "Unknown";
    }
}
