package experiments.InvasionGame;

import experiments.IWorld;

import java.util.Random;

/**
 * Invasion Game. Aka Blocking Game
 *
 * Generic by making all odd numbers
 *
 * Created by Oystein on 15/04/15.
 */
public class InvasionGame implements IWorld {

    int numStates;
    private int newPerceptEveryTimeStep;
    int lastState =-1;
    int lastAction;
    Random r = new Random();
    int gamesPlayed=0;
    int reverseAtStep=200;
    private  int totalGames;

    private int numStatesCurrently=2;

    boolean isGameOver=false;

    public InvasionGame(int numStates,int reverseAtStep, int totalGames) {

        this.numStates = numStates;
        this.reverseAtStep = reverseAtStep;
        this.totalGames = totalGames;
        lastState= r.nextInt(numStates);
        numStatesCurrently=numStates;
    }


    public InvasionGame(int numStates,int reverseAtStep, int totalGames,int newPerceptEveryTimeStep) {
        this.reverseAtStep = reverseAtStep;
        this.totalGames = totalGames;
        this.numStates = numStates;
        this.newPerceptEveryTimeStep = newPerceptEveryTimeStep;
        numStatesCurrently=2;

        lastState= r.nextInt(numStates);

        int nIncreasingSpace = totalGames/newPerceptEveryTimeStep;

        System.out.println("Will increase the space "+nIncreasingSpace+" times. Last |S|="+Math.pow(2,nIncreasingSpace) );
    }


    @Override
    public int getState() {
        return lastState;
    }

    @Override
    public int doAction(int action) {

        if(isGameOver) return -1;
        gamesPlayed++;
        lastAction=action;
        isGameOver=true;
        return 0;
    }

    @Override
    public boolean hasWon() {
        return isGameOver;
    }

    @Override
    public boolean hasGameEnded() {
        return isGameOver;
    }

    @Override
    public double getReward() {
        double r = wasLastActionCorrect()?1:0;
       return r;
    }

    private boolean wasLastActionCorrect(){
        boolean reverse = (gamesPlayed>reverseAtStep);
        boolean correct =  (lastAction%2==0) ;
        if(reverse)
            correct= (lastAction%2==1) ;
        return correct;
    }
    @Override
    public int getNumberOfStates() {
        return numStates;
    }

    @Override
    public int getNumberOfActions() {
        return 2;
    }

    @Override
    public void reset() {
        lastState= r.nextInt(numStatesCurrently);
        isGameOver=false;
        if(gamesPlayed>=totalGames) {
//            System.out.println("numStates in end "+numStatesCurrently);
            gamesPlayed = 0;
            numStatesCurrently=2;//hack

        }

        if(gamesPlayed>0 && gamesPlayed<1000 && newPerceptEveryTimeStep>0&& gamesPlayed%newPerceptEveryTimeStep==0)
            numStatesCurrently+=2;


    }

    @Override
    public int getStepCounter() {
        return wasLastActionCorrect()?1:0;
    }

    @Override
    public String toString() {
        return String.format("Invasion Game |S|=%d |A|=%d",numStates,2);
    }

    public static void main(String[] args) {
        IWorld world = new InvasionGame(2,100,200);
        Random r=  new Random(System.currentTimeMillis());
        for (int i = 0; i < 100; i++) {

            while (!world.hasWon()){

                world.doAction(r.nextInt(2));
                System.out.println(world.getReward());

            }
            world.reset();

        }
    }
}
