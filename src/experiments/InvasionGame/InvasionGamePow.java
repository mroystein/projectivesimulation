package experiments.InvasionGame;

import experiments.IWorld;

import java.util.Random;

/**
 * Invasion Game. Aka Blocking Game
 * <p>
 * Used for adding new percepts at time t. Increase by double .
 * <p>
 * Created by Oystein on 15/04/15.
 */
public class InvasionGamePow implements IWorld {

    int numStates;
    int lastState = -1;
    int lastAction;
    Random r = new Random();
    int gamesPlayed = 0;
    private int totalGames;

    private int numStatesCurrently = 2;

    boolean isGameOver = false;


    public InvasionGamePow(int numStates, int totalGames) {
        this.numStates = numStates;
        this.totalGames = totalGames;
        lastState = r.nextInt(numStates);
        numStatesCurrently = 2;
    }


    @Override
    public int getState() {
        return lastState;
    }

    @Override
    public int doAction(int action) {

        if (isGameOver) return -1;
        gamesPlayed++;
        lastAction = action;
        isGameOver = true;
        return 0;
    }

    @Override
    public boolean hasWon() {
        return isGameOver;
    }

    @Override
    public boolean hasGameEnded() {
        return isGameOver;
    }

    @Override
    public double getReward() {
        return (wasLastActionCorrect() ? 1.0 : 0.0);
    }

    private boolean wasLastActionCorrect() {
        return (lastAction % 2 == 0);
    }

    @Override
    public int getNumberOfStates() {
        return numStates;
    }

    @Override
    public int getNumberOfActions() {
        return 2;
    }

    @Override
    public void reset() {
        lastState = r.nextInt(numStatesCurrently);
        isGameOver = false;

        //Reset for a new agent.
        if (gamesPlayed >= totalGames) {
//            System.out.println("numStates in end "+numStatesCurrently);
            gamesPlayed = 0;
            numStatesCurrently = 2;//hack

        }
        boolean print = false;
        if (gamesPlayed > 0 && gamesPlayed % (numStatesCurrently * 10 + 50) == 0) {
            if (print) {
                int start = numStatesCurrently * 10 + 50;
                int end = numStatesCurrently * 2 * 10 + 50;
                System.out.println(gamesPlayed + ": Num States increased to " + numStatesCurrently + " start=" + start + " end=" + end);
            }
            numStatesCurrently *= 2;
        }


    }

    @Override
    public int getStepCounter() {
        return wasLastActionCorrect() ? 1 : 0;
    }

    @Override
    public String toString() {
        return String.format("Invasion Game |S|=%d |A|=%d", numStates, 2);
    }

    public static void main(String[] args) {
        IWorld world = new InvasionGamePow(1000, 2000);
        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < 2000; i++) {

            while (!world.hasWon()) {

                world.doAction(r.nextInt(2));
//                System.out.println(world.getReward());

            }
            world.reset();

        }
    }
}
