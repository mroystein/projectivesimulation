package experiments.mountaincar;

import ps.v1.InterfacePS;
import ps.v1.PS_Interface;
import ps.v1.PS_Matrix;
import ps.v1.ProjectionSimulationGen;
import gui.SimpleSwingGraphGUI;
import org.jfree.data.xy.XYSeries;
import old.rl.MCWorldRunnable;
import old.rl.MountainCarWorldRL;
import old.rl.RLearner;
import utils.Result;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Oystein on 02/10/2014.
 */
public class MCAgent implements PS_Interface<CoordinatePair, Integer> {

    private MountainCarWorld world;
    public InterfacePS<CoordinatePair, Integer> ps;
    private final List<CoordinatePair> perceptions;
    private PS_Matrix<CoordinatePair, Integer> psMatrix;

    private int MAX_TRIES_ONE_GAME = 500000; //hundredtousan
    private List<Integer> actions;

    public MCAgent(MountainCarWorld world, boolean useMatrix, int n) {
        this.world = world;

        //Set up PS
        perceptions = generatePerceptionList(n);
        actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);

        if (useMatrix)
            ps = new PS_Matrix<>(perceptions, actions, 0.0, 1, 1, 0.02);

        else ps = new ProjectionSimulationGen<>(this, perceptions, actions, 0.0, 20, 1, 0.02);


        //timeTest();
    }

    public MCAgent(MountainCarWorld world, int n) {
        this.world = world;

        //Set up PS
        perceptions = generatePerceptionList(n);
        actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);

        ps = new ProjectionSimulationGen<>(this, perceptions, actions, 0.0, 20, 1, 0.02);


        psMatrix = new PS_Matrix<>(perceptions, actions, 0.0, 20, 1, 0.02);

        //timeTest();
    }

    public MCAgent(int n) {
        world = new MountainCarWorld(false);
        perceptions = generatePerceptionList(n);
        actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);


       fastTraining();
//        bruteForceBestParams();
//        showGraphsComparisonOfBest();

    }


    private boolean containsXYSeries(List<XYSeries> list, String name){

        return list.stream().filter(xy-> xy.getKey().equals(name)).findFirst().isPresent();
    }

    public void showGraphsComparisonOfBest() {
        String filename = "xySeriesSarsaVsPS3_1000agents.ser-IGNORE";
        ArrayList<XYSeries> loadedXYSeriesArray=readFile(filename);
        int numAgents = 1000;
        int numGames = 400;

        ArrayList<XYSeries> xySeriesArray = new ArrayList<>();
        XYSeries series=null;
        List<CoordinatePair> coordinatePairs = generatePerceptionList(8);
        PS_Matrix<CoordinatePair, Integer> psm = new PS_Matrix<>(coordinatePairs, actions, 0.00001, 1, 1, 0.0025);

        if(!containsXYSeries(loadedXYSeriesArray,psm.toString()))
            xySeriesArray.add(createSeries(psm, numGames, numAgents, coordinatePairs));
        else
            System.out.println("Already contains..");




        //Result{name='Agent: damping: 1.0E-5, Glow: 0.011, TabSize:12', steps=166.35} after 400 epochs.
        List<CoordinatePair> coordinatePairs2 = generatePerceptionList(10);
        PS_Matrix<CoordinatePair, Integer> psm2 = new PS_Matrix<>(coordinatePairs2, actions, 0.00001, 1, 1, 0.011);
        if(!containsXYSeries(loadedXYSeriesArray,psm2.toString()))
            xySeriesArray.add(createSeries(psm2,numGames,numAgents,coordinatePairs2));

        //SARSA
        MCWorldRunnable mcWorldRunnable = new MCWorldRunnable(true);

        MountainCarWorldRL worldRL = new MountainCarWorldRL(8);
        //0Result{name='RLeaner {alpha=0.1400, damping=1.0000, lambda=0.30, epsilon=0.0650, SARSA, GREEDY TabSize :8', steps=171.20}

        RLearner learner = new RLearner(worldRL, 0.11, 0.8740,0.02, RLearner.SARSA, RLearner.E_GREEDY);
        if(!containsXYSeries(loadedXYSeriesArray, learner.toString()))
            xySeriesArray.add(mcWorldRunnable.createXYSeries(numAgents, numGames, learner));

        RLearner learner2 = new RLearner(worldRL, 0.12, 0.999,0.03, RLearner.SARSA, RLearner.E_GREEDY);
        if(!containsXYSeries(loadedXYSeriesArray, learner2.toString()))
         xySeriesArray.add(mcWorldRunnable.createXYSeries(numAgents, numGames, learner2));

        RLearner learner3 =  new RLearner(worldRL,0.14,1.0,0.065,RLearner.SARSA,RLearner.E_GREEDY);
        if(!containsXYSeries(loadedXYSeriesArray, learner2.toString()))
            xySeriesArray.add(mcWorldRunnable.createXYSeries(numAgents, numGames, learner3));



        System.out.println(xySeriesArray.size() +" new agents added.");

        loadedXYSeriesArray.addAll(xySeriesArray);
        writeToFile(loadedXYSeriesArray, filename);




//        series.equals(null);


        EventQueue.invokeLater(() ->   new SimpleSwingGraphGUI(loadedXYSeriesArray.toArray(new XYSeries[loadedXYSeriesArray.size()])));
    }


    public static void writeToFile(ArrayList<XYSeries> array,String filename){
        try {
            FileOutputStream fout = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(array);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ArrayList<XYSeries> readFile(String filename){
        try {
            FileInputStream streamIn = new FileInputStream(filename);
            ObjectInputStream objectInputStream = new ObjectInputStream(streamIn);
            ArrayList<XYSeries> arr= (ArrayList<XYSeries>) objectInputStream.readObject();
            return arr;

        } catch (Exception e) {
//            e.printStackTrace();
            System.err.println("Can't ready file."+filename);
        }
        return new ArrayList<>();
    }


    private XYSeries createSeries(PS_Matrix<CoordinatePair, Integer> psm, int numGames, int numAgents, List<CoordinatePair> coordinatePairs){
        double[] avgArr = new double[numGames];

        for (int i = 0; i < numAgents; i++) {

            psm.reset();
            ArrayList<Integer> histo = trainAgent(psm, numGames, coordinatePairs,false);
            for (int stepCount = 0; stepCount < histo.size(); stepCount++) {
//                avgArray.set(stepCount,avgArray.get(stepCount)+histo.get(stepCount));
                avgArr[stepCount] += histo.get(stepCount);
            }
        }

        XYSeries series = new XYSeries(psm.toString()+"");
        for (int i = 0; i < avgArr.length; i++) {
            avgArr[i]/=numAgents;
            series.add(i,avgArr[i]);
        }
        System.out.println(psm+" last epoch used "+avgArr[avgArr.length-1]+" steps");

        return series;
    }

    public void normalTraining() {
        int nGames = 100;
        int sampleSize = 1000;
        double gamma = 0.0000, glow = 0.01;
        int reward = 1, reflection = 1;

//        ArrayList<ProjectionSimulationGen<CoordinatePair, Integer>> ps1 = new ArrayList<>(sampleSize);
        ArrayList<PS_Matrix<CoordinatePair, Integer>> ps2 = new ArrayList<>(sampleSize);

        for (int i = 0; i < sampleSize; i++) {
            ps2.add(new PS_Matrix<>(perceptions, actions, gamma, reward, reflection, glow));
        }

        System.out.println("--------- Training NEW PS agents ----------");
        ArrayList<Integer> histogram = new ArrayList<>();
        double countNew = 0;
        long startMatrix = System.currentTimeMillis();
        int loopCount = 0;
        for (PS_Matrix<CoordinatePair, Integer> ps : ps2) {
            loopCount++;
            System.out.println("\t-- Agent " + loopCount + " --");
            int stepsUsedInLastGame = trainAgentGetLastStepCount(ps, nGames);
            histogram.add(stepsUsedInLastGame);
            countNew += stepsUsedInLastGame;
        }
        double avgNew = countNew / sampleSize;
        long ellaspedMatix = System.currentTimeMillis() - startMatrix;

        System.out.println("Number of agents of each class: " + sampleSize);


        System.out.println("------------- Matrix --------------");
        System.out.println("Matrix: Avarage after " + nGames + " games is : " + avgNew + ". time used :" + ellaspedMatix);

        checkHistogram(histogram);
    }

    public void fastTraining() {
//        world = new MountainCarWorld(true);
//        perceptions = generatePerceptionList(n);
//        actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);

        long startTime = System.currentTimeMillis();
        int nGames = 100;
        int sampleSize = 100;
        double gamma = 0.00001, glow = 0.01;
        int reward = 10, reflection = 1;


        double totalSteps = 0;
        for (int i = 0; i < sampleSize; i++) {
            PS_Matrix<CoordinatePair, Integer> psm = new PS_Matrix<>(perceptions, actions, gamma, reward, reflection, glow);

            int stepsUsed = trainAgentSimpleLastStep(psm, nGames, world);
            System.out.println(i + " : " + stepsUsed + " steps");
            totalSteps += stepsUsed;


        }
        long ellasped = System.currentTimeMillis() - startTime;


        System.out.println("Number of agents of each class: " + sampleSize);

        System.out.println("Matrix: Avarage after " + nGames + " games is : " + (totalSteps / sampleSize));
        System.out.println("Time used " + ellasped + " ms");

    }

    public void bruteForceBestParams() {
//        world = new MountainCarWorld(true);
//        perceptions = generatePerceptionList(n);
//        actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);


        long startTime = System.currentTimeMillis();
        int nGames = 100;
        int sampleSize = 20;
        double gamma = 0.00001, glow = 0.01;
        int reward = 1, reflection = 1;

        List<Result> results = new ArrayList<>();
        double totalSteps = 0;
        for (int tabSize = 6; tabSize < 22; tabSize += 2) {
            List<CoordinatePair> perceptionsN = generatePerceptionList(tabSize);

            for (double gammaLoop = 0.00001; gammaLoop < 0.001; gammaLoop += 0.2001) {//10?

                for (double glowLoop = 0.0005; glowLoop < 0.02; glowLoop += 0.002) {

                    totalSteps = 0;

                    //regular loop
                    for (int i = 0; i < sampleSize; i++) {
                        PS_Matrix<CoordinatePair, Integer> psm = new PS_Matrix<>(perceptionsN, actions, gammaLoop, reward, reflection, glowLoop);

                        int stepsUsed = trainAgentSimpleLastStep(psm, nGames, world, perceptionsN);
//                    System.out.println(i+" : "+stepsUsed+" steps");
                        totalSteps += stepsUsed;


                    }
//                System.out.println("Number of agents of each class: " + sampleSize);
                    String agent = "Agent: damping: " + gammaLoop + ", Glow: " + glowLoop + ", TabSize:" + tabSize;
                    double avgSteps = (totalSteps / sampleSize);
                    results.add(new Result(agent, avgSteps));
                    System.out.println(avgSteps + " steps." + agent);
//                    System.out.println("Avarage after " + nGames + " games is : " +);
//                System.out.println("Time used "+ellasped+" ms");
//                    System.out.println();
                }
            }
        }

        Collections.sort(results);
        System.out.println("------- Best agents --------");
        for (int i = 0; i < 6; i++) {
            System.out.println(results.get(i));
        }

        long ellasped = System.currentTimeMillis() - startTime;


    }


    /**
     * For memory Test
     *
     * @param world
     * @param n
     * @param regular
     */
    public MCAgent(MountainCarWorld world, int n, boolean regular) {
        this.world = world;
        int nGames = 500;
        //Set up PS
        perceptions = generatePerceptionList(n);
        List<Integer> actions = Arrays.asList(world.ACTION_LEFT, world.ACTION_NEUTRAL, world.ACTION_RIGHT);


        if (regular) {
            ps = new ProjectionSimulationGen<>(this, perceptions, actions, 0.0, 20, 1, 0.02);

            trainAgent(ps, nGames);
        } else {
            psMatrix = new PS_Matrix<>(perceptions, actions, 0.0, 20, 1, 0.02);
            trainAgent(psMatrix, nGames);
        }

    }


    public void timeTest() {
        int nGames = 1000;
        long start = System.currentTimeMillis();
        trainAgent(ps, nGames);
        long ellasped = System.currentTimeMillis() - start;


        long start2 = System.currentTimeMillis();
        trainAgent(psMatrix, nGames);
        long ellasped2 = System.currentTimeMillis() - start2;

        System.out.println("Old: " + ellasped);
        System.out.println("New: " + ellasped2);
    }


    public int trainAgentGetLastStepCount(InterfacePS<CoordinatePair, Integer> ps, int nGames) {
        List<Integer> l = trainAgent(ps, nGames);
        return l.get(l.size() - 1);
    }

    public int trainAgentSimpleLastStep(InterfacePS<CoordinatePair, Integer> ps, int nGames, MountainCarWorld world) {
        return trainAgentSimpleLastStep(ps, nGames, world, this.perceptions);
    }

    public int trainAgentSimpleLastStep(InterfacePS<CoordinatePair, Integer> ps, int nGames, MountainCarWorld world, List<CoordinatePair> perceptions) {
        world.reset();
        int counter = 0;
        for (int i = 0; i < nGames; i++) {
            counter = 0;
            while (!world.isGameOver()) {
                counter++;

                if (counter > MAX_TRIES_ONE_GAME) {
                    System.err.println("ran out of time at game " + i);
//                    Fuck it..
                    break;
                }


                int action = ps.getAction(find(perceptions, world.getPosition(), world.getVelocity()));
                world.update(action);

                ps.giveReward(world.isGameOver());

            }


            world.reset();

        }


        return counter;


    }

    public ArrayList<Integer> trainAgent(InterfacePS<CoordinatePair, Integer> ps, int nGames) {
        return trainAgent(ps, nGames, this.perceptions, false);
    }

    public ArrayList<Integer> trainAgent(InterfacePS<CoordinatePair, Integer> ps, int nGames, List<CoordinatePair> perceptions, boolean print) {
        long start = System.currentTimeMillis();


        MountainCarWorld world = new MountainCarWorld(false);
        ArrayList<Integer> histogram = new ArrayList<>(nGames);
        for (int i = 0; i < nGames; i++) {
            int counter = 0;
            while (!world.isGameOver()) {
                counter++;

                if (counter > MAX_TRIES_ONE_GAME) {
                    System.out.println("ran out of time at game " + i);
//                    Fuck it..
                    histogram.add(MAX_TRIES_ONE_GAME);
                    return histogram;
//                    break;
                }

                CoordinatePair percept = find(perceptions, world.getPosition(), world.getVelocity());
                if(percept ==null){
                    System.out.println("FUCK");
                }
                int action = ps.getAction(percept);
                world.update(action);
                ps.giveReward(world.isGameOver());

            }

            if (print && i % 100 == 0)
                System.out.println(i + " isGameOver ? : " + world.isGameOver() + " steps:" + counter);

            histogram.add(counter);
//            System.out.println(counter+" "+ world.getStepCounter()+" "+ (counter==world.getStepCounter()));
            world.reset();

        }
        histogram.stream().forEach(System.out::println);

        if (print) {
            System.out.println("Last : " + histogram.get(histogram.size() - 1) + ", global Min : " + histogram.stream().min(Integer::compare).get());
            long ellasped = System.currentTimeMillis() - start;
            System.out.println("Time used " + ellasped + " ms");
        }
        return histogram;


    }

    private void checkHistogram(ArrayList<Integer> histogram) {
        double avg = 0;
        int errors = 0;
        for (Integer stepCount : histogram) {
            if (stepCount == MAX_TRIES_ONE_GAME) {
                errors++;
            } else {
                avg += stepCount;
            }
        }

        avg = avg / (histogram.size() - errors);
        double successRate = 1 - ((double) errors / (double) histogram.size());
        System.out.println("Histogram: " + histogram.toString());
        System.out.println("Average (without errors):" + avg + " . # errors :" + errors + " out of " + histogram.size() + " : Success: " + successRate + "%");

    }


    public void train(int nGames) {
        trainAgent(ps, nGames);


    }

    public void getAction() {
        double x = world.getPosition();
        double v = world.getVelocity();
        CoordinatePair cp = find(perceptions, x, v);


        int action = ps.getAction(cp);
        world.update(action);

        if (world.isGameOver()) {
            ps.giveReward(true);

        } else
            ps.giveReward(false);
    }

    private List<CoordinatePair> generatePerceptionList(int n) {
        List<CoordinatePair> posList = generatePositionPairs(n);
        List<CoordinatePair> veloList = generateVelocityPairs(n);

        ArrayList<CoordinatePair> perceptions = new ArrayList<>(n * n);
        for (CoordinatePair posPair : posList) {
            for (CoordinatePair veloPair : veloList) {
                CoordinatePair cp = new CoordinatePair(posPair.x1, posPair.x2, veloPair.x1, veloPair.x2);
                perceptions.add(cp);
            }
        }
//        System.out.println("Perceptions size: "+perceptions.size());
        return perceptions;
    }


    private List<CoordinatePair> generatePositionPairs(int n) {
        ArrayList<CoordinatePair> pairs = new ArrayList<>();
        double width = (Math.abs(MountainCarWorld.MIN_POSITION) + MountainCarWorld.MAX_POSITION) / n;

        for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION - 0.001; i += width) {
            pairs.add(new CoordinatePair(i, i + width));
        }
        return pairs;
    }

    private List<CoordinatePair> generateVelocityPairs(int n) {
        ArrayList<CoordinatePair> pairs = new ArrayList<>();
        double width = (Math.abs(MountainCarWorld.MIN_VELOCITY) + MountainCarWorld.MAX_VELOCITY) / n;

        for (double i = MountainCarWorld.MIN_VELOCITY; i < MountainCarWorld.MAX_VELOCITY - 0.001; i += width) {
            pairs.add(new CoordinatePair(i, i + width));
        }
        return pairs;
    }


    private CoordinatePair find(List<CoordinatePair> list, double x, double v) {
        for (CoordinatePair cp : list) {
            if (cp.contains(x, v))
                return cp;
        }
        System.out.println(""+x+","+v+" was not found!!");
        return null;

    }


    public static void main(String[] args) {
//        MountainCarWorld mountainCarWorld = new MountainCarWorld();
//        MCAgent experiments = new MCAgent(mountainCarWorld, 20);


        MCAgent agent = new MCAgent(20);
        System.out.println("DONE");



    }










    public static void memoryTest() {
        MCAgent agent = new MCAgent(new MountainCarWorld(), 20, false);


        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory is bytes: " + memory);
        System.out.println("Used memory is megabytes: "
                + bytesToMegabytes(memory));
    }

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    @Override
    public boolean isCorrectActionGen(CoordinatePair percept, Integer action) {

        //just to do something
//        if(world.isWinningMove(action)) {
//            System.out.println("isCorrectAction TRUE");
//            return true;
//        }
        return false;
    }
}
