package experiments.mountaincar;

import experiments.IWorld;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 15.09.14.
 */
public class MountainCarWorldGen implements IWorld {

    public final int ACTION_LEFT = -1, ACTION_NEUTRAL = 0, ACTION_RIGHT = 1;
    public static int MAX_STEPS_BEFORE_QUIT=1_000_000;

    public static final double MIN_VELOCITY = -0.07, MAX_VELOCITY = 0.07;
    public static final double MIN_POSITION = -1.2, MAX_POSITION = 0.6;

    public double startPosition = -0.5, startVelocity = 0.0;

    public double position;
    public double velocity;

    private double hillPeakFrequency = 3.0;
    public double gravityFactor = 0.0025;

    private boolean isGameOver = false;

    private int stepCounter = 0;
    private int gamesCounter = 1;
    private boolean randomStart = false;
    private int numUniqueStates;
    public int uniqueState;

    private Random r = new Random(System.currentTimeMillis());


    private int normalizedIntervalSizePos, normalizedIntervalSizeVelo;
    public int normalizedPosition, normalizedVelocity;

    // -1.2 - 0.6 | +2.2 -> [1.0, 2,8]
    public MountainCarWorldGen() {
        position = startPosition;
        velocity = startVelocity;
    }

    public MountainCarWorldGen(int normalizedIntervalSize) {
        this();

        this.normalizedIntervalSizePos = normalizedIntervalSize;
        this.normalizedVelocity = normalizedIntervalSize;
    }

    public MountainCarWorldGen(int normalizedIntervalSizePos, int normalizedIntervalSizeVelo) {
        this();
        this.normalizedIntervalSizePos = normalizedIntervalSizePos;
        this.normalizedIntervalSizeVelo = normalizedIntervalSizeVelo;
        numUniqueStates = normalizedIntervalSizePos * normalizedIntervalSizeVelo;
    }

    public MountainCarWorldGen(boolean randomStart) {
        this.randomStart = randomStart;
        if (randomStart) {

            position = MIN_POSITION + (MAX_POSITION - MIN_POSITION) * r.nextDouble();
            velocity = MIN_VELOCITY + (MAX_VELOCITY - MIN_VELOCITY) * r.nextDouble();
        } else {
            position = startPosition;
            velocity = startVelocity;
        }
    }

    public List<Integer> getActions() {
        return Arrays.asList(ACTION_LEFT, ACTION_NEUTRAL, ACTION_RIGHT);
    }


    public int mapToUnqInt(int a, int b, int l) {
        return a * l + b;
    }

    public int[] mapToUnqIntRev(int l, int unq) {
        int b = unq % l;
        int a = unq / l;
        return new int[]{a, b};
    }


    //mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position)
    public static double mapRange(double a1, double a2, double b1, double b2, double s) {
        return b1 + ((s - a1) * (b2 - b1)) / (a2 - a1);
    }
    public static double mapRange2(double xMin, double xMax, double b1, double b2, double s) {
        return b1 + ((s - xMin) * (b2 - b1)) / (xMax - xMin);
    }

    public static double mapRangeFrom0(double xMin, double xMax, double nRange, double x) {
        return ((x - xMin) * nRange) / (xMax - xMin);
    }

    @Override
    public int getState() {
        return getUniqueState();
    }

    @Override
    public int doAction(int action) {
        if (action < 0 || action > 3)
            System.err.println("invalid action " + action + " in doAction() in MCWGen");
        update(action - 1);
        return getUniqueState();
    }

    @Override
    public boolean hasWon() {
        return isGameOver();
    }

    @Override
    public boolean hasGameEnded() {
        return isGameOver();
    }

    @Override
    public double getReward() {
        return hasWon() ? 1000 : -1;
    }

    @Override
    public int getNumberOfStates() {
        return numUniqueStates;
    }

    @Override
    public int getNumberOfActions() {
        return 3;
    }

    @Override
    public void reset() {
        isGameOver = false;

        if (randomStart) {
            position = MIN_POSITION + (MAX_POSITION - MIN_POSITION) * r.nextDouble();
            velocity = MIN_VELOCITY + (MAX_VELOCITY - MIN_VELOCITY) * r.nextDouble();
        } else {
            position = startPosition;
            velocity = startVelocity;
        }

        stepCounter = 0;
        gamesCounter++;
    }


    public int getUniqueState() {
        return uniqueState;
    }


    public void update(int action) {
        if (action < -1 || action > 1)
            System.out.println("Wrong action!!");

        if (isGameOver)
            return;
        stepCounter++;

        velocity = velocity + 0.001 * action - gravityFactor * Math.cos(3 * position);

        position = position + velocity;

        if (velocity > MAX_VELOCITY)
            velocity = MAX_VELOCITY;
        if (velocity < MIN_VELOCITY)
            velocity = MIN_VELOCITY;


        // System.out.println("new pos: " + position);

        if (position < MIN_POSITION) {
            position = MIN_POSITION;
            velocity = 0.0;
        }
        if (position > MAX_POSITION) {
            isGameOver = true;
            position = MAX_POSITION;
            velocity = 0.0;
        }
        if(stepCounter>=MAX_STEPS_BEFORE_QUIT){
            isGameOver=true;
            position = MAX_POSITION;
            velocity = 0.0;
            System.err.println("MC world  had to give up: stepsSize "+stepCounter);
        }


        normalizedPosition = (int) Math.round(mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position));
        normalizedVelocity = (int) Math.round(mapRange(MIN_VELOCITY, MAX_VELOCITY, 0, normalizedIntervalSizeVelo - 1, velocity));
//        System.out.println("norm Pos: " + normalizedPosition+", norm Velo: "+normalizedVelocity);
        uniqueState = normalizedPosition * normalizedIntervalSizeVelo + normalizedVelocity;
    }


    /**
     * Get the height of the hill at this position
     *
     * @param queryPosition
     * @return
     */
    public double getHeightAtPosition(double queryPosition) {
        return -Math.sin(hillPeakFrequency * (queryPosition));
    }

    /**
     * Get the slop of the hill at this position
     *
     * @param queryPosition
     * @return
     */
    public double getSlope(double queryPosition) {
        /*The curve is generated by cos(hillPeakFrequency(x-pi/2)) so the
         * pseudo-derivative is cos(hillPeakFrequency* x)
         */
        return Math.cos(hillPeakFrequency * queryPosition);
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public int getStepCounter() {
        return stepCounter;
    }

    public int getGamesCounter() {
        return gamesCounter;
    }


    public double getPosition() {
        return position;
    }

    public double getVelocity() {
        return velocity;
    }


    private static int cordinateTrans(double pos) {
        double x = (pos + 2.2) * 200 - 100;
        return (int) x;
    }


    public static void main(String[] args) {
        MountainCarWorldGen mc = new MountainCarWorldGen();

      mc.bruteForceBestPath3Ways();
//        mc.bruteForceBestPath4Ways();
//        mc.randomAvgerage(1000);
//
    }

    public static void normalizeTest() {
        int normalizedIntervalSizePos = 10;
        int normalizedIntervalSizeVelo = 5;
        for (double position = MIN_POSITION; position < MAX_POSITION; position += 0.1) {
            for (double velocity = MIN_VELOCITY; velocity < MAX_VELOCITY; velocity += 0.01) {
                int normalizedPosition = (int) Math.round(mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position));
                int normalizedVelocity = (int) Math.round(mapRange(MIN_VELOCITY, MAX_VELOCITY, 0, normalizedIntervalSizeVelo - 1, velocity));
//        System.out.println("norm Pos: " + normalizedPosition+", norm Velo: "+normalizedVelocity);
                int uniqueState = normalizedPosition * normalizedIntervalSizeVelo + normalizedVelocity;
                System.out.printf("p=%.2f v=%.2f -> %d\n", position, velocity, uniqueState);
            }
            System.out.println();
        }
    }

    public static int mine(int a, int b, int l) {
        return a * l + b;
    }

    public static int[] mineRev(int l, int unq) {
        int b = unq % l;
        int a = unq / l;
        return new int[]{a, b};
    }

    public static double cantorPairing(double k1, double k2) {
        return 0.5 * (k1 + k2) * (k1 + k2 + 1) + k2;
    }

    public static double cantorPairing2(double a, double b) {
        return (a + b) * (a + b + 1) / 2 + a;
    }

    public static int cantorPairingInt(double a, double b) {
        return (int) Math.round((a + b) * (a + b + 1) / 2 + a);
    }

//    public static double cantorPairing2(double a, double b){
////        return  ( a + b) * (a + b + 1) / 2 + a;
//    }


    public static void bruteforce() {

        MountainCarWorldGen mcw = new MountainCarWorldGen();
        double gravity = 0.0025;

        while (true) {

            for (int i = 0; i < 36; i++) {
                mcw.update(-1);
            }
            for (int i = 0; i < 63; i++) {
                mcw.update(1);
            }


            System.out.println(gravity + " : +" + mcw.isGameOver());

            if (mcw.isGameOver()) {

                System.out.println("Winner: +" + gravity);
                break;
            }
            gravity -= 0.00001;
            //0.00236

            mcw.reset();
            mcw.gravityFactor = gravity;

        }

    }

    @Override
    public String toString() {
        return "Mountain Car |S|="+normalizedIntervalSizePos+"*"+normalizedIntervalSizeVelo;
//        return String.format("Mountain Car pos=%.6f -> %d (v=%.6f -> %d)", position, normalizedPosition, velocity, normalizedVelocity);
    }


    public void bruteForceBestPath3Ways() {
        int leastSteps = Integer.MAX_VALUE;

        for (int right1 = 0; right1 < 107; right1++) {
            for (int left = 0; left < 107; left++) {
                for (int right2 = 0; right2 < 107; right2++) {

                    reset();

                    for (int i = 0; i < right1; i++) {
                        update(1);
                    }
                    for (int i = 0; i < left; i++) {
                        update(-1);
                    }
                    for (int i = 0; i < right2; i++) {
                        update(1);

                        if (isGameOver()) {
                            if (getStepCounter() <= leastSteps) {
                                leastSteps = getStepCounter();
                                System.out.println(String.format("%d,%d,%d : %d or %d steps", right1, left, right2, leastSteps, getStepCounter()));
                            }
                            break;
                        }

                    }
                }
            }
        }
        System.out.println("Least steps: " + leastSteps);
    }

    public void bruteForceBestPath4Ways() {
        int leastSteps = Integer.MAX_VALUE;
        int maxSteps = 60;

        for (int right1 = 0; right1 < 60; right1++) {
            System.out.println(right1);
            for (int left = 0; left < 60; left++) {

                for (int right2 = 0; right2 < 60; right2++) {

                    for (int left2 = 0; left2 < 60; left2++) {

                        reset();

                        for (int i = 0; i < left; i++) {
                            update(-1);
                        }

                        for (int i = 0; i < right1; i++) {
                            update(1);
                        }

                        for (int i = 0; i < left2; i++) {
                            update(-1);
                        }
                        for (int i = 0; i < right2; i++) {
                            update(1);

                            if (isGameOver()) {
                                if (getStepCounter() < leastSteps) {
                                    leastSteps = getStepCounter();
                                    System.out.println(String.format("%d,%d,%d,%d : %d steps", left, right1, left2, right2, leastSteps));
                                }
                                break;
                            }

                        }
                    }
                }
            }
        }
        System.out.println("Least steps: " + leastSteps);
    }


    public void randomAvgerage(int nAgents) {
        double avg = 0;
        for (int i = 0; i < nAgents; i++) {
            avg += randomAverage();
        }
        avg /= (double) nAgents;
        System.out.println("Random average " + avg);
    }

    public int randomAverage() {
        Random r = new Random(System.currentTimeMillis());
        reset();
        int maksCounter = 1_000_000;
        while (!isGameOver()) {
            int action = r.nextInt(3) - 1;
            update(action);

            if (getStepCounter() > maksCounter) {
                System.out.println("fail");
                break;
            }

        }
        return getStepCounter();

    }


}
