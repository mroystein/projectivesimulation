package experiments.mountaincar;

/**
 * Created by Oystein on 16/03/15.
 */
public class RangeMap {

    private final int numX;
    private final int numY;

    double minX = MountainCarWorld.MIN_POSITION;
    double maxX = MountainCarWorld.MAX_POSITION;


    CoordinatePair[] pairs;

    public RangeMap(int numX, int numY) {


        this.numX = numX;
        this.numY = numY;

        pairs = new CoordinatePair[numX];
    }


    public void fastLookUpPerceptions() {
        int n = numX;

        double width = (Math.abs(MountainCarWorld.MIN_POSITION) + MountainCarWorld.MAX_POSITION) / n;

        int c = 0;
        for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION - 0.001; i += width) {
            pairs[c++] = new CoordinatePair(i, i + width);
        }

        for (int i = 0; i < n; i++) {
            System.out.println(i + " : " + pairs[i]);
        }

        //example
        double x = -0.90;

        for (int i = 0; i < n; i++) {
            double start = mapRange(0, 10, i);
            double end = mapRange(0, 10, i + 1);
            double l = mapRange(minX, maxX, 0, 10, start);
            System.out.printf("%.2f [%.2f,%.2f]\n", l, start, end);
        }

        int rev = (int) Math.round(mapRange(minX, maxX, 0, 10, -0.90));

        System.out.println(rev);
    }

    public void debug(){
        for (double i = minX; i <= maxX; i+=0.1) {
//            CoordinatePair c = get
//            System.out.println(i+" in interval "+);
        }
    }

    public CoordinatePair getIntValueForPos(double x, double y) {

        int xVal = round(mapRange(minX, maxX, 0, numX, x));
        return pairs[xVal];

    }

    public int round(double x) {
        return (int) Math.round(x);
    }

    public static double mapRange(double a1, double a2, double s) {
        return mapRange(a1, a2, MountainCarWorld.MIN_POSITION, MountainCarWorld.MAX_POSITION, s);
    }

//    public static double mapRangeRev( double b1, double b2, double s){
//        return mapRange()
//    }

    public static double mapRange(double a1, double a2, double b1, double b2, double s) {
        return b1 + ((s - a1) * (b2 - b1)) / (a2 - a1);
    }


    public static void main(String[] args) {
        RangeMap rangeMap = new RangeMap(10, 10);
//        rangeMap.fastLookUpPerceptions();

        normalize();

    }

    public static void normalize(){
        double oldMin=MountainCarWorld.MIN_POSITION;
        double oldRange = MountainCarWorld.MAX_POSITION+Math.abs(MountainCarWorld.MIN_POSITION);
        System.out.println(oldRange);

        double newMin=0;
        double newRange=10;

        for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION - 0.001; i += 0.1) {

        }

    }
}
