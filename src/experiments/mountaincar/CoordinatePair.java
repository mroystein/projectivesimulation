package experiments.mountaincar;

/**
 * Created by Oystein on 02/10/2014.
 */
public class CoordinatePair {
    public double x1, x2;
    public double v1, v2;

    public CoordinatePair(double x1, double x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public CoordinatePair(double x1, double x2, double v1, double v2) {
        this.x1 = x1;
        this.x2 = x2;
        this.v1 = v1;
        this.v2 = v2;
    }

    public boolean contains(double x, double v){
        if(x<x1 || x>x2)
            return false;

        if(v<v1 || v>v2)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return String.format("x=[%.2f,%.2f], v=[%.2f,%.2f]",x1,x2,v1,v2);
//            return  "x=(" + x1 + ", " + x2 + "), v=(" + v1 + ", " + v2 + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoordinatePair that = (CoordinatePair) o;

        if (Double.compare(that.v1, v1) != 0) return false;
        if (Double.compare(that.v2, v2) != 0) return false;
        if (Double.compare(that.x1, x1) != 0) return false;
        if (Double.compare(that.x2, x2) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x1);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(x2);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(v1);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(v2);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
