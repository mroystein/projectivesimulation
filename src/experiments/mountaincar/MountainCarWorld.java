package experiments.mountaincar;

import ps.v1.InterfacePS;
import ps.v1.PS_Matrix;
import ps.v2.PS_MatrixSimple;
import gui.SimpleSwingGraphGUI;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import utils.entities.PairG;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 15.09.14.
 */
public class MountainCarWorld{

    public final int ACTION_LEFT = -1, ACTION_NEUTRAL = 0, ACTION_RIGHT = 1;

    public static final double MIN_VELOCITY = -0.07, MAX_VELOCITY = 0.07;
    public static final double MIN_POSITION = -1.2, MAX_POSITION = 0.5;

    public double startPosition = -0.5, startVelocity = 0.0;

    public double position;
    public double velocity;

    private double hillPeakFrequency = 3.0;
    public double gravityFactorCheat = 0.00236;
    public double gravityFactor = 0.0025;

    private boolean isGameOver = false;

    private int stepCounter = 0;
    private int gamesCounter = 1;
    private boolean randomStart = false;

    private Random r = new Random(System.currentTimeMillis());


    private int normalizedIntervalSizePos,normalizedIntervalSizeVelo;
    public int normalizedPosition, normalizedVelocity;

    // -1.2 - 0.6 | +2.2 -> [1.0, 2,8]
    public MountainCarWorld() {
        position = startPosition;
        velocity = startVelocity;
    }

    public MountainCarWorld(int normalizedIntervalSize) {
        this();

        this.normalizedIntervalSizePos = normalizedIntervalSize;
        this.normalizedVelocity=normalizedIntervalSize;
    }
    public MountainCarWorld(int normalizedIntervalSizePos,int normalizedIntervalSizeVelo) {
        this();
        this.normalizedIntervalSizePos = normalizedIntervalSizePos;
        this.normalizedIntervalSizeVelo = normalizedIntervalSizeVelo;

    }

    public MountainCarWorld(boolean randomStart) {
        this.randomStart = randomStart;
        if (randomStart) {

            position = MIN_POSITION + (MAX_POSITION - MIN_POSITION) * r.nextDouble();
            velocity = MIN_VELOCITY + (MAX_VELOCITY - MIN_VELOCITY) * r.nextDouble();
        } else {
            position = startPosition;
            velocity = startVelocity;
        }
    }

    public List<Integer> getActions() {
        return Arrays.asList(ACTION_LEFT, ACTION_NEUTRAL, ACTION_RIGHT);
    }


    public int mapToUnqInt(int a, int b, int l) {
        return a * l + b;
    }

    public int[] mapToUnqIntRev(int l, int unq) {
        int b = unq % l;
        int a = unq / l;
        return new int[]{a, b};
    }


    public static double mapRange(double a1, double a2, double b1, double b2, double s) {
        return b1 + ((s - a1) * (b2 - b1)) / (a2 - a1);
    }



    public void reset() {
        isGameOver = false;

        if (randomStart) {
            position = MIN_POSITION + (MAX_POSITION - MIN_POSITION) * r.nextDouble();
            velocity = MIN_VELOCITY + (MAX_VELOCITY - MIN_VELOCITY) * r.nextDouble();
        } else {
            position = startPosition;
            velocity = startVelocity;
        }

        stepCounter = 0;
        gamesCounter++;
    }


    public int getUniqueState() {
        return uniqueState;
    }


    public void update(int action) {
        if (action < -1 || action > 1)
            System.out.println("Wrong action!!");

        if (isGameOver)
            return;
        stepCounter++;


        boolean simpleWorld = false;
        if (simpleWorld)
            velocity = velocity + 0.001 * action - gravityFactorCheat * Math.cos(3 * position);
        else
            velocity = velocity + 0.001 * action - gravityFactor * Math.cos(3 * position);

        //velocity = velocity + action * 0.001 + Math.cos(3 * position) * (-0.0025);
//        velocity = velocity + 0.001 * action -0.00236 * Math.cos(3 * position) ;
//        velocity = velocity + 0.001 * (double)action -0.002455 * Math.cos(3 * position) ;
//        velocity += (0.001 * action) + (-0.0024) * Math.cos(3.0 * position) ;

        position = position + velocity;

        if (velocity > MAX_VELOCITY)
            velocity = MAX_VELOCITY;
        if (velocity < MIN_VELOCITY)
            velocity = MIN_VELOCITY;


        // System.out.println("new pos: " + position);

        if (position < MIN_POSITION) {
            position = MIN_POSITION;
            velocity = 0.0;
        }
        if (position > MAX_POSITION) {
            isGameOver = true;
            position = MAX_POSITION;
            velocity = 0.0;
        }


        normalizedPosition = (int) Math.round(mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position));
        normalizedVelocity = (int) Math.round(mapRange(MIN_VELOCITY, MAX_VELOCITY, 0, normalizedIntervalSizeVelo - 1, velocity));
//        System.out.println("norm Pos: " + normalizedPosition+", norm Velo: "+normalizedVelocity);
        uniqueState = normalizedPosition * normalizedIntervalSizeVelo + normalizedVelocity;
    }

    public int uniqueState;


    /**
     * Get the height of the hill at this position
     *
     * @param queryPosition
     * @return
     */
    public double getHeightAtPosition(double queryPosition) {
        return -Math.sin(hillPeakFrequency * (queryPosition));
    }

    /**
     * Get the slop of the hill at this position
     *
     * @param queryPosition
     * @return
     */
    public double getSlope(double queryPosition) {
        /*The curve is generated by cos(hillPeakFrequency(x-pi/2)) so the
         * pseudo-derivative is cos(hillPeakFrequency* x)
         */
        return Math.cos(hillPeakFrequency * queryPosition);
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public int getStepCounter() {
        return stepCounter;
    }

    public int getGamesCounter() {
        return gamesCounter;
    }


    public double getPosition() {
        return position;
    }

    public double getVelocity() {
        return velocity;
    }


    private static int cordinateTrans(double pos) {
        double x = (pos + 2.2) * 200 - 100;
        return (int) x;
    }


    public static void fastPSMCMulti() {
//        int nPos = 20;
//        int nVelo=5;


//        PS_Matrix<Integer,Integer> ps = new PS_Matrix<>()

        int[] nPosArr ={10,15,20,30};
        int[] nVeloArr ={5,6,10};
        double[] damperGlows={0.02
                ,0.005,0.01 ,0.03,0.1
        };
        ArrayList<PairG<String,Double>> successList= new ArrayList<>();
        double bestAvg=5000;
        for (int nPos : nPosArr) {
            for (int nVelo : nVeloArr) {
                for (double damperGlow : damperGlows) {

                    MountainCarWorld mcw = new MountainCarWorld(nPos,nVelo);
                    PS_MatrixSimple ps = new PS_MatrixSimple(nPos * nVelo, 3, 0.0, 1, 1, damperGlow);
                    ArrayList<Integer> histo = new ArrayList<>();
                    boolean isFailure=false;
                    for (int i = 0; i < 200; i++) {

                        while (!mcw.isGameOver()) {
                            int state = mcw.getUniqueState();

                            int action = ps.getAction(state) - 1;
                            mcw.update(action);

                            if (mcw.isGameOver()) {
                                ps.giveReward(true);
                            }else
                                ps.giveReward(false);

                            if(mcw.getStepCounter()>1_000_000) {
                                isFailure=true;
                                break;
                            }
                        }
                        if(isFailure)
                            break;
                        histo.add(mcw.stepCounter);
//                        System.out.println(i + " | " + mcw.stepCounter + " steps");
                        mcw.reset();

                    }
                    if(isFailure) {
                        System.out.println("FAILURE: ("+nPos + "*" + nVelo + ") d="+damperGlow+"");
                        continue;
                    }
                    double avg=0.0;
                    for (int i = 150; i < 200; i++) {
                        avg+=histo.get(i);
                    }
                    avg/=50;
                    String name = avg+" :  "+nPos + "*" + nVelo + " d="+damperGlow;

                    PairG<String, Double> pair = new PairG<>(name, avg);
                    System.out.println(pair);
                    successList.add(pair);
                    if(avg<bestAvg) {
                        bestAvg = avg;

                    }

                }
            }
        }
        System.out.println("----- Successes -------");
        for (PairG<String, Double> p : successList) {
            System.out.println(p.toString());
        }


//        System.out.println(ps.biggestListSeen+ " exited edges");
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(histo, ps.toString(), "Steps ! ", "Epoch", "Steps", 0, 30000));
    }

    public static void fastPSMC() {
        int numGames=500;
        int numAgents = 10;
        int nPos = 20;
        int nVelo=20;
        MountainCarWorld mcw = new MountainCarWorld(nPos,nVelo);

//        PS_Matrix<Integer,Integer> ps = new PS_Matrix<>()
        PS_MatrixSimple ps = new PS_MatrixSimple(nPos * nVelo, 3, 0.0, 1, 1, 0.02);
        //List<Double> histo = train(ps,mcw,numAgents,numGames);


        List<Integer> percepts = new ArrayList<>();
        for (int i = 0; i < nPos*nVelo; i++) percepts.add(i);
        List<Integer> actions = new ArrayList<>();
        for (int i = 0; i < 3; i++) actions.add(i);

        PS_Matrix<Integer,Integer> psOrg = new PS_Matrix<>(percepts,actions,0.0,1,1,0.02);
        List<Double> listStandard =train(psOrg,mcw,numAgents,numGames);


        XYSeriesCollection collection = new XYSeriesCollection();
        //collection.addSeries(listToXYSeries(histo,ps.toString()));
        collection.addSeries(listToXYSeries(listStandard, psOrg.toString()));

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection,"MC world. avg of "+numAgents,"Epoch","Steps",0,2000));

    }
    public static XYSeries listToXYSeries(List<Double> list,String name){
        XYSeries series = new XYSeries(name);
        for (int i = 0; i < list.size(); i++) {
            series.add(i,list.get(i));
        }
        return series;
    }
    public static List<Double> train(InterfacePS<Integer,Integer> ps, MountainCarWorld mcw , int numAgents,int numGames){

        Double[] arr = new Double[numGames];
        for (int i = 0; i < arr.length; i++)
            arr[i]= (double) 0;


        for (int i = 0; i < numAgents; i++) {

            List<Integer> histo = train(ps,mcw,numGames);
//            System.out.println(histo.toString());

            for (int j = 0; j < arr.length; j++)
                arr[j]+=histo.get(j);


            ps.reset();
            mcw.reset();

        }
        for (int i = 0; i < arr.length; i++)
            arr[i]= arr[i]/(double)numAgents;

        System.out.println(ps.toString()+" avg at step 200 its "+arr[200]+". at 500 its "+arr[499]);

        return Arrays.asList(arr);
    }

    public static List<Integer> train(InterfacePS<Integer,Integer> ps, MountainCarWorld mcw,int numGames){

        ArrayList<Integer> histo = new ArrayList<>();
        for (int i = 0; i < numGames; i++) {
            while (!mcw.isGameOver()) {
                int state = mcw.getUniqueState();
                int action = ps.getAction(state) - 1;
                mcw.update(action);
                ps.giveReward(mcw.isGameOver());

            }
            histo.add(mcw.stepCounter);
//            if(i%50==0)
//            System.out.println(i + " | " + mcw.stepCounter + " steps");
            mcw.reset();
        }
        return histo;
    }

    public static void fastPSMC2() {
//        int nPos = 10;
//        int nVelo=10;
//        MountainCarWorld mcw = new MountainCarWorld(nPos,nVelo);
//
////        PS_Matrix<Integer,Integer> ps = new PS_Matrix<>()
////        PS_MatrixSimple ps = new PS_MatrixSimple(nPos * nVelo, 3, 0.0, 1, 1, 0.02);
//        List<Integer> percepts = new ArrayList<>();
//        for (int i = 0; i < nPos*nVelo; i++) percepts.add(i);
//        List<Integer> actions = new ArrayList<>();
//        for (int i = 0; i < 3; i++) actions.add(i);
//
//        PS_Matrix<Integer,Integer> ps = new PS_Matrix<>(percepts,actions,0.0,1,1,0.02);
//
//        train(ps,mcw);
//
//        ArrayList<Integer> histo = new ArrayList<>();
//        for (int i = 0; i < 200; i++) {
//            while (!mcw.isGameOver()) {
//                int state = mcw.getUniqueState();
//                int action = ps.getAction(state)-1;
//                mcw.update(action);
//                ps.giveReward(mcw.isGameOver());
//            }
//            histo.add(mcw.stepCounter);
//            System.out.println(i + " | " + mcw.stepCounter + " steps");
//            mcw.reset();
//        }
//
////        System.out.println(ps.biggestListSeen+ " exited edges");
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(histo, ps.toString(), "Steps ! ", "Epoch", "Steps", 0, 3000));
    }


    public static void main(String[] args) {
            MountainCarWorld mc = new MountainCarWorld();


        double numGames = 1000;
        double avg=0;
        for (int i = 0; i < numGames; i++) {
            avg+= mc.randomAverage();
        }
        avg/=numGames;
        System.out.println(avg);
//        fastPSMC();
//        fastPSMC2();
//        fastPSMCMulti();
//        MountainCarWorld mcw = new MountainCarWorld(20);
//        mcw.testRunShortestPath();
//        mcw.findShortestPath();


//        mcw.findOriginalPath();
//        mcw.myTryToFindBestPossiblePath();
//        mcw.bruteForceBestPath3Ways();
//        mcw.bruteForceBestPath4Ways();
//        bruteforce();

//        int[] counter= new int[20];
//
//        for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION - 0.001; i += 0.003) {
//            counter[mcw.normalized(i)]++;
//            System.out.printf("%.2f -> %d\n",i,mcw.normalized(i));
//        }
//
//        for (int i = 0; i < counter.length; i++) {
//            System.out.println(i+" : "+counter[i]);
//        }


        //mapping to one int
//        int nPos=10;
//        int nVelo=3;
//        int[][] matrix = new int[nPos][nVelo];
//        int counter[] = new int[nPos*nVelo];
//        for (int i = 0; i < nPos; i++) {
//            for (int j = 0; j < nVelo; j++) {
//
//                int unq = mine(i, j, nVelo);
//                int[] rev = mineRev(nPos, nVelo);
//
//                boolean correct = i==rev[0] && j==rev[1];
//                System.out.printf("%s | %d , %d -> %d -> %s\n",correct, i, j, unq , Arrays.toString(rev));
//
//                matrix[i][j]= unq;
//                counter[unq]++;
//            }
//        }
//
//        System.out.println("-----");
//
//        for (int i = 0; i < counter.length; i++) {
//            System.out.println(i+" : "+counter[i]);
//        }

//        normalizeTest();
    }

    public static void normalizeTest(){
        int normalizedIntervalSizePos=10;
        int normalizedIntervalSizeVelo=5;
        for (double position = MIN_POSITION; position < MAX_POSITION; position+=0.1) {
            for (double velocity = MIN_VELOCITY; velocity < MAX_VELOCITY; velocity+=0.01) {
                int normalizedPosition = (int) Math.round(mapRange(MIN_POSITION, MAX_POSITION, 0, normalizedIntervalSizePos - 1, position));
               int normalizedVelocity = (int) Math.round(mapRange(MIN_VELOCITY, MAX_VELOCITY, 0, normalizedIntervalSizeVelo - 1, velocity));
//        System.out.println("norm Pos: " + normalizedPosition+", norm Velo: "+normalizedVelocity);
               int uniqueState = normalizedPosition * normalizedIntervalSizeVelo + normalizedVelocity;
                System.out.printf("p=%.2f v=%.2f -> %d\n",position,velocity,uniqueState);
            }
            System.out.println();
        }
    }

    public static int mine(int a, int b, int l) {
        return a * l + b;
    }

    public static int[] mineRev(int l, int unq) {
        int b = unq % l;
        int a = unq / l;
        return new int[]{a, b};
    }

    public static double cantorPairing(double k1, double k2) {
        return 0.5 * (k1 + k2) * (k1 + k2 + 1) + k2;
    }

    public static double cantorPairing2(double a, double b) {
        return (a + b) * (a + b + 1) / 2 + a;
    }

    public static int cantorPairingInt(double a, double b) {
        return (int) Math.round((a + b) * (a + b + 1) / 2 + a);
    }

//    public static double cantorPairing2(double a, double b){
////        return  ( a + b) * (a + b + 1) / 2 + a;
//    }


    public static void bruteforce() {

        MountainCarWorld mcw = new MountainCarWorld();
        double gravity = 0.0025;

        while (true) {

            for (int i = 0; i < 36; i++) {
                mcw.update(-1);
            }
            for (int i = 0; i < 63; i++) {
                mcw.update(1);
            }


            System.out.println(gravity + " : +" + mcw.isGameOver());

            if (mcw.isGameOver()) {

                System.out.println("Winner: +" + gravity);
                break;
            }
            gravity -= 0.00001;
            //0.00236

            mcw.reset();
            mcw.gravityFactor = gravity;

        }

    }

    @Override
    public String toString() {
        return String.format("pos=%.6f -> %d (v=%.6f -> %d)", position, normalizedPosition, velocity, normalizedVelocity);
    }

    public void testRunShortestPath() {

        //i=39 is positive. stop at 38

        for (int i = 0; i < 36; i++) {
            System.out.println(i + " : " + toString());
            update(-1);

        }


        double closest = MIN_POSITION;
        System.out.println("Go right");
        for (int i = 0; i < 78; i++) {
            System.out.println(i + " : " + toString());
            update(1);

            if (position > closest)
                closest = position;

            if (closest > position)
                break;
            if (isGameOver())
                break;

        }

        System.out.println("is Game Over ? " + isGameOver() + " best pos:" + closest);


    }

    public void myTryToFindBestPossiblePath() {
        boolean shouldBreak = false;
        int totalSteps = 0;
        System.out.println("\t Go Left");
        for (int i = 0; i < 20; i++) {
            update(1);
            totalSteps++;
            System.out.println(i + " : " + toString());
            if (shouldBreak && velocity < 0) break;
        }
        System.out.println("\t Go Right");
        for (int i = 0; i < 28; i++) {
            update(-1);
            totalSteps++;
            System.out.println(i + " : " + toString());
            if (shouldBreak && velocity > 0) break;
        }
        System.out.println("\t Go Left");
        for (int i = 0; i < 200; i++) {
            update(1);
            totalSteps++;
            System.out.println(i + " : " + toString());
            if (shouldBreak && velocity < 0) break;

            if (isGameOver()) {
                System.out.println("Game over!!");
                break;
            }
        }
        System.out.println("Steps: " + totalSteps);

    }

    public void bruteForceBestPath3Ways() {
        int leastSteps = Integer.MAX_VALUE;

        for (int right1 = 0; right1 < 107; right1++) {
            for (int left = 0; left < 107; left++) {
                for (int right2 = 0; right2 < 107; right2++) {

                    reset();

                    for (int i = 0; i < right1; i++) {
                        update(1);
                    }
                    for (int i = 0; i < left; i++) {
                        update(-1);
                    }
                    for (int i = 0; i < right2; i++) {
                        update(1);

                        if (isGameOver()) {
                            if (getStepCounter() < leastSteps) {
                                leastSteps = getStepCounter();
                                System.out.println(String.format("%d,%d,%d : %d or %d steps", right1, left, right2, leastSteps,getStepCounter()));
                            }
                            break;
                        }

                    }
                }
            }
        }
        System.out.println("Least steps: " + leastSteps);
    }

    public void bruteForceBestPath4Ways() {
        int leastSteps = Integer.MAX_VALUE;
        int maxSteps = 60;

        for (int right1 = 0; right1 < 60; right1++) {
            System.out.println(right1);
            for (int left = 0; left < 60; left++) {

                for (int right2 = 0; right2 < 60; right2++) {

                    for (int left2 = 0; left2 < 60; left2++) {

                        reset();

                        for (int i = 0; i < left; i++) {
                            update(-1);
                        }

                        for (int i = 0; i < right1; i++) {
                            update(1);
                        }

                        for (int i = 0; i < left2; i++) {
                            update(-1);
                        }
                        for (int i = 0; i < right2; i++) {
                            update(1);

                            if (isGameOver()) {
                                if (getStepCounter() < leastSteps) {
                                    leastSteps = getStepCounter();
                                    System.out.println(String.format("%d,%d,%d,%d : %d steps", left, right1, left2, right2, leastSteps));
                                }
                                break;
                            }

                        }
                    }
                }
            }
        }
        System.out.println("Least steps: " + leastSteps);
    }

    public void findOriginalPath() {
        for (int i = 0; i < 36; i++) {
            System.out.println(i + " : " + toString());
            update(-1);

        }


        double closest = MIN_POSITION;
        System.out.println("Go right");
        for (int i = 0; i < 63; i++) {
            System.out.println(i + " : " + toString());
            update(1);

            if (isGameOver())
                break;

        }

        System.out.println("is Game Over ? " + isGameOver() + " currentPos" + getPosition());
    }

    public int randomAverage(){
        Random r = new Random();
        reset();
        int maksCounter=1_000_000;
        while (!isGameOver()){
            int action = r.nextInt(3)-1;
            update(action);

            if(getStepCounter()>maksCounter){
                System.out.println("fail");
                break;
            }

        }
        return getStepCounter();

    }

    public void findShortestPath() {

        int steps = 0;
        while (true) {
            System.out.println(steps + " : " + toString());
            update(-1);
            steps++;
            if (velocity >= 0.0)
                break;
        }


        double closest = MIN_POSITION;
        System.out.println("\tGo right");
        int stepsRight = 0;

        while (true) {
            System.out.println(stepsRight + " : " + toString());
            update(1);
            stepsRight++;

            if (position > closest)
                closest = position;

            if (closest > position)
                break;
            if (isGameOver())
                break;
        }

        System.out.println("is Game Over ? " + isGameOver() + " best pos:" + closest+" total steps: "+(steps+stepsRight)+" or" +getStepCounter());
    }
}
