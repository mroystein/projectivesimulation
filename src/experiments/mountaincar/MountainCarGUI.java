package experiments.mountaincar;

import net.miginfocom.swing.MigLayout;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Oystein on 15.09.14.
 */
public class MountainCarGUI extends JFrame implements KeyListener, FocusListener {

    MountainCarWorld mountainCarWorld;
    private  GamePaneMC gamePaneMC;

    private BufferedImage carImg;
    private JLabel jLabel_steps;
    private final MCAgent mcAgent;
    private JLabel jLabel_games;
    private JTextField jText_trainTime;

    public MountainCarGUI() {
        mountainCarWorld = new MountainCarWorld();
        mcAgent = new MCAgent(mountainCarWorld,true,20);

        try {
            File f = new File("src/images/auto.png");
            carImg = ImageIO.read(f);
            System.out.println("success");
        } catch (IOException e) {
            e.printStackTrace();
        }

        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLocationByPlatform(true);
        gamePaneMC = new GamePaneMC(mountainCarWorld);
        gamePaneMC.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                System.out.println("Game gained Focus");
            }

            @Override
            public void focusLost(FocusEvent e) {

            }
        });
        add(getTopPanel(), BorderLayout.WEST);
        add(gamePaneMC, BorderLayout.CENTER);
   // add(new DrawSine(), BorderLayout.CENTER);
        pack();
        setMinimumSize(new Dimension(600, 500));
        setVisible(true);
        setFocusable(true);

        addKeyListener(this);
        addFocusListener(this);
        requestFocus();


    }



    private JPanel getTopPanel() {
        JPanel p = new JPanel(new MigLayout());
        JButton b_reset = new JButton("Reset");
        b_reset.addActionListener(e -> {
            mountainCarWorld.reset();
            gamePaneMC.repaint();
            System.out.println("Reset");
        });


        jLabel_steps = new JLabel();
        jLabel_games = new JLabel();

        JButton b_startAI = new JButton("Run");
        b_startAI.addActionListener(e ->{
            startLoop();
            updateJPanels();

        });

        JButton b_stopAI = new JButton("Stop");
        b_stopAI.addActionListener(e ->{
            timer.stop();
            gamePaneMC.repaint();
        });

        JButton b_stop = new JButton("Do action");
        b_stop.addActionListener(e ->{

            //doActionAgent(true);

        });

        jText_trainTime = new JTextField(6);


        JButton b_train = new JButton("Train");
        b_train.addActionListener(e ->{

            try {
                int trainTime = Integer.parseInt(jText_trainTime.getText());

                mcAgent.train(trainTime);
            }catch (Exception exepction){
                System.out.println("Not a number"+e);
            }
            System.out.println("Done training");
            getFocusBack();


        });
        p.add(b_reset, "wrap");
        p.add(b_startAI);
        p.add(b_stopAI,"wrap");
        p.add(b_stop,"wrap");
        p.add(jLabel_steps,"wrap");
        p.add(jLabel_games,"wrap");

        p.add(jText_trainTime,"wrap");
        p.add(b_train,"wrap");

        updateJPanels(); // to set text etc
        return p;
    }


    public void updateJPanels(){
        jLabel_steps.setText("Steps : "+mountainCarWorld.getStepCounter());
        jLabel_games.setText("Games : "+mountainCarWorld.getGamesCounter());
    }


    public void getFocusBack(){
        requestFocus();
    }

    @Override
    public void focusGained(FocusEvent e) {
        System.out.println("gained focus");
    }

    @Override
    public void focusLost(FocusEvent e) {
        System.out.println("lost focus");
        //requestFocus();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }



    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_F:
                requestFocus();
                break;
            case KeyEvent.VK_LEFT:

                mountainCarWorld.update(-1);
                break;
            case KeyEvent.VK_RIGHT:
                mountainCarWorld.update(1);
                break;
            case KeyEvent.VK_DOWN:
                mountainCarWorld.update(0);
                break;

            case KeyEvent.VK_A:
                startLoop();
                break;
            case KeyEvent.VK_R:
                mountainCarWorld.reset();
                break;

            default:
                System.out.println("Unknown key.");

        }
        updateJPanels();
        //System.out.println(e.getKeyCode() + " pressed");
        gamePaneMC.repaint();

    }

    private static final long serialVersionUID = 0L;
    private static final int TIMER_DELAY = 10;
    Timer timer;

    public void startLoop(){
         timer = new Timer(TIMER_DELAY, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameLoop();
            }
        });
       timer.start();
    }

    private void gameLoop() {

        if(mountainCarWorld.isGameOver()){
            //System.out.println("Stopped");
            //timer.stop();
            mountainCarWorld.reset();
            //return;

        }

        updateJPanels();
        mcAgent.getAction();
        gamePaneMC.repaint();

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }



    public class GamePaneMC extends JPanel {


        private MountainCarWorld mountainCarWorld;

        public GamePaneMC(MountainCarWorld mountainCarWorld) {
            this.mountainCarWorld = mountainCarWorld;


            setPreferredSize(new Dimension(400, 300));
            setBorder(new LineBorder(Color.CYAN, 2));


        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            Graphics2D g2 = (Graphics2D) g;



            int xPos2 = cordinateTrans(mountainCarWorld.getPosition());


//            System.out.println("GUI pos: "+xPos2);
//            g2.fillRect(xPos2, heightTrans(mountainCarWorld.getPosition()), 20, 20);

            int carPosWidth = cordinateTrans(mountainCarWorld.getPosition())-25;
            int carPosHeight = heightTrans(mountainCarWorld.getPosition())-25;

            //g2.drawImage(carImg,xPos2,heightTrans(mountainCarWorld.getPosition())-50,50,50,null);
            //g2.drawImage(carImg,carPosWidth,carPosHeight,50,50,null);


            int carWidth = 50,carHeight=50;

            int fixedX=cordinateTrans(mountainCarWorld.getPosition());
            int fixedY = heightTrans(mountainCarWorld.getPosition())-10;
            AffineTransform at = new AffineTransform();
            double theta = -mountainCarWorld.getSlope(mountainCarWorld.getPosition());
            at.translate(fixedX,fixedY);
            at.rotate(theta);
            at.scale(0.5,0.5);
            at.translate(-carImg.getWidth()/2,-carImg.getHeight()/2);


            g2.drawImage(carImg,at,null);
            //mountainCarWorld.
            //System.out.println("Slope : "+mountainCarWorld.getSlope(mountainCarWorld.getPosition()));


            String vel = String.format("%.2f", mountainCarWorld.getVelocity());
            g2.setColor(Color.BLACK);

            g2.drawString(vel,xPos2+5,heightTrans(mountainCarWorld.getPosition()));


            //g2.drawLine(cordinateTrans(MountainCarWorld.MIN_POSITION),200,cordinateTrans(MountainCarWorld.MAX_POSITION),200);


            String debugStr = String.format("Position: Real %.2f -> %d",mountainCarWorld.getPosition(),xPos2);
            g2.drawString(debugStr,10,300);

            //Draw the curve
            double len = 0.10;
            for (double i = MountainCarWorld.MIN_POSITION; i < MountainCarWorld.MAX_POSITION; i+=len) {
                int x1=cordinateTrans(i);
                int x2=cordinateTrans(i+len);
                int y1 = heightTrans(i);
                int y2 = heightTrans(i+len);
                g2.drawLine(x1,y1,x2,y2);
            }

            g2.drawString("-1.2",15,220);
            g2.drawString("0.6",340,220);
            g2.drawString("Goal",cordinateTrans(MountainCarWorld.MAX_POSITION),heightTrans(MountainCarWorld.MAX_POSITION));


        }

        private int heightTrans(double pos){
           double h= mountainCarWorld.getHeightAtPosition(pos);

            double newH =h*100+120;
//            System.out.println(String.format("x=%.2f : h=%.2f, newH=%.2f",pos,h,newH));
            return (int)newH;
        }

        private int cordinateTrans(double pos){
            double x = (pos+2.2)*200-180;
            return (int) x;
        }


    }


    public static void main(String[] args) {
        EventQueue.invokeLater(MountainCarGUI::new);
    }

}
