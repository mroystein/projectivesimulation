package experiments;

/**
 * Created by Oystein on 15/04/15.
 */
public interface IWorld {

    public int getState();

    public int doAction(int action);

    /**
     * Returns true if won the game.
     */
    public boolean hasWon();


    public boolean hasGameEnded();
    /**
     * Returns the value for the last reward received from
     * calling the method getNextState( int action ).
     *
     * @return
     */
    double getReward();


    int getNumberOfStates();

    public int getNumberOfActions();

    /**
     * Resets the current state to the start position and returns that state.
     */
    void reset();



    /**
     * The steps. Most game will have it..
     * @return
     */
    public int getStepCounter();
}


