package experiments;

import experiments.GridWorldGame.GridWorld;
import experiments.GridWorldGame.GridWorldGen;
import experiments.InvasionGame.InvasionGame;
import experiments.InvasionGame.InvasionGamePow;
import experiments.cartPole.CartPoleWorld;
import experiments.mountaincar.MountainCarWorldGen;
import gui.SimpleSwingGraphGUI;
import net.miginfocom.layout.Grid;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import ps.v2.PS;
import ps.v2.PS_MatrixSimple;
import ps.v2.PS_Novel;
import reinforcment.RLearner;
import utils.MyUtils;
import utils.Result;


import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Oystein on 15/04/15.
 */
public class MainComparison {


    public MainComparison() {

//        train(l,c,100);
//        trainGenericAgentsOnWorld(c);
//        bruteforce(c);
//        bestCartPole();
//        bestGridWorld();
//
//        bruteForceParametersEXPERIMENTALPS();
//        loadStoredXYSeriesFiles();
//     2   bruteforceRLonAWorld(c);
//        bruteForceParametersPS();
//        bruteForceParametersRL();
//        liveTraining();

//        bestMountainCar();
//        mountainCar();
//        InvasionGame();

//         bruteForceParametersWithoutGUIMultipleVariablesRL();
        bruteForceParametersWithoutGUIMultipleVariablesPS();

    }

    public void liveTraining() {

        CartPoleWorld world = new CartPoleWorld();
        ArrayList<ILearningAlgorithm> list = new ArrayList<>();

        list.add(new RLearner(world, 0.25, 1.0, 1.0, 0.99, RLearner.Q_LEARNING, RLearner.E_GREEDY));
        list.add(new RLearner(world, 0.25, 1.0, 1.0, 0.99, RLearner.Q_LAMBDA, RLearner.E_GREEDY));

//        EventQueue.invokeLater(() ->  new SimpleSwingGraphGUI(list,1000,world.toString() + " avg of ", "Epoch", "Steps", 0, 500));
    }


    public void bestMountainCar() {
        IWorld world = new MountainCarWorldGen(20,20);
        ArrayList<ILearningAlgorithm> list = new ArrayList<>();

//        list.add(new PS(world, 0.0, -1, 1, 0.02, PS.SOFTMAX));
//        list.add(new PS(world, 0.0, -1, 1, 0.0204, PS.SOFTMAX));
//        list.add(new PS(world, 0.0, -1, 1, 0.0234, PS.SOFTMAX));
//        list.add(new PS(world, 0.0, -1, 1, 0.0211, PS.SOFTMAX));
        list.add(new PS(world, 0.0, -1, 1, 0.0194, PS.SOFTMAX));
        list.add(new PS(world, 0.001, 10000, 10, 0.0194, PS.PS_SIMPLE));
        list.add(new PS(world, 0.0001, 10000, 1, 0.0194, PS.SOFTMAX));

        list.add(new RLearner(world, 0.0533, 0.5333, 0.0067, 0.90, RLearner.SARSA, RLearner.E_GREEDY));
        RLearner lambda = new RLearner(world, .10, 1.0, 0.0051, 0.9, RLearner.Q_LAMBDA, RLearner.E_GREEDY);
        lambda.lambda=.7;
        list.add(lambda);

//        RLearner lambda2 = new RLearner(world, .10, 1.0, 0.0051, 0.9, RLearner.Q_LAMBDA, RLearner.E_GREEDY);
//        lambda2.lambda=.8;
//        list.add(lambda2);
//
//        RLearner lambda3 = new RLearner(world, .10, 1.0, 0.0051, 0.9, RLearner.Q_LAMBDA, RLearner.E_GREEDY);
//        lambda3.lambda=.9;
//        list.add(lambda3);
        int numAgents = 10;
        int numGames = 800;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            XYSeries series = trainInterval(learner, numAgents, numGames, false);

            collection.addSeries(series);
//            MyUtils.storeXYSeries(series,learner, numAgents);
//            System.out.println(learner);
            System.out.println();
        }


        sortXYCollection(collection);

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 1000));

    }
    public void bestGridWorld() {
//        IWorld world=null ;//
//        world = new GridWorldGen();
//        world = GridWorldGen.parseFile(GridWorldGen.BOARD_20x20);
        IWorld world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
        double alpha = 0.12;
        double gamma = 0.9530;
        double epsilon = 0.02;

        ArrayList<ILearningAlgorithm> list = new ArrayList<>();
        //PS(IWorld world, double damping, double rewardSuccess, int maxReflection, double glow, int actionSelection)
//        list.add(new PS(world, 0.0, -1, 1, 0.035, PS.SOFTMAX));
        list.add(new PS(world, 0.005, -1, 1, 0.010, PS.SOFTMAX));
        list.add(new PS(world, 0.001, -1, 1, 0.05, PS.SOFTMAX));

//        PS psE = new PS(world, 0.001, -1, 1, 0.05, PS.SOFTMAX);
//        psE.EXPERIMENTAL=true;
//        psE.gammaFuture=1;
//        psE.learningRate=0.006;
//        list.add(psE);
//
//        PS psE2 =  new PS(world,0.001,-1,0.05);
//        psE2.EXPERIMENTAL=true;
//        psE2.gammaFuture=1;
//        psE2.learningRate=0.001;
//        list.add(psE2);

//        PS psE3 =  new PS(world,0.0,-1,0.05);
//        psE3.EXPERIMENTAL=true;
//        psE3.gammaFuture=1;
//        psE3.learningRate=0.009;
//        list.add(psE3);

//        list.add(new PS(world, 0.001, 1000, 1, 0.010, PS.SOFTMAX));
//        list.add(new PS(world, 0.01, 1000, 1, 0.010, PS.SOFTMAX));
//        list.add(new PS(world, 0.0, -1, 1, 0.001, PS.SOFTMAX));
//        list.add(new PS(world));
        list.add(new RLearner(world, 0.25, 1.0, 0.5, 0.995, RLearner.Q_LEARNING, RLearner.E_GREEDY));

        int numAgents = 50;
        int numGames = 2000;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            XYSeries series = trainInterval(learner, numAgents, numGames, false);

            collection.addSeries(series);
//            MyUtils.storeXYSeries(series,learner, numAgents);
//            System.out.println(learner);
            System.out.println();
        }


        sortXYCollection(collection);

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 100));

    }

    public void bestCartPole() {
        CartPoleWorld world = new CartPoleWorld();
        double alpha = 0.12;
        double gamma = 0.9530;
        double epsilon = 0.02;

        ArrayList<ILearningAlgorithm> list = new ArrayList<>();

        list.add(new PS(world, 0.0, -1, 1, 0.579, PS.SOFTMAX));
        list.add(new PS(world, 0.0, -1, 1, .3939, PS.SOFTMAX));
        list.add(new PS(world, 0.0, -1, 1, .4444, PS.SOFTMAX));

        list.add(new RLearner(world, 0.25, 1.0, 0.5, 0.995, RLearner.Q_LEARNING, RLearner.E_GREEDY));
        list.add(new RLearner(world, 0.25, 1.0, 0.001, 0.999, RLearner.Q_LEARNING, RLearner.E_GREEDY));
        list.add(new RLearner(world, 0.1199, 0.999, 0.001, 0.9, RLearner.Q_LEARNING, RLearner.E_GREEDY));

        int numAgents = 10;
        int numGames = 10000;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            XYSeries series = trainInterval(learner, numAgents, numGames, true);

            collection.addSeries(series);
//            MyUtils.storeXYSeries(series,learner, numAgents);
//            System.out.println(learner);
            System.out.println();
        }


        sortXYCollection(collection);

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 500));

        for (int i = 0; i < collection.getSeriesCount(); i++) {
            XYSeries series = collection.getSeries(i);
            askToSave(series, list.get(i), numAgents);
        }
    }

    public void bruteforcePSonAWorld(IWorld world) {
        ArrayList<ILearningAlgorithm> list = new ArrayList<>();

        for (double glow = 0.0; glow <= 1.0; glow += 0.1) {
            for (double gamma = 0.0; gamma < 0.1; gamma += 0.01) {
                list.add(new PS_MatrixSimple(world, gamma, -1, 1, glow));
            }

        }
        list.add(new RLearner(world, 0.12, 0.953, 0.001, 1.0, RLearner.SARSA, RLearner.E_GREEDY));

//        list.add(new PS_MatrixSimple(world, 0.0, -1, 1, 0.02));
//        list.add(new PS_MatrixSimple(world, 0.0, -1, 1, 0.01));
//        list.add(new PS_MatrixSimple(world, 0.005, -1, 1, 0.02));
////        list.add(new PS_MatrixSimple(world, 0.001, 1, 1, 0.02));


        int numAgents = 100;
        int numGames = 5000;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            collection.addSeries(trainInterval(learner, numAgents, numGames, false));
            System.out.println(learner);
            System.out.println();
        }

        sortXYCollection(collection);

        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 500));


    }

    public void loadStoredXYSeriesFiles() {
        int[] fileNums = {0, 1};

        XYSeriesCollection collection = new XYSeriesCollection();
        for (int fileNum : fileNums) {
            XYSeries series = MyUtils.getXYSeries(fileNum);


            collection.addSeries(series);
        }
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, "Graph", "Epoch", "Steps", 0, 500));

    }

    public void bruteforceRLonAWorld(IWorld world) {
        ArrayList<ILearningAlgorithm> list = new ArrayList<>();


        int numPoints = 10;

        for (double alpha : MyUtils.generateRange(0.01, 0.50, numPoints)) {
            for (double gamma : MyUtils.generateRange(0.85, 0.99, numPoints)) {
                list.add(new RLearner(world, alpha, gamma, 0.001, 1.0, RLearner.SARSA, RLearner.E_GREEDY));
            }

        }
        int numAgents = 100;
        int numGames = 800;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            collection.addSeries(trainInterval(learner, numAgents, numGames, false));
            System.out.println(learner);
            System.out.println();
        }

        sortXYCollection(collection);


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 500));


    }

    public void trainGenericAgentsOnWorld(IWorld world) {
        double alpha = 0.12;
        double gamma = 0.9530;
        double epsilon = 0.02;

        ArrayList<ILearningAlgorithm> list = new ArrayList<>();
//        list.add(new RLearner(world, alpha, damping, epsilon, 0.95, RLearner.Q_LEARNING, RLearner.E_GREEDY));
//        list.add(new RLearner(world, alpha, damping, 0.0001, 1.0, RLearner.SARSA, RLearner.E_GREEDY));
        list.add(new RLearner(world, alpha, gamma, 0.001, 1.0, RLearner.SARSA, RLearner.E_GREEDY));
        list.add(new RLearner(world, alpha, gamma, 1.0, 0.99, RLearner.SARSA, RLearner.E_GREEDY));
//        list.add(new RLearner(world, alpha, damping, 1.0, 0.999, RLearner.SARSA, RLearner.E_GREEDY));
        list.add(new RLearner(world, alpha, gamma, 0.0001, 1.0, RLearner.Q_LEARNING, RLearner.E_GREEDY));

//        list.add(new RLearner(world, alpha, damping, epsilon, 0.95, RLearner.SARSA, RLearner.E_GREEDY));

//        list.add(new RLearner(world, alpha, damping, epsilon, 0.99, RLearner.SARSA, RLearner.SOFTMAX));
//        list.add(new RLearner(world, alpha, damping, 0.9, 0.99, RLearner.SARSA, RLearner.E_GREEDY));


        list.add(new PS_MatrixSimple(world, 0.0, -1, 1, 0.5));
        list.add(new PS_MatrixSimple(world, 0.0, -1, 1, 0.02));
        list.add(new PS_MatrixSimple(world, 0.0, -1, 1, 0.01));
        list.add(new PS_MatrixSimple(world, 0.005, -1, 1, 0.02));
//        list.add(new PS_MatrixSimple(world, 0.001, 1, 1, 0.02));


        int numAgents = 100;
        int numGames = 400;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            collection.addSeries(trainInterval(learner, numAgents, numGames, false));
            System.out.println(learner);
            System.out.println();
        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + " avg of " + numAgents + " agents", "Epoch", "Steps", 0, 500));
    }


    public static void sortXYCollection(XYSeriesCollection col) {

        List<Result> resultList = new ArrayList<>();
        for (int i = 0; i < col.getSeriesCount(); i++) {
            XYSeries s = col.getSeries(i);

            double avg = 0.0;
            for (int j = 0; j < s.getItemCount(); j++) {
                avg += s.getY(j).doubleValue();
            }
            avg /= (double) s.getItemCount();
            resultList.add(new Result(s.getKey().toString(), avg));

        }
        Collections.sort(resultList);

        resultList.forEach(System.out::println);
    }


    public void InvasionGame() {
        int numAgents = 1000;
        int numGames = 2000;
        int reverseAt = 1000;
        IWorld world;
//        world= new InvasionGame(36,reverseAt,numGames,100);
        world = new InvasionGamePow(256, numGames);
//        world= new InvasionGame(1024,reverseAt,numGames);
        System.out.println(world.getNumberOfStates());


        double alpha = 0.0;
        double gamma = 0.9530;
        double epsilon = 0.02;

        double damping = 0.0, glow = 1.0;

        ArrayList<ILearningAlgorithm> list = new ArrayList<>();

        double g1 = 1 / 50;//0.02
        double g2 = 1 / 10;//0.1
        double g3 = 1 / 5;//0.2

        int maxRef = 100;
        double damp=0.01;

        list.add(new PS(world, damp, -1, 1, glow, PS.E_GREEDY));
//        list.add(new PS(world, 0.00, -1, maxRef, glow, PS.E_GREEDY));

        list.add(new PS(world, damp, -1, 1, glow, PS.SOFTMAX));
//        list.add(new PS(world, 0.00, -1, maxRef, glow, PS.SOFTMAX));

        list.add(new PS(world, damp, -1, 1, glow, PS.PS_SIMPLE));
//        list.add(new PS(world, 0.00, -1, maxRef, glow, PS.PS_SIMPLE));

//        PS psGreedy = new PS(world, 0.2, -1, 1, glow, PS.E_GREEDY);
//        psGreedy.epsilon=0.1;
//        list.add(psGreedy);


//        list.add(new PS(world, 0.01, -1, 1, glow, PS.PS_SIMPLE));
//        list.add(new PS(world, 0.02, -1, 1, glow, PS.PS_SIMPLE));
//        list.add(new PS(world, 0.1, -1, 1, glow, PS.PS_SIMPLE));
//        list.add(new PS(world, 0.2, -1, 1, glow, PS.PS_SIMPLE));


        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            collection.addSeries(trainInterval(learner, numAgents, numGames, false));
            System.out.println(learner);
            System.out.println();
        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, world.toString() + ". Avg of " + numAgents, "Epoch", "Blocking Efficiency", 0.49, 1.01));
    }

    public void mountainCar() {
        IWorld world = new MountainCarWorldGen(10, 10);
//        IWorld world = new InvasionGame(2);
        System.out.println(world.getNumberOfStates());


        double alpha = 0.12;
        double gamma = 0.9530;
        double epsilon = 0.02;

        ArrayList<ILearningAlgorithm> list = new ArrayList<>();
        list.add(new RLearner(world, alpha, 0.9, epsilon, 0.95, RLearner.Q_LEARNING, RLearner.E_GREEDY));
//        list.add(new RLearner(world, 0.2267, 0.4, 0.0034, 0.9111, RLearner.SARSA, RLearner.E_GREEDY));
//        list.add(new RLearner(world, 0.0533, 0.5333, 0.0067, 0.90, RLearner.SARSA, RLearner.E_GREEDY));
////        list.add(new RLearner(world, alpha, damping, epsilon, 0.95, RLearner.SARSA, RLearner.E_GREEDY));
////        list.add(new RLearner(world, alpha, damping, epsilon, 0.99, RLearner.SARSA, RLearner.E_GREEDY));
//        list.add(new RLearner(world, alpha, damping, 0.9, 0.99, RLearner.SARSA, RLearner.E_GREEDY));
//        list.add(new PS_MatrixSimple(world, 0.001, 1, 1, 0.02));
//
////        list.add(new PS(world,0.0,-1,1,0.5,PS.SOFTMAX));
        list.add(new PS(world,0.001,1,1,0.02,PS.SOFTMAX));
        list.add(new PS(world, 0.001, -1, 1, 0.02, PS.SOFTMAX));
        list.add(new PS(world,0.001,1,10,0.02,PS.SOFTMAX));


        int numAgents = 10;
        int numGames = 1000;

        XYSeriesCollection collection = new XYSeriesCollection();
        for (ILearningAlgorithm learner : list) {
            System.out.println(learner);
            collection.addSeries(trainInterval(learner, numAgents, numGames, false));
            System.out.println(learner);
            System.out.println();
        }


        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, "MC world", "Epoch", "Steps", 0, 600));
    }

    public double[] trainInternal(ILearningAlgorithm learner, int numGames, boolean print) {
        int maxStepsForceQ=100_000;
        if(learner.getWorld() instanceof MountainCarWorldGen){
            maxStepsForceQ=MountainCarWorldGen.MAX_STEPS_BEFORE_QUIT;
        }

        double[] steps = new double[numGames];
        if (print)
            System.out.println();
        for (int i = 0; i < numGames; i++) {

            learner.runOneEpoch();
            steps[i] = learner.getWorld().getStepCounter();

            if(steps[i]>=maxStepsForceQ){
                System.err.println("Agent takes too much time. Filling in with fails and quitting");
                for (int j = i+1; j < numGames; j++) {
                    steps[j]=maxStepsForceQ;
                }
                return steps;
            }

//            if (i > 2000) {
//                boolean allSuccess = true;
//                for (int j = i - 2000; j <= i; j++) {
//                    if (steps[j] < CartPoleWorld.MAKS_STEPS)
//                        allSuccess = false;
//                }
//                if (allSuccess) {
//                    System.err.println("");
//                    System.err.println("Broke since all 100 last was success, filling " + i + " to " + steps.length + " with MAX " + CartPoleWorld.MAKS_STEPS);
//                    System.err.println("");
//                    for (int j = i + 1; j < steps.length; j++) {
//                        steps[j] = CartPoleWorld.MAKS_STEPS;
//                    }
//                    break;
//                }
//
//            }
            if (print && i % (numGames / 10) == 0) {
//                System.out.printf("-");
                if (learner instanceof RLearner) {
                    RLearner l = (RLearner) learner;

                    System.out.printf("%d (%d e=%.5f) ", i, learner.getWorld().getStepCounter(), l.getEpsilon());
                } else {
                    System.out.printf("%d (s=%d) ", i, learner.getWorld().getStepCounter());
                }

            }

        }
        if (print)
            System.out.println();
        //debug -----
        double avg = 0;
        for (double step : steps) {
            avg += step;
        }
        avg /= (double) numGames;
        if (print)
            System.out.println(avg + " :" + learner.toString());

        //debug end ------


        return steps;

    }

    public XYSeries trainInterval(ILearningAlgorithm learner, int numAgents, int numGames, boolean showIndividualGraph) {
        long startTime = System.currentTimeMillis();
        XYSeriesCollection c = new XYSeriesCollection();
        double[] avg = new double[numGames];
        for (int i = 0; i < numAgents; i++) {
            learner.reset();
            double[] steps = trainInternal(learner, numGames, false);
            for (int j = 0; j < avg.length; j++) {
                avg[j] += steps[j];
            }
            //debug
            if (c.getSeriesCount() < 200) {
                XYSeries s = new XYSeries(i);
                for (int j = 0; j < avg.length; j++) {
                    s.add(j, steps[j]);
                }
                c.addSeries(s);
            }
            //debug end --

        }
        //debug --
        if (showIndividualGraph)
            EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(c, learner.toString(), "Epoch", "Steps", 0, 1000));
        //debug end --

        XYSeries series = new XYSeries(learner.toString());
        for (int j = 0; j < avg.length; j++) {
            avg[j] /= (double) numAgents;
            series.add(j, avg[j]);
        }

        long elapsed = System.currentTimeMillis() - startTime;
        System.out.printf("%s | steps=%.1f | Time used:%.2f s \n", learner, MyUtils.averageLastN(series, 10), ((double) elapsed / (double) 1000));
//        System.out.println("Time used: " + (double) elapsed / (double) 1000 + "s   average steps in the end: " + MyUtils.averageLastN(series, 20)+" "+learner);

        return series;
    }


    public void train(ILearningAlgorithm learner, IWorld world, int numGames) {

        for (int i = 0; i < numGames; i++) {


            while (!world.hasWon()) {
                if (world.getStepCounter() > 10_00_000) break;

                int state = world.getState();
//                System.out.println(state);
                int action = learner.getAction(state);
                world.doAction(action);
                learner.update(world.getReward(), state);
//                System.out.println(world.getReward());

//                if(world.getStepCounter()%100==0)
//                System.out.println("\t"+world.toString()+" | "+world.getState()+" "+action +" | "+world.getStepCounter());
            }
            System.out.println("Finished in " + world.getStepCounter() + " steps");
            world.reset();

        }

    }

    public void trainSpecialForInvasionGame(ILearningAlgorithm learner, IWorld world, int numGames) {

        double avg = 0.0;
        for (int i = 0; i < numGames; i++) {


            while (!world.hasWon()) {
                if (world.getStepCounter() > 10_00_000) break;

                int state = world.getState();
                int action = learner.getAction(state);
                world.doAction(action);
                System.out.println(String.format("State: %d, Did action %d , R=%f", state, action, world.getReward()));
                learner.update(world.getReward(), world.getState());
//                System.out.println(world.getReward());
                avg += world.getReward();

            }
//            System.out.println("Finished in "+world.getStepCounter()+" steps");
            world.reset();

        }
        avg /= (double) numGames;
        System.out.println(avg);
    }

    public static void main(String[] args) {
        new MainComparison();
        System.out.println("Program is finished!");
    }


    public XYSeries makeGraphFromParamters(ILearningAlgorithm learner, IWorld world, String para, double[] values, int epochs, int numAgents) {
        System.out.println("Bruteforce epochs=" + epochs + ", avg of " + numAgents + " agents. On parameter " + para + " values=" + Arrays.toString(values));

        XYSeriesCollection collection = new XYSeriesCollection();
        XYSeries series = new XYSeries(para);
        List<Result> resultList = new ArrayList<>();
        for (double v : values) {//0.015 ish

            learner.setParameter(para, v);
            XYSeries xySeries = trainInterval(learner, numAgents, epochs, false);
            double steps = MyUtils.averageLastN(xySeries, 10);
//            steps = xySeries.getY(xySeries.getItemCount()-1).doubleValue();
            Result result = new Result(learner.toString(), steps);
            System.out.println(result);
            resultList.add(result);
//            results.add(result);
            series.add(v, steps);

        }
        Collections.sort(resultList);
//        resultList.sort(Result.smallStepFirst);

        System.out.println("-- Sorted ----");
        for (int i = 0; i < 10; i++) {
            System.out.println(resultList.get(i));
        }
        collection.addSeries(series);
        String header = String.format("Mountain Car. avg of %d agents. Epochs=%d , %s", numAgents, epochs, learner.toString());

        //EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Epoch", "Steps", 0, 250));


        return series;
    }


    public void bruteForceParametersRL() {
        int epochs = 600;
        int numAgents = 50;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 100;

        IWorld world ;//
        // = new CartPoleWorld();
        world = new MountainCarWorldGen(10,10);

        ILearningAlgorithm learner;
        //= new RLearner(world, 0.1, 1.0, 0.001, 0.999, RLearner.Q_LEARNING, RLearner.E_GREEDY);
//        XYSeries alphaSeries = makeGraphFromParamters(learner, world, "alpha", MyUtils.generateRange(0.0, 1.0, numPoints), epochs, numAgents);

//        collection.addSeries(alphaSeries);
        //Gamma - 0- only consider current rewards, 1=strive for long term reward.
        learner = new RLearner(world, 0.2822, 0.9900, 0.001, 1.00, RLearner.Q_LAMBDA, RLearner.E_GREEDY);
//        learner = new RLearner(world, 0.25, 1.0, 0.5, 0.995, RLearner.Q_LEARNING, RLearner.E_GREEDY);
//        XYSeries gammaSeries = makeGraphFromParamters(learner, world, "alpha", MyUtils.generateRange(0.0, 1.0, numPoints), epochs, numAgents);
//        collection.addSeries(gammaSeries);
//        collection.addSeries(makeGraphFromParamters(learner,world,"epsilon", MyUtils.generateRange(0.0, 0.1, numPoints),epochs,numAgents));

//        MyUtils.storeXYSeries(gammaSeries, learner, numAgents);
        collection.addSeries(MyUtils.getXYSeries(1));

        String header = String.format("%s avg of %d agents. Epochs=%d", world, numAgents, epochs);
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Parameter value", "Steps", 0, 250));


    }

    public void bruteForceParametersPS() {
        int epochs = 100;
        int numAgents = 1000;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 30;
        IWorld world;
//        IWorld world = new CartPoleWorld();
//         world = new MountainCarWorldGen(10,10);
//        world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
        world = GridWorldGen.parseFile(GridWorldGen.BOARD_6x9);

//        PS learner = new PS(world, 0.0, 1, 0.105);
        PS learner  = new PS(world,0.0,1.0,1,0.0,ILearningAlgorithm.SOFTMAX);


//        double[] pointsAll = MyUtils.addArraysAndSort(MyUtils.generateRange(0,1,numPoints),MyUtils.generateRange(0.001,0.25,numPoints));
        XYSeries damperglowSeries = makeGraphFromParamters(learner, world, "glow",  MyUtils.generateRange(0.0001, 0.5, numPoints), epochs, numAgents);
        collection.addSeries(damperglowSeries);
//        learner = new PS_MatrixSimple(world, 0.0, -1, 1, 0.01);
//        collection.addSeries(makeGraphFromParamters(learner, world, "damping", MyUtils.generateRange(0.0, 0.2, numPoints), epochs, numAgents));




//        collection.addSeries( MyUtils.getXYSeries(8));
        String header = String.format("%s avg of %d agents. Epochs=%d", world, numAgents, epochs);
        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Parameter value", "Steps", 0, 100));
//


        for (int i = 0; i < collection.getSeriesCount(); i++) {
            askToSave(collection.getSeries(i), learner, numAgents);
        }

    }



    public void bruteForceParametersWithoutGUIMultipleVariablesRL() {
        int epochs = 100;
        int numAgents = 2;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 20;
        IWorld world;
//        world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
        world= new MountainCarWorldGen(20,20);
//        world = new CartPoleWorld();
//        world=new GridWorldGen();

//      RLearner learner = new RLearner(world,0.1,1.0,0.001,1.0,RLearner.Q_LAMBDA,RLearner.E_GREEDY);
//        learner.lambda=.3;

        RLearner learner = new RLearner(world,0.1,1.0,0.001,.9,RLearner.Q_LEARNING,RLearner.E_GREEDY);
//        learner.EXPERIMENTAL=true;
        List<Result> resultList = new ArrayList<>();
//        double[] range = MyUtils.generateRange(0.0, 0.99, numPoints);
//        System.out.println(Arrays.toString(range));


        double[][] plot = new double[numPoints*numPoints][3];
        StringBuilder plotStr = new StringBuilder();
        plotStr.append("# damp  glow    steps");
        int c=0;
        for (double alpha : MyUtils.generateRange(0.0, 1.0, numPoints)) {
            plotStr.append("\n");
            for (double gamma : MyUtils.generateRange(0.0, 1.0, numPoints)) {
//                for (double epsilon: MyUtils.generateRange(0.0001, 0.01, numPoints) ) {
//                    for (double epsilonMulti: MyUtils.generateRange(0.9,1.0, numPoints) ) {

                        learner.setParameter("alpha", alpha);
                        learner.setParameter("gamma", gamma);
//                        learner.setParameter("epsilon", epsilon);
//                        learner.setParameter("epsilonMulti", epsilonMulti);

                        XYSeries series = trainInterval(learner, numAgents, epochs, false);
                        //Result r = new Result()
                        double avgSteps = MyUtils.averageLastN(series, 10);
//                        plot[c][0] = alpha;
//                        plot[c][1] = gamma;
//                        plot[c][2] = avgSteps;

                        plotStr.append(String.format("%.4f  %.4f    %.4f\n", alpha, gamma, avgSteps));
                        c++;
                        resultList.add(new Result(learner.toString(), MyUtils.averageLastN(series, 10)));
//                    }
//                }
            }
        }
        System.out.println("Best results");
//        Collections.sort(resultList);
        Collections.sort(resultList,Result.smallStepFirst);
//        Collections.sort(resultList,Result.largestStepFirst);
        int nOutput=resultList.size()<50?resultList.size():50;
        for (int i = 0; i < nOutput; i++) {
            System.out.println(resultList.get(i));
        }


        //print plot
//        System.out.println("# damp  glow    steps");
//        for (int i = 0; i < plot.length; i++) {
//            System.out.println(String.format("%.4f  %.4f    %.4f",plot[i][0],plot[i][1],plot[i][2]));
//        }

        System.out.println(plotStr);

//        MyUtils.writeToFile(plotStr,"plot2");
    }
    public void bruteForceParametersWithoutGUIMultipleVariablesPS() {
        int epochs = 400;
        int numAgents = 10;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 100;
        IWorld world;
//        world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
        world= new MountainCarWorldGen(20,20);
//        world = new CartPoleWorld();
//        world=new GridWorldGen();

//
        PS learner = new PS(world, 0.0, 1000, 1, 0.579, PS.SOFTMAX);
//        ILearningAlgorithm learner = new PS_MatrixSimple(world,0.0,-1,1,0.4);
//        learner.EXPERIMENTAL=true;
        List<Result> resultList = new ArrayList<>();
//        double[] range = MyUtils.generateRange(0.0, 0.99, numPoints);
//        System.out.println(Arrays.toString(range));


        double[][] plot = new double[numPoints*numPoints][3];
        StringBuilder plotStr = new StringBuilder();
        plotStr.append("# damp  glow    steps");
        int c=0;
        for (double damping : MyUtils.generateRange(0.0, 0.0, 1)) {
            plotStr.append("\n");
            for (double glow : MyUtils.generateRange(0.001, 1.0, numPoints)) {


                learner.setParameter("damping", damping);
                learner.setParameter("glow", glow);

                XYSeries series = trainInterval(learner, numAgents, epochs, false);
                //Result r = new Result()
                double avgSteps = MyUtils.averageLastN(series,10);
                plot[c][0]=damping;
                plot[c][1]=glow;
                plot[c][2]=avgSteps;

                plotStr.append(String.format("%s %s %s\n",fmt(damping),fmt(glow),fmt(avgSteps)));
                c++;
                resultList.add(new Result(learner.toString(), MyUtils.averageLastN(series, 10)));


            }
        }
        System.out.println("Best results");
        Collections.sort(resultList,Result.smallStepFirst);

        int nOutput=resultList.size()<10?resultList.size():10;
        for (int i = 0; i < nOutput; i++) {
            System.out.println(resultList.get(i));
        }


        //print plot
//        System.out.println("# damp  glow    steps");
//        for (int i = 0; i < plot.length; i++) {
//            System.out.println(String.format("%.4f  %.4f    %.4f",plot[i][0],plot[i][1],plot[i][2]));
//        }

        System.out.println(plotStr);

//        MyUtils.writeToFile(plotStr,"plot2");
    }
    public static String fmt(double d)
    {
//        if(d == (long) d)
//            return String.format("%d",(long)d);
//        else
            return String.format("%s",d);
    }
    public void bruteForceParametersEXPERIMENTALPS() {
        int epochs = 500;
        int numAgents = 2;

        XYSeriesCollection collection = new XYSeriesCollection();
        int numPoints = 15;
        IWorld world;
//        IWorld world = new CartPoleWorld();
//         world = new MountainCarWorldGen(10,10);
//        world = GridWorldGen.parseFile(GridWorldGen.BOARD_20x20);
        world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
//        world=new GridWorldGen();

        PS learner = new PS(world, 0.01, -1, 0.1);
        learner.EXPERIMENTAL = true;
        List<Result> resultList = new ArrayList<>();
//        double[] range = MyUtils.generateRange(0.0, 0.99, numPoints);
//        System.out.println(Arrays.toString(range));




        for (double damping : MyUtils.generateRange(0, 0.2, 5)) {
            for (double glow : MyUtils.generateRange(0, 0.2, 5)) {
                for (double gammaFutureLoop : MyUtils.generateRange(0.0, 0.99, numPoints)) {
                    for (double learningRateLoop : MyUtils.generateRange(0.0, 0.15, numPoints)) {
                        learner.gammaFuture = gammaFutureLoop;
                        learner.learningRate = learningRateLoop;
                        learner.setParameter("damping", damping);
                        learner.setParameter("glow", glow);

                        XYSeries series = trainInterval(learner, numAgents, epochs, false);
                        //Result r = new Result()

                        resultList.add(new Result(learner.toString(), MyUtils.averageLastN(series, 10)));
                    }

                }
            }
        }
        System.out.println("Best results");
        Collections.sort(resultList);
        for (int i = 0; i < 100; i++) {
            System.out.println(resultList.get(i));
        }


//
//        collection.addSeries(makeGraphFromParamters(learner, world, "gammaFuture", MyUtils.generateRange(0.0, 1.0, numPoints), epochs, numAgents));
////        learner.learningRate
//        learner =  new PS(world,0.0,-1,0.014);
//        learner.EXPERIMENTAL=true;
//        learner.gammaFuture=0.9;
//        learner.learningRate=0.25;
//        collection.addSeries(makeGraphFromParamters(learner, world, "learningRate", MyUtils.generateRange(0.0, 1.0, numPoints), epochs, numAgents));
//
////        collection.addSeries( MyUtils.getXYSeries(8));
//
//
//        String header = String.format("%s avg of %d agents. Epochs=%d , %s", world, numAgents, epochs,learner);
//        EventQueue.invokeLater(() -> new SimpleSwingGraphGUI(collection, header, "Parameter value", "Steps", 0, 250));
//
//
////
//
//        for (int i = 0; i < collection.getSeriesCount(); i++) {
//            askToSave(collection.getSeries(i), learner, numAgents);
//        }


    }


    public void askToSave(XYSeries series, ILearningAlgorithm learner, int numAgents) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Press 1 to save, else 0");
        if (sc.nextInt() == 1) {
            MyUtils.storeXYSeries(series, learner, numAgents);
            System.out.println("Saved.");
        } else {
            System.out.println("Not saved.");
        }
    }
}
