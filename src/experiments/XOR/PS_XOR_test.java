package experiments.XOR;

import ps.v1.PSR_Matrix;
import ps.v1.PS_Interface;
import ps.v1.PS_Matrix;
import ps.v1.ProjectionSimulationGen;
import ps.v2.PS;
import utils.entities.PairB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Oystein on 04.09.14.
 */
public class PS_XOR_test implements PS_Interface<PairB, Boolean> {


    public void alphabetTest() {

        ArrayList<Character> perceptions = new ArrayList<>();
        ArrayList<Character> actions = new ArrayList<>();
        for (char i = 'a'; i <= 'z'; i++) {

            perceptions.add(i);
        }

        for (char i = 'A'; i <= 'Z'; i++) {

            actions.add(i);
        }

        for (int i = 0; i < perceptions.size(); i++) {
            System.out.println(perceptions.get(i) + " = " + actions.get(i));
        }


        PSR_Matrix<Character, Character> psr = new PSR_Matrix<>(perceptions, actions, 0.0001, 1.0, 1, 1.0);


        for (int j = 0; j < 50; j++) {

            double successes = 0;
            for (int i = 0; i < perceptions.size(); i++) {
                Character action = psr.getAction(perceptions.get(i));

                boolean success = action.toString().toUpperCase().equals(perceptions.get(i).toString().toUpperCase());
                psr.giveReward(success);


                if (success)
                    successes++;
            }


            System.out.println("Round " + j + " : successes=" + successes + " -> " + successes / 26.0);
        }

    }


    public void newSimple(){
        Random r = new Random();
        int[] percepts = {0,1,2,3};
        int[] actions= {0,1};
        double damping=0.0, glow=1.0;
                int reflection=1;
        PS ps = new PS(4,2,0.0,1.0,1,1.0);

        for (int steps = 0; steps < 50; steps++) {
            int percept = percepts[r.nextInt(4)];
            int action = ps.getAction(percept);

        }
    }

    public PS_XOR_test(int lol) {
        Random r = new Random(System.currentTimeMillis());

//        List<String> percepts = Arrays.asList("00", "01", "10", "11");
        List<PairB> percepts = Arrays.asList(new PairB(false, false), new PairB(false, true), new PairB(true, false), new PairB(true, true));
        List<Boolean> actions = Arrays.asList(true, false);

        PS_Matrix<PairB, Boolean> ps = new PS_Matrix<>(percepts, actions, 0, 1, 1, 1);

        for (int epochs = 0; epochs < 20; epochs++) {

            PairB percept = percepts.get(epochs % percepts.size());
            boolean action = ps.getAction(percept);

            boolean isCorrect = (percept.a ^ percept.b) == action;
            ps.giveReward(isCorrect);

        }

        for (PairB percept : percepts) {
            boolean action = ps.getAction(percept);
//            boolean isCorrect = (percept.a ^ percept.b) == action;
//            String isCorrectStr = (percept.a ^ percept.b) == action ? "Correct": "Wrong";
            System.out.println(percept + " = " + action + " (" + ((percept.a ^ percept.b) == action ? "Correct" : "Wrong") + ")");
        }

//        System.out.println("Max success rate: " + ps.getSeries().getMaxY());
    }


    public PS_XOR_test() {

        alphabetTest();
        ArrayList<PairB> percepts = new ArrayList<>();
        percepts.add(new PairB(false, false));
        percepts.add(new PairB(false, true));
        percepts.add(new PairB(true, false));
        percepts.add(new PairB(true, true));

        ArrayList<Boolean> actions = new ArrayList<>();
        actions.add(true);
        actions.add(false);

        PS_Matrix<PairB, Boolean> ps = new PS_Matrix<>(percepts, actions, 0.0, 1.0, 1, 1);

        for (int i = 0; i < 2; i++) {

            for (PairB p : percepts) {

                boolean selectedAction = ps.getAction(p);

                boolean correct = logicalXOR(p.a, p.b) == selectedAction;
                ps.giveReward(correct);


            }
        }


        for (PairB p : percepts) {
            boolean selectedAction = ps.getAction(p);
            System.out.println(p + " = " + selectedAction);

            //System.out.println(p + " = " + selectedAction + "  (isCorrect?=" + isCorrectActionGen(p, selectedAction) + ")");
        }
        // System.out.println("Max success rate: "+ps.getSeries().getMaxY());


        //System.out.println("With Strings :");
        //new XOR_test_String();

    }


    public void old() {

        ArrayList<PairB> percepts = new ArrayList<>();
        percepts.add(new PairB(false, false));
        percepts.add(new PairB(false, true));
        percepts.add(new PairB(true, false));
        percepts.add(new PairB(true, true));

        ArrayList<Boolean> actions = new ArrayList<>();
        actions.add(true);
        actions.add(false);
        ProjectionSimulationGen<PairB, Boolean> ps = new ProjectionSimulationGen<>(this, percepts, actions, 0.02, 1.0, 2);


        ps.train(20);

        for (PairB p : percepts) {
            boolean selectedAction = ps.getActionSelfReward(p);
            System.out.println(p + " = " + selectedAction);

            //System.out.println(p + " = " + selectedAction + "  (isCorrect?=" + isCorrectActionGen(p, selectedAction) + ")");
        }
        System.out.println("Max success rate: " + ps.getSeries().getMaxY());


        System.out.println("With Strings :");
        //new XOR_test_String();
    }

    @Override
    public boolean isCorrectActionGen(PairB percept, Boolean action) {
        return logicalXOR(percept.a, percept.b) == action;

    }

    private boolean logicalXOR(boolean a, boolean b) {
        return a ^ b;
    }

    private boolean logicalOR(boolean a, boolean b) {
        return a | b;
    }

    public static void main(String[] args) {

        new PS_XOR_test(123);

    }


    public class XOR_test_String implements PS_Interface<String, Boolean> {

        public XOR_test_String() {
            ArrayList<String> percepts = new ArrayList<>();
            percepts.add("00");
            percepts.add("01");
            percepts.add("10");
            percepts.add("11");

            ArrayList<Boolean> actions = new ArrayList<>();
            actions.add(true);
            actions.add(false);

            ProjectionSimulationGen<String, Boolean> ps = new ProjectionSimulationGen<>(this, percepts, actions, 0.02, 1.0, 2);

            ps.train(100);

            for (String p : percepts) {
                boolean selectedAction = ps.getActionSelfReward(p);
                System.out.println(p + " = " + selectedAction);

                //System.out.println(p + " = " + selectedAction + "  (isCorrect?=" + isCorrectActionGen(p, selectedAction) + ")");
            }

            System.out.println("Max success rate: " + ps.getSeries().getMaxY());

        }

        @Override
        public boolean isCorrectActionGen(String percept, Boolean action) {
            if (percept.equals("00") || percept.equals("11")) {
                return !action;
            } else {
                return action;
            }

        }
    }


}
