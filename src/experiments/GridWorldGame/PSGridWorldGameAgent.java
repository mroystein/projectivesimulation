package experiments.GridWorldGame;

import ps.v1.*;
import org.jfree.data.xy.XYSeries;
import utils.MyUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oystein on 09.09.14.
 */
public class PSGridWorldGameAgent implements PS_Interface<Position, String> {


    private XYSeries series;
    private GridWorld gridWorld;
    //    private  ProjectionSimulationGen<Position, String> ps;
    InterfacePS<Position, String> ps;


    public PSGridWorldGameAgent(GridWorld gridWorld, double gamma, double rewardSuccess, double damperGlow) {
        this.gridWorld = gridWorld;
        ps = new ProjectionSimulationGen<>(this, getPercepts(gridWorld), getActions(), gamma, rewardSuccess, 1, damperGlow);

        series = new XYSeries(toString());
    }

    public PSGridWorldGameAgent(GridWorld gridWorld, double gamma, double rewardSuccess, double damperGlow,int reflection, boolean matrix,boolean useSoftmax) {
        this.gridWorld = gridWorld;

        if (matrix) {
            if(reflection>1)
                ps = new PSR_Matrix<>(getPercepts(gridWorld), getActions(), gamma, rewardSuccess, reflection, damperGlow);
            else
                ps = new PS_Matrix<>(getPercepts(gridWorld), getActions(), gamma, rewardSuccess, 1, damperGlow);

        }
        else
            ps = new ProjectionSimulationGen<>(this, getPercepts(gridWorld), getActions(), gamma, rewardSuccess, 1, damperGlow);

        ps.useSoftMax(useSoftmax);
        series = new XYSeries(toString());
    }

    private ArrayList<String> getActions() {
        ArrayList<String> actionsStr = new ArrayList<>();
        actionsStr.add("LEFT");
        actionsStr.add("RIGHT");
        actionsStr.add("UP");
        actionsStr.add("DOWN");
        return actionsStr;
    }

    @Override
    public String toString() {
        return ps.toString();
    }

    private ArrayList<Position> getPercepts(GridWorld gridWorld) {
        ArrayList<Position> percepts = new ArrayList<>();
        for (int i = 0; i < gridWorld.getRows(); i++) {
            for (int j = 0; j < gridWorld.getColumns(); j++) {
                //if(gridWorld.get(i,j)!=GridWorld.WALL)
                percepts.add(new Position(i, j));
                //should ignore walls
            }
        }
        return percepts;
    }

    private double findReward(int steps){
//        double d = 1000-steps;
//        if(d<0){
//            return 1.0;
//        }
//        return d;



        if(steps<14)
            return 100;
        if(steps<15)
            return 50;

        return 1.0;
    }

    public int train() {
        gridWorld.reset();
        while (true) {
            Position agentPos = gridWorld.getAgentPos();

            String action = ps.getAction(agentPos);
            doAction(action);

            if(ps instanceof PSR_Matrix){
                PSR_Matrix<Position, String> psr = (PSR_Matrix<Position, String>) ps;

                if(gridWorld.isGameEnded()) {


                    psr.giveRewardAmount(findReward(gridWorld.getStepCount()));

                    break;
                }
                else psr.giveReward(0);

            }else{
                if(gridWorld.isGameEnded()){
                    ps.giveReward(true);

                    break;
                }else{
                    ps.giveReward(false);
                }
            }


        }


        int stepCount = gridWorld.getStepCount();
        gridWorld.reset();
        return stepCount;
    }


    public void trainGames(int numberOfGames) {
        int minStepCount = Integer.MAX_VALUE;
        ArrayList<Integer> timesArray = new ArrayList<>();

        int currNumGames = 0;
        while (currNumGames < numberOfGames) {

            int stepCount = train();

            if (stepCount < minStepCount)
                minStepCount = stepCount;

            System.out.println("Steps : " + stepCount);
            timesArray.add(stepCount);

            currNumGames++;
        }

        System.out.println("Histogram: " + timesArray);
        System.out.println("Best :" + minStepCount + " steps");


    }

    private void doAction(String action) {
        switch (action) {
            case "LEFT":
                gridWorld.moveLeft();
                break;
            case "RIGHT":
                gridWorld.moveRight();
                break;
            case "UP":
                gridWorld.moveUp();
                break;
            case "DOWN":
                gridWorld.moveDown();
                break;
            default:
                System.out.println("Unkown action?");
                break;
        }

    }


    public void printEdgesDebug() {
//        int totEdges = 0;
//        List<PerceptClip<Position, String>> percClips = ps.getPercClips();
//        for (PerceptClip<Position, String> p : percClips) {
//            ArrayList<EdgeGen<Position, String>> edges = p.getEdges();
//            totEdges += edges.size();
//            for (EdgeGen edgeGen : edges) {
//                //System.out.println("E "+edgeGen+" h="+edgeGen.h);
//            }
//        }

//        System.out.println("percClips : " + percClips.size());
//        System.out.println("Total edges: " + totEdges);
    }

    public void print() {
        String[][] arr = new String[gridWorld.getRows()][gridWorld.getColumns()];
        if(ps==null)
            return;
        if(!(ps instanceof ProjectionSimulationGen))
            return;

        ProjectionSimulationGen<Position, String> psGen = (ProjectionSimulationGen<Position, String>) ps;
        List<PerceptClip<Position, String>> percClips = psGen.getPercClips();
        for (PerceptClip<Position, String> p : percClips) {
            String action = p.getBestAction();
            double h = p.getHopping(action);

            String prop = MyUtils.toStrAsPropability(p.getHoppingProbability(action));
            String hStr = String.format("%.2f", h);
            arr[p.id.y][p.id.x] = action + "(" + prop + ", " + hStr + ")";

        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sb.append(arr[i][j] + "\t\t");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }


    @Override
    public boolean isCorrectActionGen(Position percept, String action) {
        return gridWorld.isWinningMove(action);
    }
}
