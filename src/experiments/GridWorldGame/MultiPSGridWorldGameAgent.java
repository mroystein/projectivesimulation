package experiments.GridWorldGame;

import org.jfree.data.xy.XYSeries;

/**
 * Created by Oystein on 09.09.14.
 */
public class MultiPSGridWorldGameAgent {

    private int gamesCounter=0;
    private  PSGridWorldGameAgent[] agents;
    private  XYSeries series;


    public MultiPSGridWorldGameAgent(int nAgents, GridWorld gridWorld){
        this(nAgents,gridWorld,0.0001,4.0,0.07,1,true,true);
    }

    public MultiPSGridWorldGameAgent(int nAgents, GridWorld gridWorld,double gamma, double rewardSuccess,double damperGlow){
        this(nAgents,gridWorld,gamma,rewardSuccess,damperGlow,1,true,true);
    }
    public MultiPSGridWorldGameAgent(int nAgents, GridWorld gridWorld,double gamma, double rewardSuccess,double damperGlow,int reflection, boolean matrix,boolean useSoftmax){

        agents = new PSGridWorldGameAgent[nAgents];
        for (int i = 0; i < nAgents; i++) {
            agents[i] = new PSGridWorldGameAgent(gridWorld.clone(),gamma,rewardSuccess,damperGlow, reflection,matrix,useSoftmax);

        }
        series = new XYSeries(agents[0].toString());

    }


    public XYSeries getSeries() {
        return series;
    }

    public void trainGames(boolean print){
        double average =0;

        for (PSGridWorldGameAgent agent: agents){
            double steps = agent.train();
            average+=steps;
        }
        average/=agents.length;
        if(print)
            System.out.println("Round: "+gamesCounter+"| Avg: "+average+" | "+agents[0].toString());
        series.add(gamesCounter,average);
        gamesCounter++;
    }


    public static void main(String[] args){
        GridWorld gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/worldPaper6x9.txt");

//        MultiPSGridWorldGameAgent multi = new MultiPSGridWorldGameAgent(10, gridWorld);

        MultiPSGridWorldGameAgent m = new MultiPSGridWorldGameAgent(500,gridWorld,0.0000,1.0,0.07,2,true,true);


        for (int i = 0; i < 100; i++) {
            m.trainGames(true);
        }



    }
}
