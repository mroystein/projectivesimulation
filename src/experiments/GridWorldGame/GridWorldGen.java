package experiments.GridWorldGame;

import experiments.IWorld;
import javafx.geometry.Pos;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Oystein on 03.09.14.
 */
public class GridWorldGen implements IWorld {

    public static final String BOARD_6x9 = "src/experiments/GridWorldGame/worldPaper6x9.txt";
    public static final String BOARD_10x10 = "src/experiments/GridWorldGame/worldPaper10x10.txt";
    public static final String BOARD_20x20 = "src/experiments/GridWorldGame/worldPaper20x20.txt";
    public static final int EMPTY = 0, WALL = 1, EMPTY_USED = 3, AGENT = 2, GOAL = 4;

    private int[][] grid, gridCloneStart;
    //    private Position agentPos;
    private int agentX, agentY;
    private int stepCount = 0;
    private boolean gameEnded = false;
    private int rows, columns;
    private int shortestPath;

    public int MAX_STEPS = 100_000;

    public GridWorldGen() {
        this(new int[][]{{0, 0, 0, 0, 0, 0, 0, GOAL},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, WALL, WALL, WALL, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {AGENT, 0, 0, 0, 0, 0, 0, 0}});


    }

    public GridWorldGen(int[][] grid) {
        this.grid = grid;
        gridCloneStart = deepCopyIntMatrix(grid);
        rows = grid.length;
        columns = grid[0].length;
        init();
        shortestPath = calcShortestPath();
        System.out.println("Shortest path: " + shortestPath);
    }

    private void init() {

        rows = grid.length;
        columns = grid[0].length;

        //Find the agent..
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == AGENT) {
//                    agentPos = new Position(i, j);
                    agentY = i;
                    agentX = j;
                }
            }
        }

    }


    private int toUnqInt(int i, int j) {
        return i * rows + j;
    }

    private int toUnqInt(Position p) {
        return toUnqInt(p.x, p.y);
    }

    private int calcShortestPath() {
        int nNodes = rows * columns;

        Queue<Position> q = new LinkedList<>();
        boolean[] discovered = new boolean[nNodes];
        Position agentPos= new Position(agentY,agentX);
        q.add(agentPos);
        discovered[toUnqInt(agentPos)] = true;
        int[] distSimple = new int[nNodes];
        Arrays.fill(distSimple, Integer.MAX_VALUE);
        distSimple[toUnqInt(agentPos)] = 0;

        while (!q.isEmpty()) {
            Position p = q.poll();

            if (grid[p.y][p.x] == GOAL) {
//                System.out.println("Dist new: " + distSimple[toUnqInt(p)]);
                return distSimple[toUnqInt(p)];
            }

            for (int b = 0; b < 2; b++) {
                for (int i = -1; i <= 1; i += 2) {
                    Position neighbor = new Position(p.y + i, p.x);
                    if (b == 0) neighbor = new Position(p.y, p.x + i);

                    if (!isValidPosition(neighbor)) continue;
                    if (discovered[toUnqInt(neighbor)]) continue;

                    q.add(neighbor);
                    discovered[toUnqInt(neighbor)] = true;
                    distSimple[toUnqInt(neighbor)] = Math.min(distSimple[toUnqInt(neighbor)], distSimple[toUnqInt(p)] + 1);
                }
            }

        }

        return -1;

    }



    @Override
    public int getState() {
        return agentX * grid.length + agentY;
    }

    @Override
    public int doAction(int action) {
        switch (action) {
            case 0:
                moveLeft();
                break;
            case 1:
                moveRight();
                break;
            case 2:
                moveUp();
                break;
            case 3:
                moveDown();
                break;
            default:
                System.err.println("Illegial movement");
        }
        return getState();
    }


    @Override
    public boolean hasWon() {
        return gameEnded;
    }

    @Override
    public boolean hasGameEnded() {
        return gameEnded;
    }

    @Override
    public double getReward() {
        if (stepCount >= MAX_STEPS) {
            System.err.println("Gave -100 reward since it ran out of time in GW getReward()");
            return -100;
        }

        return gameEnded ? 100 : -1;
    }

    @Override
    public int getNumberOfStates() {
        return grid.length * grid[0].length;
    }

    @Override
    public int getNumberOfActions() {
        return 4;
    }

    public void reset() {
        stepCount = 0;
        gameEnded = false;
        grid = deepCopyIntMatrix(gridCloneStart);
        init();

    }

    @Override
    public int getStepCounter() {
        return stepCount;
    }

    public boolean isGameEnded() {
        return gameEnded;
    }

    private boolean isValidPosition(int x, int y) {
        if (x < 0 || y < 0)
            return false;
        if (y >= rows || x >= columns)
            return false;
        if (grid[y][x] == WALL)
            return false;


        return true;
    }


    private boolean isValidPosition(Position newPos) {
        return isValidPosition(newPos.x, newPos.y);
    }


    private void moveTo(int y, int x) {
        if (gameEnded) {
            System.out.println("Game has ended. you must reset it");
            return;
        }
        stepCount++;
        if (!isValidPosition(x, y))
            return;


        boolean won = grid[y][x] == GOAL;

        grid[agentY][agentX] = EMPTY_USED;
        grid[y][x] = AGENT;

        if (won) {
            gameEnded = true;
            return;
        }
        agentX = x;
        agentY = y;

        if (stepCount >= MAX_STEPS) {
            gameEnded = true;
            System.err.println("Ended the game in GW since step=" + stepCount);
        }


    }


    public void moveLeft() {
        moveTo(agentY, agentX - 1);
    }

    public void moveRight() {
        moveTo(agentY, agentX + 1);
    }

    public void moveUp() {
        moveTo(agentY - 1, agentX);
    }

    public void moveDown() {
        moveTo(agentY + 1, agentX);
    }

    public int[][] getGrid() {
        return grid;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int get(int y, int x) {
        return grid[y][x];
    }

    public int getStepCount() {
        return stepCount;
    }

    public void printGrid() {
        StringBuilder sb = new StringBuilder();
        for (int[] aGrid : grid) {
            for (int anAGrid : aGrid) {
                sb.append(anAGrid).append(" ");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

    @Override
    public String toString() {
        return String.format("GridWorld %d*%d (minPath=%d)", grid.length, grid[0].length, shortestPath);
    }

    public static GridWorldGen parseFile(String file) {

        int[][] g = new int[0][0];
        try {
            g = readFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new GridWorldGen(g);
    }

    private static int[][] readFile(String file) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(file));

        int[][] g = new int[sc.nextInt()][sc.nextInt()];
        for (int i = 0; i < g.length; i++) {
            for (int j = 0; j < g[i].length; j++) {
                g[i][j] = sc.nextInt();
            }
        }

        return g;
    }

    @Override
    protected GridWorldGen clone() {
        return new GridWorldGen(deepCopyIntMatrix(gridCloneStart));
    }

    public static int[][] deepCopyIntMatrix(int[][] input) {
        if (input == null)
            return null;
        int[][] result = new int[input.length][];
        for (int r = 0; r < input.length; r++) {
            result[r] = input[r].clone();
        }
        return result;
    }

    public static void play(GridWorldGen w) {

        while (!w.hasGameEnded()) {
            w.printGrid();
            Scanner sc = new Scanner(System.in);
            switch (sc.next()) {
                case "w":
                    w.moveUp();
                    break;
                case "a":
                    w.moveLeft();
                    break;
                case "s":
                    w.moveDown();
                    break;
                case "d":
                    w.moveRight();
                    break;
            }
        }

    }
    public static void test_uniqueStates(GridWorldGen w){
        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < w.rows; i++) {
            for (int j = 0; j < w.columns; j++) {
                Position p = new Position(i, j);
                int unqInt = w.toUnqInt(p);

                if(set.contains(unqInt))
                    System.out.println("Error. Allreayd contains "+unqInt);
                set.add(unqInt);
                System.out.printf("%d | (%d,%d)\n", unqInt, i, j);
            }
        }
    }

    public static void main(String[] args) {
        GridWorldGen w = GridWorldGen.parseFile("src/experiments/GridWorldGame/worldPaper10x10.txt");
//        w= new GridWorldGen();
//        play(w);
        test_uniqueStates(w);

//        int[][] grid1 = new int[20][3];
//        grid1[0][0]=GOAL;
//        grid1[2][1]=AGENT;
//        w = new GridWorldGen(grid1);


    }
}
