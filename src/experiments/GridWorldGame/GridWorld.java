package experiments.GridWorldGame;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Oystein on 03.09.14.
 */
public class GridWorld {

    public static final int EMPTY = 0, WALL = 1, EMPTY_USED = 3, AGENT = 2, GOAL = 4;

    private int[][] grid, gridCloneStart;
    private Position agentPos;
    private int stepCount=0;
    private boolean gameEnded=false;
    private int rows, columns;

    public GridWorld() {

        grid = new int[][]{{0, 0, 0, 0, 0, 0, 0, GOAL},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, WALL, WALL, WALL, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {AGENT, 0, 0, 0, 0, 0, 0, 0}};
        gridCloneStart = deepCopyIntMatrix(grid);
        init();
    }



    private void init() {

        rows = grid.length;
        columns = grid[0].length;

        //Find the experiments..
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == AGENT)
                    agentPos = new Position(i, j);

            }
        }

        //System.out.println("Agent pos: "+agentPos);
        //System.out.println("Init grid:");
        //printGrid();

    }

    public GridWorld(int[][] grid) {
        this.grid = grid;
        gridCloneStart = deepCopyIntMatrix(grid);
        init();

    }

    public void reset() {
        stepCount=0;
        gameEnded=false;
        grid = deepCopyIntMatrix(gridCloneStart);
        init();

    }

    public boolean isGameEnded() {
        return gameEnded;
    }

    private boolean isValidPosition(int x, int y) {
        if (x < 0 || y < 0)
            return false;
        if (x >= columns || y >= rows)
            return false;
        if (grid[y][x] == WALL)
            return false;


        return true;
    }

    public Position getAgentPos() {
        return agentPos;
    }



    private boolean isValidPosition(Position newPos) {
        return isValidPosition(newPos.x, newPos.y);
    }

    private void moveTo(Position newPos) {
        if(gameEnded){
            System.out.println("Game has ended. you must reset it");
            return;
        }
        stepCount++;
        if (!isValidPosition(newPos))
            return;


        boolean won = grid[newPos.y][newPos.x]==GOAL;

        grid[agentPos.y][agentPos.x] = EMPTY_USED;
        grid[newPos.y][newPos.x] = AGENT;

        if(won){
            //System.out.println("WON -> Resetting | in "+stepCount+" steps");
            gameEnded=true;
            return;
        }
        agentPos = newPos;

        //printGrid();


    }


    public boolean isWinningMove(String action){
        Position position;
        switch (action){
            case "LEFT": position=new Position(agentPos.y, agentPos.x - 1);
                break;
            case "RIGHT": position=new Position(agentPos.y, agentPos.x + 1);
                break;
            case "UP":position=new Position(agentPos.y-1, agentPos.x);
                break;
            case "DOWN": position=new Position(agentPos.y+1, agentPos.x);
                break;
            default:
                System.out.println("Unkown action?");
                return false;

        }
        if(!isValidPosition(position))
            return false;
        return get(position.y,position.x)==GOAL;


    }
    public void moveLeft() {
        moveTo(new Position(agentPos.y, agentPos.x - 1));
    }

    public void moveRight() {
        moveTo(new Position(agentPos.y, agentPos.x + 1));
    }

    public void moveUp() {
        moveTo(new Position(agentPos.y - 1, agentPos.x));
    }

    public void moveDown() {
        moveTo(new Position(agentPos.y + 1, agentPos.x));
    }

    public int[][] getGrid() {
        return grid;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int get(int y, int x) {
        return grid[y][x];
    }

    public int getStepCount() {
        return stepCount;
    }

    public void printGrid() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < grid.length; i++) {

            for (int j = 0; j < grid[i].length; j++) {
                sb.append(grid[i][j] + " ");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }


    public static GridWorld parseFile(String file) {

        int[][] g = new int[0][0];
        try {
            g = readFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new GridWorld(g);
    }

    private static int[][] readFile(String file) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(file));

        int[][] g = new int[sc.nextInt()][sc.nextInt()];
        for (int i = 0; i < g.length; i++) {
            for (int j = 0; j < g[i].length; j++) {
                g[i][j] = sc.nextInt();
            }
        }

        return g;
    }

    @Override
    protected GridWorld clone() {
       return new GridWorld(deepCopyIntMatrix(gridCloneStart));
    }

    public static int[][] deepCopyIntMatrix(int[][] input) {
        if (input == null)
            return null;
        int[][] result = new int[input.length][];
        for (int r = 0; r < input.length; r++) {
            result[r] = input[r].clone();
        }
        return result;
    }
}
