package experiments.cartPole;

import experiments.IWorld;

import java.util.Random;
import java.util.function.DoubleSupplier;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Created by Oystein on 18/04/15.
 */

public class CartPoleWorld implements IWorld {
    public static double GRAVITY = 9.8;
    public static double MASSCART = 1.0;
    public static double MASSPOLE = 0.1;
    public static double TOTAL_MASS = (MASSPOLE + MASSCART);
    public static double LENGTH = 0.5;	  /* actually half the pole's length */
    public static double POLEMASS_LENGTH = (MASSPOLE * LENGTH);
    public static double FORCE_MAG = 10.0;
    public static double TAU = 0.02;		  /* seconds between state updates */
    public static double FOURTHIRDS = 1.3333333333333;

   private static double one_degree = 0.0174532;
    private static double six_degrees = 0.1047192;
    private static double twelve_degrees = 0.2094384;
    private static double fifty_degrees = 0.87266;
    public static int MAKS_STEPS=100_000;//1_000_000;
    private double rewardDefault;
    private double rewardDeath;

    public Random r = new Random(System.currentTimeMillis());

    public double x;
    public double xDot;
    public double theta;
    public double thetadot;
    public int stepCount = 0;


    public CartPoleWorld() {
        this(0, 0, 0, 0);
    }

    public CartPoleWorld(double x, double xDot, double theta, double thetadot) {
        this.x = x;
        this.xDot = xDot;
        this.theta = theta;
        this.thetadot = thetadot;
    }

    public CartPoleWorld(double rewardDefault, double rewardDeath) {

        this.rewardDefault = rewardDefault;
        this.rewardDeath = rewardDeath;
    }

    @Override
    public int getState() {
        //returns a number from 0 to 162

        int box;


        if (x < -2.4 ||
                x > 2.4 ||
                theta < -twelve_degrees ||
                theta > twelve_degrees){
//            System.err.println("Failure.. returning 1.");
            return (0); /* to signal failure */
        }

        if (x < -0.8) box = 0;
        else if (x < 0.8) box = 1;
        else box = 2;

        if (xDot < -0.5) ;
        else if (xDot < 0.5) box += 3;
        else box += 6;

        if (theta < -six_degrees) ;
        else if (theta < -one_degree) box += 9;
        else if (theta < 0) box += 18;
        else if (theta < one_degree) box += 27;
        else if (theta < six_degrees) box += 36;
        else box += 45;

        if (thetadot < -fifty_degrees) ;
        else if (thetadot < fifty_degrees) box += 54;
        else box += 108;

        return box;
    }

    @Override
    public int doAction(int action) {
        stepCount++;
        double force = 0.0;
        if (action > 0) force = FORCE_MAG;
        else force = -FORCE_MAG;

        double costheta = Math.cos(theta);
        double sintheta = Math.sin(theta);

        double tmp = (force + POLEMASS_LENGTH * (Math.pow(thetadot, 2)) * sintheta) / TOTAL_MASS;
        //thetaacc = (self.gravity * sintheta - costheta * tmp) / (self.length * (self.fourthirds - self.masspole * costheta ** 2 / self.total_mass))
        double thetaacc = (GRAVITY * sintheta - costheta * tmp) / (LENGTH * (FOURTHIRDS - MASSPOLE * Math.pow(costheta, 2) / TOTAL_MASS));
        //xacc = tmp - self.polemass_length * thetaacc * costheta / self.total_mass
        double xacc = tmp - POLEMASS_LENGTH * thetaacc * costheta / TOTAL_MASS;


        double px, pxdot, ptheta, pthetadot;
        px = x;
        pxdot = xDot;
        ptheta = theta;
        pthetadot = thetadot;

        x += TAU * pxdot;
        xDot += TAU * xacc;
        theta += TAU * pthetadot;
        thetadot += TAU * thetaacc;

        return getState();
    }

    @Override
    public boolean hasWon() {
        return failure();
    }

    @Override
    public boolean hasGameEnded() {
        return failure() || stepCount>MAKS_STEPS;
    }


    private double cheatReward(){
        double absTheta = Math.abs(theta);


        if (theta < -six_degrees) return -10;
        else if (theta < -one_degree)return 1;
        else if (theta < 0) return 2;
        else if (theta < one_degree) return 1;
        else if (theta < six_degrees) return 0;
        else return -10;
    }
    @Override
    public double getReward() {
//        if(stepCount>=MAKS_STEPS) return 1;
//        if(failure())
//            return -1000;
//        //or 1 for every step balanced
////        if (failure()) return -100;
//        return 1.0;//1.0;

//        return v1();
        return v2();
//        return  v3();


//        if(failure()) return rewardDeath;
//        return rewardDefault;


//
//        return cheatReward();
    }

    private double v1(){
        if(stepCount>=MAKS_STEPS) return 1;
        if(failure())
            return -1000;
        return 1.0;//1.0;
    }
    private double v2(){ //9751. seems to be the best
        if(stepCount>=MAKS_STEPS) return 100;
        if(failure())
            return -10_000;
        return 1.0;//1.0;
    }
    private double v3(){ //shit
//        if(stepCount>=MAKS_STEPS) return 100;
        if(failure())
            return -100;
        return 1.0;//1.0;
    }

    @Override
    public String toString() {
        if(rewardDefault<=0)
        return String.format("Cart Pole");

        return String.format("Cart Pole reward: default=%.4f , death=%.4f",rewardDefault,rewardDeath);
    }

    @Override
    public int getNumberOfStates() {
        return 162;//or 162?
    }

    @Override
    public int getNumberOfActions() {
        return 2;
    }

    public void reset() {
        this.x = 0;
        this.xDot = 0;
        this.theta = 0;
        this.thetadot = 0;
        stepCount = 0;
    }

    @Override
    public int getStepCounter() {
        return stepCount;
    }

    public boolean failure() {
        double twelve_deg = 0.2094384;
        if (x < -2.4 || x > 2.4) return true;
        if (theta < -twelve_deg || theta > twelve_deg) return true;

        return false;
    }

    public int randomPolicy() {
        return r.nextInt(2);
    }

    public void single_episode() {
        reset();

//        int action = randomPolicy();
        while (!failure()) {
            doAction(randomPolicy());
        }
    }








    public static void main(String[] args) {
        CartPoleWorld w = new CartPoleWorld();
        w.single_episode();
        System.out.println(w.stepCount);
        w.single_episode();
        System.out.println(w.stepCount);

        randomAgent(1_000_000);
    }

    private static void randomAgent(int numTries){
        CartPoleWorld w = new CartPoleWorld();
        double avg=0;


        for (int i = 0; i < numTries; i++) {
            w.single_episode();
            avg+=w.getStepCounter();
        }
        avg/=(double) numTries;
        System.out.println(avg);

    }

}
