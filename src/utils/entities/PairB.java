package utils.entities;

public class PairB {
    public boolean a,b;

    public PairB(boolean a, boolean b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PairB pair = (PairB) o;


        return a==pair.a && b==pair.b;
    }

    @Override
    public String toString() {
        return a+","+b;
    }
}