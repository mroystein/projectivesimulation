package utils.entities;

import experiments.GridWorldGame.Position;
import ps.v1.ActionClip;
import ps.v1.PS_Interface;
import ps.v1.PerceptClip;
import ps.v1.ProjectionSimulationGen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by Oystein on 02.09.14.
 */
public class SimpleMainMethodDebug implements PS_Interface<Position,String> {


    public void overFlowSoftmax(){

    }

    public static double logSumOfExponentials(double[] xs) {
        if (xs.length == 1) return xs[0];
        double max = maximum(xs);
        double sum = 0.0;
        for (int i = 0; i < xs.length; ++i)
            if (xs[i] != Double.NEGATIVE_INFINITY)
                sum += Math.exp(xs[i] - max);
        return max + Math.log(sum);
    }

    private double softMax(double[] arr,double a){
        return Math.exp((a-logSumOfExponentials(arr)));
    }

    private static double maximum(double[] xs) {
        double currMax = 0;
        for (int i = 0; i < xs.length; i++) {
            currMax = Double.MIN_VALUE;
            if (xs[i] > currMax) {
                currMax = xs[i];
            }
        }
        return currMax;
    }
    public static final double MIN_VELOCITY = -0.07, MAX_VELOCITY = 0.07;
    public static final double MIN_POSITION = -1.2, MAX_POSITION = 0.5;
    public SimpleMainMethodDebug() {


        Random r = new Random(System.currentTimeMillis());

        double sX = MIN_POSITION + (MAX_POSITION-MIN_POSITION) * r.nextDouble();
        double sV = MIN_VELOCITY+(MAX_VELOCITY-MIN_VELOCITY)*r.nextDouble();

        System.out.println("Pos : "+sX);
        System.out.println("Velo: "+sV);
//        double[] a ={1.0,2.0,3.0};
//
//        double logSumExp = logSumOfExponentials(a);
//
//        System.out.println(logSumExp);
//
//
//        System.out.println(softMax(a,1.0));

    }

    private void psTest(){

        ArrayList<String> actions = new ArrayList<>();
        actions.add("left");
        actions.add("right");

        ArrayList<Position> percepts = new ArrayList<>();
        percepts.add(new Position(0,-1));
        percepts.add(new Position(0,1));

        ProjectionSimulationGen<Position,String> ps = new ProjectionSimulationGen<>(this,percepts,actions,0.02,1.0,2);
        ps.train(20);




        for (int i = 0; i < 10; i++) {
            String chosen =  ps.getActionSelfReward(percepts.get(0));
            System.out.println(chosen);
        }

    }


    private void lol(){
        PerceptClip<Position,Position> pc1 = new PerceptClip<>(new Position(0,0),1,1);

        ActionClip<Position> ac1 = new ActionClip<>(new Position(-1,0));
        pc1.addNeighbour(ac1);

        System.out.println(pc1.getBestAction());
    }

    private void propabilityTesting(){
        Random r = new Random(System.currentTimeMillis());


        //double[] props = {0.25, 0.25, 0.25, 0.25};
        double[] props = {0.20, 0.20, 0.20, 0.40};
        int[] counter = new int[props.length];
        double n = 50000;
        for (int j = 0; j < n; j++) {
            r = new Random(System.currentTimeMillis());
            double p = r.nextDouble();
            double cumulativeProbability = 0.0;

            for (int i = 0; i < props.length; i++) {
                cumulativeProbability += props[i];
                if (p <= cumulativeProbability) {
                    counter[i]++;
                    break;
                }
            }
        }

        for (int i = 0; i < counter.length; i++) {
            double dd = (counter[i] * 1.0) / n;
            System.out.println(dd);
        }
        String s = Arrays.toString(counter);
        System.out.println(s);
    }


    public static void main(String[] args) {

        new SimpleMainMethodDebug();
    }


    @Override
    public boolean isCorrectActionGen(Position percept, String action) {
        if(!action.equals("left"))
            return true;
        return false;
    }
}
