package utils;

import java.util.Comparator;

/**
 * Created by Oystein on 27/11/14.
 */
public class Result implements Comparable<Result>{
    String name;
    double steps;

    public Result(String name, double steps) {
        this.name = name;
        this.steps = steps;
    }

    @Override
    public int compareTo(Result o) {
        if(steps<o.steps)return -1;
        if(steps>o.steps)return 1;
        return 0;
    }


   public static Comparator<Result> smallStepFirst = new Comparator<Result>() {
        @Override
        public int compare(Result o1, Result o2) {
            if(o1.steps<o2.steps)return -1;
            if(o1.steps>o2.steps)return 1;
            return 0;
        }
    };

    public static Comparator<Result> largestStepFirst = new Comparator<Result>() {
        @Override
        public int compare(Result o1, Result o2) {
            if(o1.steps<o2.steps)return 1;
            if(o1.steps>o2.steps)return -1;
            return 0;
        }
    };

    @Override
    public String toString() {
        return "R{" +
                " " + name + '\'' +
                ", steps=" + String.format("%.2f",steps) +
                '}';
    }
}