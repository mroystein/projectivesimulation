package gui;

import ps.v1.ActionClip;
import ps.v1.ClipGen;
import ps.v1.EdgeGen;
import utils.MyUtils;
import ps.v1.PerceptClip;
import org.jgraph.JGraph;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultListenableGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedMultigraph;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GraphPanel {
    private static final long serialVersionUID = 3256444702936019250L;
    public static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    //public static final Dimension DEFAULT_SIZE = new Dimension(530, 320);

    public static final Color COLOR_PERC_NODE = Color.decode("#218777");
    public static final Color COLOR_ACTION_NODE = Color.decode("#F53357");
    private  int WIDHT,HEIGHT ;

    private final List<PerceptClip> percClips;
    private final List<ActionClip> actionClips;


    //
    private JGraphModelAdapter<ClipGen, MyWeightedEdge> jgAdapter;
    private JGraph jgraph;

    public GraphPanel(List<PerceptClip> percClips, List<ActionClip> actionClips,int WIDHT,int HEIGHT) {
        this.WIDHT=WIDHT;
        this.HEIGHT=HEIGHT;
        this.percClips = percClips;
        this.actionClips = actionClips;
        generateGraph();
    }



    private void generateGraph() {
        //new ListenableDirectedMultigraph<>(MyWeightedEdge.class);
        // create a JGraphT graph
        //ListenableDirectedMultigraph<Clip,MyWeightedEdge> ldm= new ListenableDirectedMultigraph<>(MyWeightedEdge.class);
        //ListenableGraph<Clip,MyWeightedEdge> g = ldm;

        /*ListenableGraph<ClipGen, MyWeightedEdge> g =
                new ListenableDirectedMultigraph<>(
                        MyWeightedEdge.class);*/

        ListenableGraph<ClipGen, MyWeightedEdge> g =
                new ListenableDirectedMultigraph<>(
                        MyWeightedEdge.class);

        // create a visualization using JGraph, via an adapter
        jgAdapter = new JGraphModelAdapter<>(g);

        jgraph = new JGraph(jgAdapter);


        adjustDisplaySettings(jgraph);


        for (PerceptClip clip : percClips) {
            g.addVertex(clip);

        }
        for (ActionClip clip : actionClips) {
            g.addVertex(clip);
        }


        for (PerceptClip perceptClip : percClips){

            for (Object o : perceptClip.getEdges()){
                if(o instanceof EdgeGen){
                    EdgeGen e =(EdgeGen) o;
                    MyWeightedEdge myWeightedEdge = new MyWeightedEdge(e);
                    g.addEdge(e.start, e.end, myWeightedEdge);
                }

            }
        }



        if(percClips.size()==2){
            // position vertices nicely within JGraph component
            positionVertexAt(percClips.get(0), 40, 10, COLOR_PERC_NODE);
            positionVertexAt(percClips.get(1), 380, 10, COLOR_PERC_NODE);
            positionVertexAt(actionClips.get(0), 100, 200, COLOR_ACTION_NODE);
            positionVertexAt(actionClips.get(1), 280, 200, COLOR_ACTION_NODE);
        }
        else{


            //Order Perc: LEFT,RIGHT,2LEFT,2RIGHT
            //Order Action: LEFT,RIGHT,2LEFT,2RIGHT

            positionVertexAt(percClips.get(0), 10, 10, COLOR_PERC_NODE);
            positionVertexAt(percClips.get(1), WIDHT-150, 10, COLOR_PERC_NODE);

            positionVertexAt(percClips.get(2), 10, 370, COLOR_PERC_NODE);
            positionVertexAt(percClips.get(3),  WIDHT-150, 370, COLOR_PERC_NODE);


            int middleX = (WIDHT-150)/2;
            if(actionClips.size()>2)
                positionVertexAt(actionClips.get(2), middleX, 240, COLOR_ACTION_NODE);
            positionVertexAt(actionClips.get(0), middleX, 10, COLOR_ACTION_NODE);
            positionVertexAt(actionClips.get(1), middleX, 120, COLOR_ACTION_NODE);
            if(actionClips.size()>2)
            positionVertexAt(actionClips.get(3), middleX, 370, COLOR_ACTION_NODE);



        }





    }


    public void updateEdges(){
        jgraph.refresh();
    }

    public JGraph getGraph() {
        return jgraph;

    }

    private void adjustDisplaySettings(JGraph jg) {
        //jg.setPreferredSize(DEFAULT_SIZE);

        Color c = DEFAULT_BG_COLOR;
        jg.setBackground(c);
    }

    @SuppressWarnings("unchecked") // FIXME hb 28-nov-05: See FIXME below
    private void positionVertexAt(Object vertex, int x, int y,Color color) {
        DefaultGraphCell cell = jgAdapter.getVertexCell(vertex);
        AttributeMap attr = cell.getAttributes();

        Rectangle2D bounds = GraphConstants.getBounds(attr);

        Rectangle2D newBounds =
                new Rectangle2D.Double(
                        x,
                        y,
                        bounds.getWidth(),
                        bounds.getHeight());


        GraphConstants.setBounds(attr, newBounds);
        GraphConstants.setBackground(attr,color);

        // TODO: Clean up generics once JGraph goes generic
        AttributeMap cellAttr = new AttributeMap();
        cellAttr.put(cell, attr);
        //jgAdapter.get
        jgAdapter.edit(cellAttr, null, null, null);
    }


    /**
     * a listenable directed multigraph that allows loops and parallel edges.
     */
    private static class ListenableDirectedMultigraph<V, E>
            extends DefaultListenableGraph<V, E>
            implements DirectedGraph<V, E> {
        private static final long serialVersionUID = 1L;

        ListenableDirectedMultigraph(Class<E> edgeClass) {
            super(new DirectedMultigraph<V,E>(edgeClass));
        }
    }

    class MyWeightedEdge extends DefaultWeightedEdge {
        private EdgeGen e;

        public MyWeightedEdge(EdgeGen e) {
            this.e = e;
        }

        @Override
        protected Object getSource() {
            return e.start;
        }

        @Override
        protected Object getTarget() {
            return e.end;
        }

        @Override
        protected double getWeight() {
            return e.h;
        }

        @Override
        public String toString() {


            return  e.getEmotionStr()+" "+String.format("%.1f", e.h)+ " ("+  MyUtils.toStrAsPropability(e.start.getHoppingProbability(e.end))+")";
        }
    }


}
