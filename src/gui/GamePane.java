package gui;

public class GamePane{} /* <Integer> extends JPanel implements KeyListener, FocusListener, MouseListener {

   public static final int WIDTH = 400, HEIGHT = 150;

    private Attacker attacker;
    private Defender defender;
    private ProjectionSimulationGen<E,Integer> ps;

    public boolean reverse = false;

    private boolean gameEnded = false;

    private MainSwing2 mainSwing;

    public GamePane(MainSwing2 mainSwing, ProjectionSimulationGen<E,Integer> ps) {
        this.mainSwing = mainSwing;
        this.ps = ps;
        attacker = new Attacker();
        defender = new Defender();
        //add(attacker);
        setFocusable(true);
        addKeyListener(this);
        addMouseListener(this);
        addFocusListener(this);


    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public boolean isReverse() {
        return reverse;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(WIDTH, HEIGHT);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();


        g2d.setColor(Color.black);
        g2d.drawRect(0, 0, WIDTH, HEIGHT);


        int centerY = HEIGHT / 2;
        g2d.drawLine(0, centerY, 20, centerY);
        g2d.drawLine(380, centerY, 400, centerY);

        for (int i = 40; i < 360; i += 80) {
            g2d.drawLine(i + 20, centerY, i + 60, centerY);
        }


        attacker.draw(g2d);
        defender.draw(g2d);

        if (gameEnded) {
            String strGameEnd = "Game ended!! (Press enter to restart)";
            int stringLen = (int)
                    g2d.getFontMetrics().getStringBounds(strGameEnd, g2d).getWidth();
            int start = WIDTH / 2 - stringLen / 2;
            g2d.drawString(strGameEnd, start, 20);


        }


        g2d.dispose();
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int selectedAction = -1;
        //double rewardPunish = -0.04;
        //double rewardSuccess = 1;

        switch (e.getKeyCode()) {
            case KeyEvent.VK_ENTER:
                attacker = new Attacker();
                defender = new Defender();
                gameEnded = false;
                break;

            case KeyEvent.VK_DOWN:
                //defender.moveRight();
                break;
            case KeyEvent.VK_UP:
                //defender.moveLeft();
                break;
            case KeyEvent.VK_LEFT:
                actionMove(true);
                break;
            case KeyEvent.VK_RIGHT:
                actionMove(false);
                break;
        }
        repaint();
        mainSwing.updateUI();

    }

    private void actionMove(boolean left) {
        if (gameEnded)
            return;
        int selectedAction;
        if (left) {
            attacker.moveLeft();
            if (isReverse())

                selectedAction = ps.getAction(Const.SYMBOL_RIGHT);
            else
                selectedAction = ps.getAction(Const.SYMBOL_LEFT);

            if (selectedAction == Const.ACTION_RIGHT) {
                defender.moveRight();
                gameEnded();
                ps.giveRewardForLastAction(false);
            } else {
                ps.giveRewardForLastAction(true);
                defender.moveLeft();
            }
        } else {

            attacker.moveRight();
            if (isReverse())
                selectedAction = ps.getAction(Const.SYMBOL_LEFT);
            else
                selectedAction = ps.getAction(Const.SYMBOL_RIGHT);

            //selectedAction = ps.getAction(ProjectionSimulationGen.SYMBOL_RIGHT);
            if (selectedAction == Const.ACTION_RIGHT) {
                defender.moveRight();
                ps.giveRewardForLastAction(true);
            } else {
                defender.moveLeft();
                gameEnded();
                ps.giveRewardForLastAction(false);
            }

        }

    }

    private void gameEnded() {
        System.out.println("Game ended.");
        gameEnded = true;


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void focusGained(FocusEvent e) {
        System.out.println("GP gained focus");
    }

    @Override
    public void focusLost(FocusEvent e) {
        System.out.println("GP lost focus");
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("Mouse pressed");
        requestFocus();

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}*/
