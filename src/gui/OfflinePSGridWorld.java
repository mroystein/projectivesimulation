package gui;

import experiments.GridWorldGame.GridWorld;
import experiments.GridWorldGame.PSGridWorldGameAgent;

/**
 * Created by Oystein on 05.09.14.
 */
public class OfflinePSGridWorld {

    private GridWorld gridWorld;


    public OfflinePSGridWorld() {
        gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/worldPaper6x9.txt");
//        gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/world.txt");
//       gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/world3x2.txt");
        gridWorld.printGrid();
        PSGridWorldGameAgent psGW;

        double gamma = 0.001, reward = 2.0, damperGlow = 0.02;
        psGW = new PSGridWorldGameAgent(gridWorld, gamma, reward, damperGlow,1, false,true);
//        psGW = new PSGridWorldGameAgent(gridWorld, damping, reward, damperGlow, true);
        psGW.trainGames(200);


        System.out.println("DOne");

        psGW.print();
        //psGW.printEdgesDebug();


    }



    public static void main(String[] args) {
        new OfflinePSGridWorld();
    }
}
