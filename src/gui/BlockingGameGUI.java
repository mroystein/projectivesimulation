package gui;

import experiments.InvasionGame.Const;
import ps.v1.PSMulti;
import ps.v1.PS_Interface;
import ps.v1.ProjectionSimulationGen;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BlockingGameGUI extends JFrame implements KeyListener, PS_Interface<Integer, Integer> {


    // ProjectionSimulation ps;
    private JButton b_train;
    private JTextField text_train;
    public static final int WIDTH = 700, HEIGHT = 800;
    private JLabel label_stats;
    private JLabel label_stats2;
    private JLabel label_timeStamp;
    private ChartPanel chartPanel;
    private JLabel label_successrate;
    // private GamePane gamePane;
    private JButton b_reverse;
    private boolean reverse;

    private int timeSteps = 0;

    //private GraphPanel graphPanel;

    List<ProjectionSimulationGen> projectionSimulationList;
    private List<GraphPanel> graphPanelList;
    //private ProjectionSimulationGen<Integer, Integer> psGen;
    private ArrayList<PSMulti> PSMultiList;
    private JButton b_changeColour;

    public BlockingGameGUI() {

        System.out.println("Started");
        projectionSimulationList = new ArrayList<>();

        //ps = genPS(2, ProjectionSimulation.GAMMA_UPPER_TOP, 1, 1);

        //projectionSimulationList.add(ps);

       // psGen = new ProjectionSimulationGen<>(this, Const.getSymbols(), Const.getActions(), ProjectionSimulationGen.GAMMA_UPPER_TOP, 1, 1);
        //ProjectionSimulationGen ps = new ProjectionSimulationGen(this, Const.getSymbols(), Const.getActions(), ProjectionSimulationGen.GAMMA_UPPER_TOP, 1, 2);
        //projectionSimulationList.add(ps);

//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER, 1,1));
//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER, 1,3));
        //  projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER, 1,15));
//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_LOWER, 1,1));
//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER, 1,1));
//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER, 5,1));

//        projectionSimulationList.add(genPS(4, ProjectionSimulation.GAMMA_UPPER_TOP, 1,1));

//        projectionSimulationList.add(genPS(4, 0.001, 1,1));

        //pSmulti = new PSMulti.java(this,ProjectionSimulation.GAMMA_UPPER_TOP,1,1000,4);


        PSMultiList = new ArrayList<>();
        PSMultiList.addAll(demoDataAssociation());

        setTitle("PS : Invasion Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGHT);
        setUpUI();


        genRand();
        genRand();

    }

    private List<PSMulti> demoDataAssociation() {
        return Arrays.asList(
               new PSMulti.PSBuilder<>(this, Const.getSymbols2Colour(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).build()
              );
    }


    private List<PSMulti> demoDataGamma() {
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(0.0).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_LOWER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).build());
    }

    private List<PSMulti> demoDataEmotion() {
        int reflectionTime=5;
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(0.0).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(0.0).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).build());
    }

    private List<PSMulti> demoDataEmotion4() {
        int reflectionTime=5;
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(0.0).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(0.0).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).reflectionTime(reflectionTime).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).build());
    }
    private List<PSMulti> demoDataReward() {
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).reward(1.0).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).reward(5.0).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions()).reward(0.5).build());
    }

    private List<PSMulti> demoDataGamma4() {
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_LOWER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_MIDDLE).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER_TOP).build());
    }

    private List<PSMulti> demoDataGamma4Reflection() {
        return Arrays.asList(
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).reflectionTime(400).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).reflectionTime(4).build(),
                new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(ProjectionSimulationGen.GAMMA_UPPER).reflectionTime(1).build()
              );
    }
    private List<PSMulti> demoDataRandom() {
        List<PSMulti> PSMultiList = new ArrayList<>();
        PSMultiList.add(genPS(2, ProjectionSimulationGen.GAMMA_MIDDLE, 1.0, 15));
        PSMultiList.add(genPS(2, ProjectionSimulationGen.GAMMA_LOWER, 1.0, 15));
        //PSMultiList.add(new PSMulti(this, ProjectionSimulation.GAMMA_UPPER_TOP, 1, 1000, 4, 1));

        PSMulti.PSBuilder<Integer, Integer> builder = new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour());

        PSMultiList.add(builder.reflectionTime(10).build());
        PSMultiList.add(new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour()).gamma(0.001).build());
        //PSMultiList.add(new PSMulti.PSBuilder(this).damping(ProjectionSimulation.GAMMA_UPPER_TOP).nSymbols(2).build());

//        PSMultiList.add(new PSMulti(this, ProjectionSimulation.GAMMA_UPPER_TOP, 1, 1000, 4, 15));
//
//        PSMultiList.add(new PSMulti(this, ProjectionSimulation.GAMMA_UPPER_TOP, 1, 1000, 2, 1));
//
        //PSMultiList.add(new PSMulti.PSBuilder(this).damping(ProjectionSimulation.GAMMA_UPPER_TOP).maxReflectionTime(12).build());
        //projectionSimulationList.add( new ProjectionSimulationGeneric(this, Const.getSymbols(), Const.getActions(),ProjectionSimulationGeneric.GAMMA_UPPER));

        //projectionSimulationList.add( new ProjectionSimulationGeneric(this, symbols, actions,ProjectionSimulationGeneric.GAMMA_UPPER));
        return PSMultiList;
    }




    private void genRand() {
        Random r1 = new Random(System.currentTimeMillis());
        Random r2 = new Random();
        double avg1 = 0.0, avg2 = 0.0, avg3 = 0.0;
        double n = 500000;
        for (int i = 0; i < n; i++) {
            avg1 += Math.random();
            avg2 += r1.nextDouble();
            avg3 += r2.nextDouble();
        }
        avg1 = avg1 / n;
        avg2 = avg2 / n;
        avg3 /= n;
        System.out.println("1:" + avg1);
        System.out.println("2:" + avg2);
        System.out.println("3:" + avg3);
    }


    private PSMulti<Integer, Integer> genPS(int nSymbols, double gamma, double reward, int reflectionTime) {
        PSMulti.PSBuilder<Integer, Integer> builder;

        if (nSymbols == 2) {
            builder = new PSMulti.PSBuilder<>(this, Const.getSymbols(), Const.getActions());
        } else {
            builder = new PSMulti.PSBuilder<>(this, Const.getSymbolsFour(), Const.getActionsFour());
        }
        builder = builder.gamma(gamma).reward(reward).reflectionTime(reflectionTime);


        return new PSMulti<>(builder);
    }

    private void setUpUI() {
        setLayout(new BorderLayout());
        add(getTopPanel(), BorderLayout.PAGE_START);
        //gamePane = new GamePane(this, ps);
        add(getCenterPanel(), BorderLayout.CENTER);
        add(getLeftJPanel(), BorderLayout.LINE_END);
        add(getBottomPanel2(), BorderLayout.PAGE_END);

        setLocationRelativeTo(null);
        setVisible(true);
    }


    public JPanel getCenterPanel() {
        JPanel p = new JPanel(new MigLayout());
        //p.setBorder(BorderFactory.createLineBorder(Color.black));
        p.setBorder(BorderFactory.createTitledBorder("Agents"));
        // BorderFactory.create
        JComboBox<ProjectionSimulationGen> comboBox = new JComboBox<>(projectionSimulationList.toArray(new ProjectionSimulationGen[projectionSimulationList.size()]));

        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                ProjectionSimulationGen selectedItem = (ProjectionSimulationGen) cb.getSelectedItem();
                System.out.println(selectedItem);
            }
        });


        JTextField t_gamma = new JTextField("0.02");
        Integer[] options = {2, 4};
        JComboBox<Integer> combo_symbols = new JComboBox<>(options);

        JButton button_create = new JButton("Create PS experiments");

        p.add(comboBox, "wrap");
        p.add(new JLabel("Gamma:"));
        p.add(t_gamma, "wrap");
        p.add(combo_symbols, "wrap");
        p.add(button_create);


        return p;

    }

    public JPanel getTopPanel() {
        ImageIcon img_left_arrow = new ImageIcon("arrowLeft30.png");
        ImageIcon img_right_arrow = new ImageIcon("arrowRight30.png");

        // MigLayout migLayout = ;
        JPanel p = new JPanel(new MigLayout());
        label_timeStamp = new JLabel("Timesteps: " + timeSteps);
        label_successrate = new JLabel("SuccessRate: ");
        label_stats = new JLabel("?", img_left_arrow, JLabel.CENTER);
        label_stats2 = new JLabel("?", img_right_arrow, JLabel.CENTER);
        //new JLabel()
        p.add(label_timeStamp);
        p.add(label_successrate, "wrap");
        p.add(label_stats, "wrap");
        p.add(label_stats2);

        return p;
    }

    public JPanel getGamePanel() {
        return null;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void updateUI() {
        //label_stats.setText(ps.toStringLeftProp());
        //label_stats2.setText(ps.toStringRightProp());
        label_timeStamp.setText("Timesteps: " + timeSteps);

        label_successrate.setText("?: ");
        updateChart();


        for (GraphPanel gp : graphPanelList) {
            gp.updateEdges();
        }

        //jGraph.refresh();

       /* GraphPanel graphPanel = new GraphPanel();
        jGraph = graphPanel.getGraph(ps.getPercClips(),ps.getActionClips());

        JPanel p = new JPanel();
        p.add(jGraph);
        graphJPanel =p;
        graphJPanel.updateUI();*/


    }


    public JTabbedPane getBottomPanel2() {
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setPreferredSize(new Dimension(WIDTH, 500));
        //

        JTabbedPane tabGraphs = new JTabbedPane();
        graphPanelList = new ArrayList<>();
        for (ProjectionSimulationGen pSim : projectionSimulationList) {

            GraphPanel gp = new GraphPanel(pSim.getPercClips(), pSim.getActionClips(), WIDTH, 500);
            graphPanelList.add(gp);
            tabGraphs.addTab(pSim.toString(), gp.getGraph());
        }

        for (PSMulti psMulti : PSMultiList) {

            GraphPanel gp = new GraphPanel(psMulti.list[0].getPercClips(), psMulti.list[0].getActionClips(), WIDTH, 500);
            graphPanelList.add(gp);
            tabGraphs.addTab(psMulti.list[0].toString(), gp.getGraph());
        }


        JPanel jPanel = new JPanel();
        jPanel.add(new JLabel("Tab2"));


        chartPanel = new ChartPanel(generateChart());
        chartPanel.setInitialDelay(1);
        //chartPanel.setDomainZoomable(true);
        //chartPanel.setMouseZoomable(true);
        //chartPanel.setRangeZoomable(true);
        //chartPanel.setPreferredSize(new Dimension(WIDTH, 200));

        tabbedPane.addTab("Learning Curve", chartPanel);
        tabbedPane.addTab("Graphs", tabGraphs);
        tabbedPane.addTab("Tab2", jPanel);
        return tabbedPane;
    }

    private JFreeChart generateChart() {
        XYSeriesCollection ds = new XYSeriesCollection();
        for (ProjectionSimulationGen pSim : projectionSimulationList) {

            ds.addSeries(pSim.getSeries());
        }

        for (PSMulti pSmulti : PSMultiList) {
            ds.addSeries(pSmulti.getSeries());
        }

        //ds.addSeries(psGen.getSeries());


        JFreeChart chart = ChartFactory.createXYLineChart("Learning curve", "time step", "blocking efficiency", ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        //xyPlot.set
        ValueAxis axis = xyPlot.getRangeAxis();
        //axis.setRange(0.45,1.01);

        return chart;
    }

    public void updateChart() {
        chartPanel.setChart(generateChart());


    }


    public JPanel getLeftJPanel() {

        JPanel p = new JPanel(new MigLayout());
        p.setBorder(BorderFactory.createTitledBorder("Training"));
        //jPanel.setLayout(new BoxLayout(jPanel,BoxLayout.PAGE_AXIS));
        b_train = new JButton("Train");
        b_train.addActionListener(e -> {

            System.out.println(text_train.getText());
            long startTime = System.currentTimeMillis();

            int trainTime = Integer.parseInt(text_train.getText());
            timeSteps += trainTime;
            for (ProjectionSimulationGen pSim : projectionSimulationList) {
                pSim.train(trainTime);

                //double debPSUM = pSim.getPercClips().get(0).pSum/pSim.getPercClips().get(0).pSumNumber;
                //System.out.println("pSum "+debPSUM);
            }

            //psGen.train(trainTime);
            for (PSMulti pSmulti : PSMultiList) {
                pSmulti.train(trainTime);
            }
            long ellasped = System.currentTimeMillis() - startTime;
            System.out.println("Time used " + ellasped + "ms");


            updateUI();


        });

        text_train = new JTextField(5);
        text_train.setText("250");


        JButton b_reset = new JButton("Reset");
        b_reset.addActionListener(e -> {
//            System.out.println("reset");
//            ps = new ProjectionSimulation();
//            setUpUI();
//            updateUI();
        });


        String modeNormal = "Mode: Normal";
        String modeReversed = "Mode: Reverse";
        b_reverse = new JButton(isReverse() ? modeReversed : modeNormal);
        b_reverse.addActionListener(e -> {

            setReverse(!isReverse());
            b_reverse.setText(isReverse() ? modeReversed : modeNormal);


        });


        b_changeColour = new JButton("Change colour");
        b_changeColour.addActionListener(e -> {
                    addSHitDebug();
                }
        );


        //text_train.setMaximumSize(new Dimension(50,50));
        p.add(text_train);

        p.add(b_train, "wrap");
        p.add(b_reset, "wrap");
        p.add(b_reverse);
        p.add(b_changeColour);


        return p;

    }

    private void addSHitDebug() {

        PSMulti psMulti = PSMultiList.get(0);
        for (ProjectionSimulationGen psDebug : psMulti.list) {
            if (psDebug.percepts.size() == 2) {
                psDebug.addNewPercept(Const.SYMBOL_BLUE_LEFT);
                psDebug.addNewPercept(Const.SYMBOL_BLUE_RIGHT);
                psDebug.removePercept(Const.SYMBOL_LEFT);
                psDebug.removePercept(Const.SYMBOL_RIGHT);
            }
        }


    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(BlockingGameGUI::new);
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }


    @Override
    public boolean isCorrectActionGen(Integer percept, Integer action) {

        if (isReverse()) {
            switch (percept) {
                case Const.SYMBOL_LEFT:
                    return Const.ACTION_RIGHT == action;
                case Const.SYMBOL_RIGHT:
                    return Const.ACTION_LEFT == action;

                case Const.SYMBOL_BLUE_LEFT:
                    return Const.ACTION_RIGHT == action;
                case Const.SYMBOL_BLUE_RIGHT:
                    return Const.ACTION_LEFT == action;

                case Const.SYMBOL_RED_LEFT:
                    return Const.ACTION_RIGHT == action;
                case Const.SYMBOL_RED_RIGHT:
                    return Const.ACTION_LEFT == action;


                case Const.SYMBOL_2_LEFT:
                    return Const.ACTION_2_RIGHT == action;
                case Const.SYMBOL_2_RIGHT:
                    return Const.ACTION_2_LEFT == action;
            }

            System.out.println("WTF 1 ");
        }


        switch (percept) {
            case Const.SYMBOL_LEFT:
                return Const.ACTION_LEFT == action;
            case Const.SYMBOL_RIGHT:
                return Const.ACTION_RIGHT == action;

            case Const.SYMBOL_BLUE_LEFT:
                return Const.ACTION_LEFT == action;
            case Const.SYMBOL_BLUE_RIGHT:
                return Const.ACTION_RIGHT == action;

            case Const.SYMBOL_RED_LEFT:
                return Const.ACTION_LEFT == action;
            case Const.SYMBOL_RED_RIGHT:
                return Const.ACTION_RIGHT == action;

            case Const.SYMBOL_2_LEFT:
                return Const.ACTION_2_LEFT == action;
            case Const.SYMBOL_2_RIGHT:
                return Const.ACTION_2_RIGHT == action;

        }
        System.out.println("WTF 2 ");

        return false;

    }
}