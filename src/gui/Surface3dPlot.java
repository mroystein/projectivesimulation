package gui;

import com.panayotis.gnuplot.JavaPlot;

/**
 * Created by Oystein on 07/05/15.
 */
public class Surface3dPlot {
    public static void main(String[] args) {
        JavaPlot p = new JavaPlot();
        p.addPlot("sin(x)");
        p.plot();
    }
}
