package gui;

import experiments.GridWorldGame.GridWorld;
import experiments.GridWorldGame.GridWorldGen;
import experiments.GridWorldGame.Position;
import experiments.IWorld;
import ps.v1.PS_Interface;
import ps.v1.ProjectionSimulationGen;
import net.miginfocom.swing.MigLayout;
import ps.v2.PS;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Created by Oystein on 03.09.14.
 */
public class GridWorldGUI extends JFrame implements KeyListener, FocusListener, PS_Interface<Position,String> {

    Color color_wall = Color.decode("#424342");
    Color color_border = Color.decode("#D9CDB8");
    Color color_empty = Color.decode("#F5F2EB");
    Color color_empty_used = Color.decode("#DCDAD4");
    Color color_agent = Color.decode("#435A66");
    Color color_goal = Color.decode("#EFC94C");

    private JPanel[][] panelGrid;

    GridWorld gridWorld;
    private JLabel jLabel_steps;
    private ProjectionSimulationGen<Position, String> ps;

    public GridWorldGUI() {

        IWorld world = GridWorldGen.parseFile(GridWorldGen.BOARD_10x10);
        gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/worldPaper10x10.txt");

        IWorld w = GridWorldGen.parseFile("src/experiments/GridWorldGame/worldPaper20x20.txt");
        //gridWorld=new GridWorld();
        System.out.println("grid r=" + gridWorld.getRows() + ", c=" + gridWorld.getColumns());


        setLayout(new BorderLayout());
        addKeyListener(this);

        JPanel chessPanel = new JPanel(new GridLayout(gridWorld.getRows(), gridWorld.getColumns()));
        chessPanel.setPreferredSize(new Dimension(400, 400));
        chessPanel.setBorder(new LineBorder(Color.BLACK, 2));


        Color bgColor = new Color(204, 119, 34);
        //JPanel boardConstrain = new JPanel(new GridBagLayout());
//        boardConstrain.setBackground(bgColor);
//        boardConstrain.add(chessPanel);
        chessPanel.setBackground(bgColor);
        //addKeyListener(this);

        addFocusListener(this);


        add(getTopPanel(), BorderLayout.LINE_START);
        add(chessPanel, BorderLayout.CENTER);

        panelGrid = new JPanel[gridWorld.getRows()][gridWorld.getColumns()];

        for (int i = 0; i < panelGrid.length; i++) {
            for (int j = 0; j < panelGrid[i].length; j++) {

                JPanel b = new JPanel();
                b.setBorder(new LineBorder(color_border, 1));
                chessPanel.add(b);
                panelGrid[i][j] = b;

            }
        }
        updateGridUI();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLocationByPlatform(true);
        pack();
        setMinimumSize(new Dimension(200, 200));
        setVisible(true);
        setFocusable(true);

        setUpPS();

    }

    private void vizulaizePS(){
        PS ps = new PS(gridWorld.getRows()*gridWorld.getColumns(),4,0.0,-1,1,0.05);


    }

    private void setUpPS(){
        ArrayList<Position> percepts = new ArrayList<>();
        for (int i = 0; i < gridWorld.getRows(); i++) {
            for (int j = 0; j < gridWorld.getColumns(); j++) {
                percepts.add(new Position(i,j));
                //should ignore walls
            }
        }

        for (Position p: percepts){
            //System.out.println(p);
        }

        System.out.println(percepts.size());

//        ArrayList<Position> actions = new ArrayList<>();
//        actions.add(new Position(-1,0));
//        actions.add(new Position(1,0));
//        actions.add(new Position(0,-1));
//        actions.add(new Position(0,1));

        ArrayList<String> actionsStr = new ArrayList<>();
        actionsStr.add("LEFT");
        actionsStr.add("RIGHT");
        actionsStr.add("UP");
        actionsStr.add("DOWN");

        ps = new ProjectionSimulationGen<>(this,percepts,actionsStr,0.05,20.0,1);
        //ps = new ProjectionSimulationGen<>(this,percepts,actionsStr,0.001,2.0,1);
        ps.setDamperGlow(0.02);






    }

    private void train(int time){
        int minStepCount = Integer.MAX_VALUE;
        for (int i = 0; i < time; i++) {

            if(gridWorld.isGameEnded()) {

                minStepCount = (gridWorld.getStepCount()<minStepCount) ? gridWorld.getStepCount():minStepCount;
                gridWorld.reset();
            }

            Position agentPos = gridWorld.getAgentPos();
            String action = ps.getActionSelfReward(agentPos);
            // System.out.println(i+"| "+agentPos+ " : "+action);
            doAction(action);

        }
        System.out.println("Best round : "+minStepCount+" steps");
        gridWorld.reset();

    }

    private void doAction(String action){
        switch (action){
            case "LEFT": gridWorld.moveLeft();
                break;
            case "RIGHT": gridWorld.moveRight();
                break;
            case "UP": gridWorld.moveUp();
                break;
            case "DOWN": gridWorld.moveDown();
                break;
            default:
                System.out.println("Unkown action?");break;
        }

    }


    private void updateGridUI() {
        updateTextFields();
        for (int i = 0; i < panelGrid.length; i++) {
            for (int j = 0; j < panelGrid[i].length; j++) {

                JPanel p = panelGrid[i][j];
                Color gridColor;
                switch (gridWorld.get(i, j)) {
                    case GridWorld.WALL:
                        gridColor = color_wall;
                        break;
                    case GridWorld.AGENT:
                        gridColor = color_agent;
                        break;
                    case GridWorld.GOAL:
                        gridColor = color_goal;
                        break;
                    case GridWorld.EMPTY_USED:
                        gridColor = color_empty_used;
                        break;
                    default:
                        gridColor = color_empty;
                        break;
                }

                p.setBackground(gridColor);
            }
        }

    }

    private void doActionAgent(boolean online){
        Position agentPos = gridWorld.getAgentPos();
        String action = ps.getActionSelfReward(agentPos);
        //System.out.println(action);
        switch (action){
            case "LEFT": gridWorld.moveLeft();break;
            case "RIGHT": gridWorld.moveRight();break;
            case "UP": gridWorld.moveUp();break;
            case "DOWN": gridWorld.moveDown();break;
            default:
                System.out.println("Unkown action?");break;
        }
        System.out.println("Did action :"+action);
        if(online)
            updateGridUI();
    }


    private void updateTextFields(){
        jLabel_steps.setText("Steps = "+gridWorld.getStepCount());
    }

    private JPanel getTopPanel() {
        JPanel p = new JPanel(new MigLayout());
        JButton b_reset = new JButton("Reset");
        b_reset.addActionListener(e -> {
            gridWorld.reset();
            updateGridUI();
            System.out.println("Reset");
        });


        jLabel_steps = new JLabel("Steps = ");


        JButton b_startAI = new JButton("Run");
        b_startAI.addActionListener(e ->{

           /* for (int i = 0; i < 10000; i++) {
                doActionAgent(false);
            }*/
            System.out.println("done");

        });

        JButton b_stop = new JButton("Do action");
        b_stop.addActionListener(e ->{

            doActionAgent(true);

        });


        JButton b_train = new JButton("Train");
        b_train.addActionListener(e ->{

            train(1000);

            System.out.println("Done training");

        });
        p.add(b_reset, "wrap");
        p.add(b_startAI,"wrap");
        p.add(b_stop,"wrap");
        p.add(jLabel_steps,"wrap");
        p.add(b_train,"wrap");
        return p;
    }


    public static void main(String[] args) {
        EventQueue.invokeLater(GridWorldGUI::new);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                gridWorld.moveLeft();

                break;
            case KeyEvent.VK_RIGHT:
                gridWorld.moveRight();
                break;
            case KeyEvent.VK_UP:
                gridWorld.moveUp();
                break;
            case KeyEvent.VK_DOWN:
                gridWorld.moveDown();
                break;
            case KeyEvent.VK_ENTER:
                gridWorld.reset();
                break;
        }
        updateGridUI();


    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
        requestFocus();
    }

    @Override
    public boolean isCorrectActionGen(Position percept, String action) {

//        if(gridWorld.isGameEnded()){
//            System.out.println("WON : Percept : "+percept);
//            gridWorld.reset();
//            return true;
//        }
//        if(percept.equals(new Position(1,8)) && action.equals("UP")){
//            System.out.println("WON : Percept : "+percept);
//            return true;
//        }


        return gridWorld.isWinningMove(action);
    }
}
