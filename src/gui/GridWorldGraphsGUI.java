package gui;

import experiments.GridWorldGame.GridWorld;
import experiments.GridWorldGame.MultiPSGridWorldGameAgent;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Oystein on 09.09.14.
 */
public class GridWorldGraphsGUI extends SimpleSwingGraphGUI{


    private  ChartPanel chartPanel;
    private JLabel jLabel_steps;
    private  MultiPSGridWorldGameAgent[] agents;
    private  GridWorld gridWorld;

    private void old(){

        agents = new MultiPSGridWorldGameAgent[4];

        boolean useMatrix = true,useSoftMax=true;
        int nAgents = 200;
        int reflection = 1;

        agents[0] = new MultiPSGridWorldGameAgent(nAgents, gridWorld,0.001,20.0,0.07,reflection,useMatrix,useSoftMax);
        agents[1] = new MultiPSGridWorldGameAgent(nAgents, gridWorld,0.0001,20.0,0.07,reflection,useMatrix,useSoftMax);
        agents[2] = new MultiPSGridWorldGameAgent(nAgents, gridWorld,0.0005,20,0.02,reflection,useMatrix,useSoftMax);

        agents[3] = new MultiPSGridWorldGameAgent(nAgents, gridWorld,0.0,1.0,0.12,reflection,useMatrix,useSoftMax);
//        agents[4] = new MultiPSGridWorldGameAgent(100,gridWorld,0.00001,20.0,0.07,useMatrix);
        // agents[3] = new MultiPSGridWorldGameAgent(100,gridWorld,0.05,20,0.02);

        for (int i = 0; i < 10; i++) {
            train();

        }

//        setLayout(new BorderLayout());



    }

    public GridWorldGraphsGUI() {
        gridWorld = GridWorld.parseFile("src/experiments/GridWorldGame/worldPaper6x9.txt");
//        setChart(generateChartOptimalDamping());

        old();
        setChart(generateChart());

    }

    public void train(){
        for (MultiPSGridWorldGameAgent agent : agents) {

           agent.trainGames(false);

        }
    }


    public void updateChart() {
        chartPanel.setChart(generateChart());


    }

    private JFreeChart generateChart() {
        XYSeriesCollection ds = new XYSeriesCollection();


        for (MultiPSGridWorldGameAgent agent : agents) {

            ds.addSeries(agent.getSeries());
        }

        JFreeChart chart = ChartFactory.createXYLineChart("Learning curve", "trials", "Average steps", ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        //xyPlot.set
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(0,200);

        return chart;
    }


    private XYSeries generateSeries(String name, boolean useSoftMax){
        XYSeries series = new XYSeries(name);
        double bestSteps=Double.MAX_VALUE,bestGlow=-1;

        for (double i = 0.0; i <= 0.21; i+=0.01) {
            MultiPSGridWorldGameAgent ps =new MultiPSGridWorldGameAgent(10,gridWorld,0.0,1.0,i,1,true,useSoftMax);

            for (int j = 0; j <100 ; j++) {
                ps.trainGames(false);
            }
            Number avgLast = ps.getSeries().getY(ps.getSeries().getItemCount()-1);
            series.add(i,avgLast);

            if(avgLast.doubleValue()<bestSteps){
                bestSteps=avgLast.doubleValue();
                bestGlow=i;
            }

        }
        System.out.println("Least steps= "+bestSteps+" with glow= "+bestGlow);
        return series;

    }


    private JFreeChart generateChartOptimalDamping() {
        XYSeriesCollection ds = new XYSeriesCollection();

        ds.addSeries(generateSeries("SoftMax",true));
        ds.addSeries(generateSeries("Regular",false));


        JFreeChart chart = ChartFactory.createXYLineChart("Optimal AfterGlow", "glow damping parameter", "avg # steps", ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        //xyPlot.set
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(0,100);



        return chart;
    }

    private JPanel getTopPanel() {
        JPanel p = new JPanel(new MigLayout());
        JButton b_reset = new JButton("Train");
        b_reset.addActionListener(e -> {

           train();
            updateChart();
        });


        jLabel_steps = new JLabel("Steps = ");


        JButton b_startAI = new JButton("Run");
        b_startAI.addActionListener(e ->{



            System.out.println("done");

        });

        JButton b_stop = new JButton("Stop");
        b_stop.addActionListener(e ->{



        });
        p.add(b_reset, "wrap");
        p.add(b_startAI,"wrap");
        p.add(b_stop,"wrap");
        p.add(jLabel_steps,"wrap");
        return p;
    }



    public static void main(String[] args) {
        EventQueue.invokeLater(GridWorldGraphsGUI::new);
    }
}
