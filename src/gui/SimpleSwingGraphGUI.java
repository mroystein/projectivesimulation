package gui;

import experiments.ILearningAlgorithm;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Oystein on 23/10/14.
 */
public class SimpleSwingGraphGUI extends JFrame {


    private ChartPanel chartPanel;
    private JLabel jLabel_steps;


    public SimpleSwingGraphGUI() {


        setLayout(new BorderLayout());


        add(getTopPanel(),BorderLayout.PAGE_START);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLocationByPlatform(true);
        pack();
        setMinimumSize(new Dimension(200, 200));
        setVisible(true);
        setFocusable(true);


    }
    public SimpleSwingGraphGUI(List<Double> frequencies) {
        XYSeries series = new XYSeries("Frequencies");
        for (int i = 0; i < frequencies.size(); i++) {
            series.add(i,frequencies.get(i ));
        }
        init(series);

    }
    public SimpleSwingGraphGUI(List<Integer> frequencies, String plotName, String headerName,String xLabel, String yLabel, int yRangeStart, int yRangeEnd) {
        XYSeries series = new XYSeries(plotName);
        for (int i = 0; i < frequencies.size(); i++) {
            series.add(i,frequencies.get(i ));
        }
        XYSeriesCollection ds = new XYSeriesCollection();
        ds.addSeries(series);
        init(ds,headerName,xLabel,yLabel,yRangeStart,yRangeEnd);

    }

    public SimpleSwingGraphGUI(  XYSeriesCollection ds,  String headerName,String xLabel, String yLabel, double yRangeStart, double yRangeEnd) {

        init(ds,headerName,xLabel,yLabel,yRangeStart,yRangeEnd);

    }


    private void init(XYSeries... serieses) {
        XYSeriesCollection ds = new XYSeriesCollection();
        for (XYSeries series : serieses)
            ds.addSeries(series);
        init(ds,"Graf","Score","Games",0,200);
    }
    private void init(ArrayList<XYSeries> serieses){
        init(serieses.toArray(new XYSeries[serieses.size()]));
    }


//   static XYSeriesCollection ds;
//    public SimpleSwingGraphGUI(List<ILearningAlgorithm> learners, int numGames,String headerName,String xLabel, String yLabel, double yRangeStart, double yRangeEnd) {
//
//        XYSeriesCollection ds = new XYSeriesCollection();
//        for (ILearningAlgorithm learner : learners) {
//            ds.addSeries(new XYSeries(learner.toString()));
//        }
//        setLayout(new BorderLayout());
//
//        JFreeChart chart = ChartFactory.createXYLineChart(headerName, xLabel, yLabel, ds, PlotOrientation.VERTICAL, true, true, false);
//
//        XYPlot xyPlot = chart.getXYPlot();
//        ValueAxis axis = xyPlot.getRangeAxis();
//        axis.setRange(yRangeStart, yRangeEnd);
//        chartPanel = new ChartPanel(chart);
//        chartPanel.setInitialDelay(1);
//        add(chartPanel, BorderLayout.CENTER);
//        chartPanel.setChart(chart);
//
//        setMinimumSize(new Dimension(600, 500));
//        setLocation(0,300);
//        setVisible(true);
//        setFocusable(true);
//        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//
//
//        for (int i = 0; i < numGames; i++) {
//
//            for (int l = 0; l < learners.size(); l++) {
//                ILearningAlgorithm learner = learners.get(l);
//                learner.runOneEpoch();
//                int steps = learner.getWorld().getStepCounter();
//                ds.getSeries(l).add(i,steps);
//            }
//        }
//
//    }


    private void init(XYSeriesCollection ds, String headerName, String xLabel, String yLabel, double yRangeStart, double yRangeEnd) {
        setLayout(new BorderLayout());

        JFreeChart chart = ChartFactory.createXYLineChart(headerName, xLabel, yLabel, ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(yRangeStart, yRangeEnd);
        chartPanel = new ChartPanel(chart);
        chartPanel.setInitialDelay(1);
        add(chartPanel, BorderLayout.CENTER);
        chartPanel.setChart(chart);

        setMinimumSize(new Dimension(600, 500));
        setLocation(0,300);
        setVisible(true);
        setFocusable(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

   public void train(){};

    public SimpleSwingGraphGUI(XYSeries... serieses) {
//        this();
        setLayout(new BorderLayout());
        setChart(generateChart(serieses));

        setMinimumSize(new Dimension(600, 600));
        setVisible(true);
        setFocusable(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }





    public void setChart(JFreeChart chart){
        if(chartPanel==null){
            chartPanel = new ChartPanel(chart);
            chartPanel.setInitialDelay(1);
            add(chartPanel,BorderLayout.CENTER);

        }
        chartPanel.setChart(chart);
    }


    public void updateChart() {
        chartPanel.setChart(generateChart());


    }

    private JFreeChart generateChart(XYSeries... serieses) {
        XYSeriesCollection ds = new XYSeriesCollection();


        for (XYSeries series:serieses) {
            ds.addSeries(series);
        }

        JFreeChart chart = ChartFactory.createXYLineChart("Learning curve", "trials", "Average steps", ds, PlotOrientation.VERTICAL, true, true, false);

        XYPlot xyPlot = chart.getXYPlot();
        //xyPlot.set
        ValueAxis axis = xyPlot.getRangeAxis();
        axis.setRange(0,500);

        return chart;
    }







    private JPanel getTopPanel() {
        JPanel p = new JPanel(new MigLayout());
        JButton b_reset = new JButton("Train");
        b_reset.addActionListener(e -> {

            train();
            updateChart();
        });


        jLabel_steps = new JLabel("Steps = ");


        JButton b_startAI = new JButton("Run");
        b_startAI.addActionListener(e ->{



            System.out.println("done");

        });

        JButton b_stop = new JButton("Stop");
        b_stop.addActionListener(e ->{



        });
        p.add(b_reset, "wrap");
        p.add(b_startAI,"wrap");
        p.add(b_stop,"wrap");
        p.add(jLabel_steps,"wrap");
        return p;
    }


    public void start(){

        EventQueue.invokeLater(SimpleSwingGraphGUI::new);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(SimpleSwingGraphGUI::new);
    }
}
