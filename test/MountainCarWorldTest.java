import experiments.mountaincar.MountainCarWorld;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Oystein on 27/10/14.
 */
public class MountainCarWorldTest {

    @Test
    public void testRandomWorldGenerator() {
        MountainCarWorld w = new MountainCarWorld(true);

        for (int i = 0; i < 1000; i++) {

            Assert.assertTrue(w.getPosition() < MountainCarWorld.MAX_POSITION);
            Assert.assertTrue(w.getPosition() > MountainCarWorld.MIN_POSITION);

            Assert.assertTrue(w.getVelocity() < MountainCarWorld.MAX_VELOCITY);
            Assert.assertTrue(w.getVelocity() > MountainCarWorld.MIN_VELOCITY);

            w.reset();

//            System.out.println(w.getPosition()+" "+w.getVelocity());

        }
    }


    @Test
    public void testShortestPathFromBook() {
        //From book : 36 left and 63 right should bring the car to the goal.
        MountainCarWorld w = new MountainCarWorld(false);

        for (int i = 0; i < 36; i++) {
            w.update(-1);
        }
        for (int i = 0; i < 63; i++) {
            w.update(1);
        }

        Assert.assertTrue(w.isGameOver());
    }


}
